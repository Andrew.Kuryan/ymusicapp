import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    id("org.jetbrains.kotlin.jvm") version "1.4.30"
}

val compileKotlin: KotlinCompile by tasks
val compileTestKotlin: KotlinCompile by tasks

compileKotlin.kotlinOptions.jvmTarget = "1.8"
compileKotlin.kotlinOptions.freeCompilerArgs = listOf("-Xuse-experimental=kotlin.Experimental")

compileTestKotlin.kotlinOptions.jvmTarget = "1.8"
compileTestKotlin.kotlinOptions.freeCompilerArgs = listOf("-Xuse-experimental=kotlin.Experimental")

application {
    mainClass.set("com.appspot.magtech.ui.Application")
}

repositories {
    jcenter()
    // mavenLocal()
    maven("https://kotlin.bintray.com/ktor")
    maven("https://mymavenrepo.com/repo/lY4sT8ZAv6kDYJ8bfMRz/")
    maven("https://www.beatunes.com/repo/maven2/")
}

val ktor_version = "1.5.4"
val tornadofx_version = "1.7.20"
val coroutines_version = "1.4.3"
val junit_version = "5.7.0"

dependencies {
    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
    //ui
    implementation("no.tornado:tornadofx:$tornadofx_version")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx:$coroutines_version")
    implementation("com.jfoenix:jfoenix:8.0.8")
    implementation("com.dorkbox:SystemTray:3.17")
    implementation("fr.jcgay.send-notification:send-notification:0.16.0")
    implementation("org.reduxkotlin:redux-kotlin-threadsafe-jvm:0.5.5")

    //ktor-client
    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-core-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-cio:$ktor_version")
    implementation("io.ktor:ktor-client-websockets:$ktor_version")
    implementation("khttp:khttp:1.0.0")

    // utilities
    implementation("com.github.salomonbrys.kotson:kotson:2.5.0")
    implementation("commons-codec:commons-codec:1.9")
    implementation("org.jsoup:jsoup:1.13.1")
    implementation("org.zeroturnaround:zt-exec:1.12")
    implementation("ch.qos.logback:logback-classic:1.2.3")

    //DB
    implementation("org.jetbrains.exposed", "exposed-core", "0.24.1")
    implementation("org.jetbrains.exposed", "exposed-jdbc", "0.24.1")
    implementation("com.h2database:h2:1.4.200")

    // testing
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.1.0")
    testImplementation("io.ktor:ktor-client-mock:$ktor_version")
    testImplementation("io.ktor:ktor-client-mock-jvm:$ktor_version")

    testImplementation(platform("org.junit:junit-bom:$junit_version"))
    testImplementation("org.junit.jupiter:junit-jupiter:$junit_version")
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

tasks.named<Jar>("jar") {
    exclude("images/vector")
    exclude("downloads")
    manifest {
        attributes("Main-Class" to "com.appspot.magtech.ui.Application")
    }
    configurations["compileClasspath"].forEach { file: File ->
        from(zipTree(file.absoluteFile))
    }
}
