package com.appspot.magtech.api

import com.appspot.magtech.api.parsers.parseUserInfo
import com.appspot.magtech.apiports.AuthApi
import com.appspot.magtech.entities.SessionCredentials
import com.appspot.magtech.entities.User
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.client.statement.*
import io.ktor.http.*
import org.jsoup.Jsoup

fun HttpRequestBuilder.withSession(sessionCredentials: SessionCredentials) {
    header(
        "Cookie",
        "Session_id=${sessionCredentials.sessionId}; sessionid2=${sessionCredentials.sessionId2}; sessguard=${sessionCredentials.sessGuard}; yandexuid=${sessionCredentials.yandexUid}; uniqueuid=${sessionCredentials.uniqueUid}"
    )
}

class YMusicAuthApi(private val client: HttpClient, private val gson: Gson) : AuthApi {

    override suspend fun authorize(username: String, password: String): User {
        val loginPageResult =
            client.request<HttpResponse>("https://passport.yandex.by/auth/welcome?origin=music_button-header")
        val (yandexuid, uniqueuid) = with(loginPageResult.setCookie()) {
            find { it.name == "yandexuid" } to find { it.name == "uniqueuid" }
        }

        val csrfToken = Jsoup.parse(loginPageResult.readText())
            .getElementsByAttributeValue("name", "csrf_token")
            .firstOrNull()
            ?.attr("value")

        val enterPage = client.get<String>("https://passport.yandex.by/auth?origin=music_button-header")
        val storeStart = enterPage.indexOf("window.__REDUX_STORE__")
        val storeObjStart = enterPage.indexOf("{", storeStart)
        val storeEnd = enterPage.indexOf("</script>", storeStart)
        val script = enterPage.substring(storeObjStart, storeEnd)
        val store: LinkedTreeMap<Any, Any> = gson.fromJson(script)

        val process_uid = (store["auth"] as? LinkedTreeMap<*, *>)?.get("process_uuid")

        fun HeadersBuilder.defaultCookies() {
            append("Host", "passport.yandex.by")
            append("Origin", "https://passport.yandex.by")
            append("Sec-Fetch-Dest", "empty")
            append("Sec-Fetch-Mode", "cors")
            append("Sec-Fetch-Site", "same-origin")
            append("Cookie", "yandexuid=${yandexuid?.value}; uniqueuid=${uniqueuid?.value}")
        }

        val loginInputRes =
            client.post<String>("https://passport.yandex.by/registration-validations/auth/multi_step/start") {
                headers {
                    append("Referer", "https://passport.yandex.by/auth/add?origin=music_button-header")
                    defaultCookies()
                }
                body = FormDataContent(Parameters.build {
                    append("csrf_token", csrfToken ?: "")
                    append("login", username)
                    append("origin", "music_button-header")
                    append("process_uuid", process_uid?.toString() ?: "")
                })
            }
        val loginInputObj: LinkedTreeMap<Any, Any> = gson.fromJson(loginInputRes)

        val passInputRes =
            client.request<HttpResponse>("https://passport.yandex.by/registration-validations/auth/multi_step/commit_password") {
                method = HttpMethod.Post
                headers {
                    append("Referer", "https://passport.yandex.by/auth/welcome?origin=music_button-header&mode=edit")
                    defaultCookies()
                }
                body = FormDataContent(Parameters.build {
                    append("csrf_token", csrfToken ?: "")
                    append("track_id", loginInputObj["track_id"]?.toString() ?: "")
                    append("password", password)
                })
            }

        val resultCookies = passInputRes.setCookie()

        return getUserInfo(
            SessionCredentials(
                sessionId = resultCookies.find { it.name == "Session_id" }?.value ?: "",
                sessionId2 = resultCookies.find { it.name == "sessionid2" }?.value ?: "",
                sessGuard = resultCookies.find { it.name == "sessguard" }?.value ?: "",
                yandexUid = yandexuid?.value ?: "",
                uniqueUid = uniqueuid?.value ?: ""
            )
        )
    }

    private suspend fun getUserInfo(sessionCredentials: SessionCredentials): User {
        val result = client.get<String>("https://music.yandex.by/handlers/accounts.jsx") {
            withSession(sessionCredentials)
        }
        return parseUserInfo(gson.fromJson(result), sessionCredentials).first()
    }
}