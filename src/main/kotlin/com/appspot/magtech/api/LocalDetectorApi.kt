package com.appspot.magtech.api

import com.appspot.magtech.api.parsers.*
import com.appspot.magtech.apiports.DetectorApi
import com.appspot.magtech.application.Action
import com.appspot.magtech.application.tracks.TracksAction
import com.appspot.magtech.entities.LocalTrack
import com.appspot.magtech.entities.Track
import com.appspot.magtech.entities.TrackAnalysis
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import io.ktor.client.*
import io.ktor.client.features.websocket.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.withContext

class LocalDetectorApi(
    private val client: HttpClient,
    private val gson: Gson,
    val actions: Channel<Action>
) : DetectorApi {

    override suspend fun getTrackAnalysis(track: Track): TrackAnalysis {
        val result = client.get<String>("http://localhost:3355/analysis/${track.id}")
        return parseTrackAnalysis(gson.fromJson(result))
    }

    override suspend fun getAllTrackAnalysis(): List<Pair<Int, TrackAnalysis>> {
        val result = client.get<String>("http://localhost:3355/analysis")
        return parseTrackAnalysisList(gson.fromJson(result))
    }

    override suspend fun removeTrackAnalysis(track: Track) {
        client.delete<String>("http://localhost:3355/analysis/${track.id}")
    }

    override suspend fun notifyDetector(newTrack: LocalTrack) {
        client.put<String>("http://localhost:3355/new-track") {
            contentType(ContentType.Application.Json)
            body = """{
                        |"id": ${newTrack.id};
                        |"localPath": "${newTrack.localPath}"
                     |}""".trimMargin()
        }
    }

    override suspend fun connectToDetector(afterConnect: suspend () -> Unit) {
        client.ws(host = "127.0.0.1", port = 3355, path = "/updates", method = HttpMethod.Get) {
            afterConnect()
            for (frame in incoming) {
                frame as? Frame.Text ?: continue
                try {
                    val update = parseAnalysisListUpdate(gson.fromJson(frame.readText()))
                    val action = when (update) {
                        is AnalysisListUpdate.ChangeItem -> TracksAction.FetchTrackAnalysis(
                            trackId = update.newTask.trackId,
                            trackAnalysis = update.newTask.toTrackAnalysis()
                        )
                        is AnalysisListUpdate.AddItem -> null
                        is AnalysisListUpdate.RemoteItem -> null
                        is AnalysisListUpdate.NoChange -> null
                    }
                    if (action != null) {
                        withContext(Dispatchers.Main) { actions.send(action) }
                    }
                } catch (exc: Exception) {
                    println(exc)
                }
            }
        }
    }
}