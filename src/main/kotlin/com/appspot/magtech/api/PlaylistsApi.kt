package com.appspot.magtech.api

import com.appspot.magtech.api.parsers.playlist.*
import com.appspot.magtech.apiports.LocalApi
import com.appspot.magtech.entities.*
import com.appspot.magtech.extensions.NotAuthorizedException
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import io.ktor.client.*
import io.ktor.client.request.*

class PlaylistsApi(
    private val client: HttpClient,
    private val gson: Gson,
    private val downloadsApi: LocalApi
) {

    private fun savedPlaylistsListUrl(ownerLogin: String) =
        "https://music.yandex.by/handlers/library.jsx?owner=$ownerLogin&filter=playlists&likeFilter=favorite&external-domain=music.yandex.by&overembed=false"

    private fun feedPlaylistsListUrl() = "https://music.yandex.by/handlers/feed.jsx"
    private fun playlistByKindUrl(ownerLogin: String, kind: Int) =
        "https://music.yandex.by/handlers/playlist.jsx?owner=$ownerLogin&kinds=$kind"

    private suspend fun <T> withUser(auth: Auth, action: suspend (User) -> T) =
        if (auth is User) {
            action(auth)
        } else throw NotAuthorizedException()

    suspend fun getPlaylistTracks(auth: Auth, playlist: Playlist): List<Track> {
        return when (playlist) {
            is LocalPlaylist -> downloadsApi.getLocalPlaylistTracks(playlist)
            is FeedPlaylist -> withUser(auth) { user ->
                val result = client.get<String>(feedPlaylistsListUrl()) {
                    withSession(user.sessionCredentials)
                }
                parseFeedPlaylistTracks(gson.fromJson(result), playlist)
            }
            is UserPlaylist -> withUser(auth) { user ->
                val result = client.get<String>(playlistByKindUrl(user.login, playlist.kind)) {
                    withSession(user.sessionCredentials)
                }
                parseSavedPlaylistTracks(gson.fromJson(result))
            }
        }
    }

    suspend fun getSavedPlaylists(user: User): List<Pair<UserPlaylist, List<YMusicTrack>?>> {
        val result = client.get<String>(savedPlaylistsListUrl(user.login)) {
            withSession(user.sessionCredentials)
            header("Referer", "https://music.yandex.by/users/${user.login}/playlists")
            header("X-Current-UID", user.uid)
            header("X-Retpath-Y", "https://music.yandex.by/users/${user.login}/playlists")
        }
        return parseSavedPlaylists(gson.fromJson(result)).map {
            when (it) {
                is ParseSavedPlaylistResult.Success -> it.playlist to null
                is ParseSavedPlaylistResult.Failed -> getSavedPlaylistDetailedInfo(user, it.kind)
            }
        }
    }

    suspend fun getSelectedPlaylists(user: User): List<Pair<FeedPlaylist, List<YMusicTrack>>> {
        val result = client.get<String>(feedPlaylistsListUrl()) {
            withSession(user.sessionCredentials)
        }
        return parseFeed(gson.fromJson(result))
    }

    private suspend fun getSavedPlaylistDetailedInfo(user: User, kind: Int): Pair<UserPlaylist, List<YMusicTrack>> {
        val result = client.get<String>(playlistByKindUrl(user.login, kind)) {
            withSession(user.sessionCredentials)
        }
        return parseSavedPlaylistDetailedInfo(gson.fromJson(result))
    }
}
