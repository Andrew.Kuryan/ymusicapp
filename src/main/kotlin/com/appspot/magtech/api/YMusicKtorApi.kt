package com.appspot.magtech.api

import com.appspot.magtech.api.db.DBDownloadsApi
import com.appspot.magtech.api.parsers.parseTrackLyrics
import com.appspot.magtech.api.parsers.parseTracksSearch
import com.appspot.magtech.apiports.FileSystem
import com.appspot.magtech.apiports.YMusicApi
import com.appspot.magtech.application.Action
import com.appspot.magtech.entities.*
import com.appspot.magtech.extensions.getApiValue
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import io.ktor.client.*
import io.ktor.client.request.*
import kotlinx.coroutines.channels.Channel
import org.apache.commons.codec.digest.DigestUtils

class UnknownApiException(description: String? = null) : Exception(description)

class YMusicKtorApi(
    private val client: HttpClient,
    private val fileSystem: FileSystem,
    storagePath: String,
    val actions: Channel<Action>,
) : YMusicApi {

    private val gson = Gson()

    override suspend fun getTrackLink(auth: Auth, track: Track) = when (track) {
        is LocalTrack -> fileSystem.getURI(track.localPath)
        is YMusicTrack -> {
            if (track.available && track.info.albums.isNotEmpty() && auth is User) {
                val infoRaw =
                    client.get<String>("https://music.yandex.by/api/v2.1/handlers/track/${track.id}:${track.info.albums[0].id}/web-album_track-track-track-main/download/m?hq=0") {
                        header("X-Retpath-Y", "https://music.yandex.by/")
                        withSession(auth.sessionCredentials)
                    }
                val info: LinkedTreeMap<Any, Any> = gson.fromJson(infoRaw)
                val src = info.getApiValue<String>("src")

                val trackInfoRaw = client.get<String>("$src&format=json") {
                    withSession(auth.sessionCredentials)
                }

                val trackInfo: LinkedTreeMap<Any, Any> = gson.fromJson(trackInfoRaw)
                val path = trackInfo.getApiValue<String>("path")
                val s = trackInfo.getApiValue<String>("s")
                val host = trackInfo.getApiValue<String>("host")
                val ts = trackInfo.getApiValue<String>("ts")

                val secret = DigestUtils.md5Hex("XGRlBW9FXlekgbPrRHuSiA${path.substring(1)}$s")

                "https://$host/get-mp3/$secret/$ts$path?track-id=${track.id}&play=false"
            } else ""
        }
    }

    override suspend fun getTrackDetailedInfo(user: User, track: Track): TrackDetailedInfo {
        val result =
            client.get<String>("https://music.yandex.by/handlers/track.jsx?track=${track.id}:${track.info.albums[0].id}&lang=ru&external-domain=music.yandex.by&overembed=false") {
                withSession(user.sessionCredentials)
                header("Referer", "https://music.yandex.by/album/${track.info.albums[0].id}/track/${track.id}")
                header("X-Current-UID", user.uid)
                header("X-Retpath-Y", "https://music.yandex.by/album/${track.info.albums[0].id}/track/${track.id}")
            }
        val lyrics = parseTrackLyrics(gson.fromJson(result))
        return TrackDetailedInfo(lyrics, listOf())
    }

    override suspend fun searchTracks(user: User, query: String): List<YMusicTrack> {
        val result =
            client.get<String>("https://music.yandex.by/handlers/music-search.jsx?text=$query&type=all&lang=ru&external-domain=music.yandex.by&overembed=false") {
                withSession(user.sessionCredentials)
            }
        return parseTracksSearch(gson.fromJson(result))
    }

    override fun getImageLink(rawLink: String, size: Int): String {
        val sizeRequest = "${size}x$size"
        return "https://${rawLink.replace("%%", sizeRequest)}"
    }

    override val localApi = DBDownloadsApi(storagePath, client, ::getTrackLink, fileSystem)
    override val authApi = YMusicAuthApi(client, gson)
    override val detectorApi = LocalDetectorApi(client, gson, actions)

    private val playlistsApi = PlaylistsApi(client, gson, localApi)

    override suspend fun getSavedPlaylists(user: User) =
        playlistsApi.getSavedPlaylists(user)

    override suspend fun getSelectedPlaylists(user: User) =
        playlistsApi.getSelectedPlaylists(user)

    override suspend fun getPlaylistTracks(auth: Auth, playlist: Playlist) =
        playlistsApi.getPlaylistTracks(auth, playlist)
}
