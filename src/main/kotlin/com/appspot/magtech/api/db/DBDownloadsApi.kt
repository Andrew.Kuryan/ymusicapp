package com.appspot.magtech.api.db

import com.appspot.magtech.api.db.triggers.*
import com.appspot.magtech.apiports.FileSystem
import com.appspot.magtech.apiports.LocalApi
import com.appspot.magtech.entities.*
import com.appspot.magtech.extensions.db.createh2trigger
import io.ktor.client.*
import io.ktor.client.request.*
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync
import org.jetbrains.exposed.sql.transactions.transaction
import java.net.URLEncoder
import java.sql.Connection

class DBDownloadsApi(
    private val storagePath: String,
    private val client: HttpClient,
    private val getTrackLink: suspend (Auth, Track) -> String,
    private val fileSystem: FileSystem,
) : LocalApi {

    private val defaultPlaylistTitle = "Downloads"

    // ;AUTO_SERVER=TRUE;AUTO_SERVER_PORT=8043;AUTO_RECONNECT=TRUE;DB_CLOSE_ON_EXIT=FALSE;DB_CLOSE_DELAY=-1;
    private val db = Database.connect(
        url = "jdbc:h2:./${storagePath}downloads",
        driver = "org.h2.Driver"
    )

    init {
        transaction(Connection.TRANSACTION_SERIALIZABLE, 2, db) {
            SchemaUtils.create(Playlists, Tracks, Albums, TrackAlbums, Artists, TrackArtists, Users, YMusicSessions)

            createh2trigger<OnAddTrackTrigger>()
            createh2trigger<OnRemoveTrackTrigger>()
            createh2trigger<OnRemoveTrackArtistTrigger>()
            createh2trigger<OnRemoveTrackAlbumTrigger>()

            Playlists.select { Playlists.title eq defaultPlaylistTitle }.firstOrNull()
                ?: Playlists.insert {
                    it[title] = defaultPlaylistTitle
                }
        }
    }

    private fun Track.makeLocalPath() =
        "${fileSystem.getAbsolutePath(storagePath)}/${URLEncoder.encode(info.title, "UTF-8")}.mp3"

    override suspend fun downloadTrack(user: User, track: YMusicTrack): LocalTrack {
        val trackLink = getTrackLink(user, track)
        val bytes = client.get<ByteArray>(trackLink)
        val trackLocalPath = track.makeLocalPath()
        fileSystem.writeBytes(trackLocalPath, bytes)
        suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            Tracks.insert {
                it[id] = track.id
                it[title] = track.info.title
                it[duration] = track.info.duration.toInt()
                it[localPath] = trackLocalPath
                it[coverUri] = track.info.coverUri
                it[playlistTitle] = defaultPlaylistTitle
            }
            track.info.artists.forEach { artist ->
                Artists.select { Artists.id eq artist.id }.firstOrNull()
                    ?: Artists.insert {
                        it[id] = artist.id
                        it[name] = artist.name
                        it[coverUri] = artist.coverUri
                    }
                TrackArtists.insert {
                    it[trackId] = track.id
                    it[artistId] = artist.id
                }
            }
            track.info.albums.forEach { album ->
                Albums.select { Albums.id eq album.id }.firstOrNull()
                    ?: Albums.insert {
                        it[id] = album.id
                        it[title] = album.title
                        it[trackCount] = album.trackCount
                        it[coverUri] = album.coverUri
                    }
                TrackAlbums.insert {
                    it[trackId] = track.id
                    it[albumId] = album.id
                }
            }
        }.await()
        return track.toLocal(trackLocalPath, NotAvailableAnalysis)
    }

    override suspend fun removeTrack(track: LocalTrack): YMusicTrack {
        fileSystem.deleteFile(track.localPath)
        suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            Tracks.deleteWhere { Tracks.id eq track.id }
        }.await()
        return track.toOriginal()
    }

    override suspend fun getLocalPlaylists(): List<Pair<LocalPlaylist, List<LocalTrack>>> {
        return suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            Playlists.leftJoin(
                Tracks
                    .leftJoin(TrackAlbums.leftJoin(Albums))
                    .leftJoin(TrackArtists.leftJoin(Artists))
            )
                .selectAll()
                .map {
                    LocalPlaylist(
                        title = it[Playlists.title], info = PlaylistInfo(
                            duration = it[Playlists.duration].toLong(),
                            trackCount = it[Playlists.trackCount]
                        )
                    ) to if (it.getOrNull(Tracks.id) != null)
                        listOf(parseTrack(it))
                    else listOf()
                }
                .groupBy { it.first.title }
                .mapValues { entry ->
                    entry.value.first().first to entry.value.map { it.second }.flatten()
                        .collectAsResponse()
                }
                .values
                .toList()
        }.await()
    }

    override suspend fun getLocalPlaylistTracks(playlist: LocalPlaylist): List<LocalTrack> {
        return suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            Tracks
                .leftJoin(TrackAlbums.leftJoin(Albums))
                .leftJoin(TrackArtists.leftJoin(Artists))
                .select { Tracks.playlistTitle eq playlist.title }
                .map { parseTrack(it) }
                .collectAsResponse()
        }.await()
    }

    suspend fun getPlaylistArtists(playlist: LocalPlaylist): List<Artist> {
        return suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            Artists.innerJoin(TrackArtists.innerJoin(Tracks))
                .select { Tracks.playlistTitle eq playlist.title }
                .map(::parseArtist)
                .distinctBy { it.id }
        }.await()
    }

    suspend fun getPlaylistAlbums(playlist: LocalPlaylist): List<Album> {
        return suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            Albums.innerJoin(TrackAlbums.innerJoin(Tracks))
                .select { Tracks.playlistTitle eq playlist.title }
                .map(::parseAlbum)
                .distinctBy { it.id }
        }.await()
    }

    override suspend fun saveUser(user: User, isActive: Boolean): User {
        suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            Users.insert {
                it[uid] = user.uid
                it[login] = user.login
                it[avatarUrl] = user.avatarUrl
                it[Users.isActive] = isActive
            }
            YMusicSessions.insert {
                it[sessionId] = user.sessionCredentials.sessionId
                it[sessionId2] = user.sessionCredentials.sessionId2
                it[sessGuard] = user.sessionCredentials.sessGuard
                it[yandexUid] = user.sessionCredentials.yandexUid
                it[uniqueUid] = user.sessionCredentials.uniqueUid
                it[userUid] = user.uid
            }
        }.await()
        return user
    }

    override suspend fun getSavedUsers(): List<Pair<User, Boolean>> {
        return suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            Users.innerJoin(YMusicSessions)
                .selectAll()
                .map(::parseUser)
        }.await()
    }

    override suspend fun markUserAsActive(user: User): User {
        suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            Users.update {
                it[isActive] = uid eq user.uid
            }
        }.await()
        return user
    }

    override suspend fun unmarkUserAsActive(user: User): User {
        suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            Users.update({ Users.uid eq user.uid }) {
                it[isActive] = false
            }
        }.await()
        return user
    }

    override suspend fun removeUser(user: User): User {
        suspendedTransactionAsync(Dispatchers.IO, db, Connection.TRANSACTION_SERIALIZABLE) {
            Users.deleteWhere { Users.uid eq user.uid }
        }.await()
        return user
    }

    private fun List<LocalTrack>.collectAsResponse() = groupBy { it.id }
        .mapValues {
            listOf(it.value.first().copy(
                info = it.value.first().info.copy(
                    artists = it.value.map { track -> track.info.artists }
                        .flatten()
                        .distinct(),
                    albums = it.value.map { track -> track.info.albums }
                        .flatten()
                        .distinct()
                ),
            ))
        }
        .values
        .flatten()

    private fun parseTrack(row: ResultRow): LocalTrack =
        LocalTrack(
            id = row[Tracks.id],
            localPath = row[Tracks.localPath],
            info = TrackInfo(
                title = row[Tracks.title],
                duration = row[Tracks.duration].toLong(),
                coverUri = row[Tracks.coverUri],
                artists = listOf(parseArtist(row)),
                albums = listOf(parseAlbum(row))
            ),
            analysis = NotAvailableAnalysis,
        )

    private fun parseAlbum(row: ResultRow): Album =
        Album(
            id = row[Albums.id],
            title = row[Albums.title],
            trackCount = row[Albums.trackCount],
            coverUri = row[Albums.coverUri]
        )

    private fun parseArtist(row: ResultRow): Artist =
        Artist(
            id = row[Artists.id],
            name = row[Artists.name],
            coverUri = row[Artists.coverUri]
        )

    private fun parseUser(row: ResultRow): Pair<User, Boolean> =
        User(
            uid = row[Users.uid],
            login = row[Users.login],
            avatarUrl = row[Users.avatarUrl],
            sessionCredentials = SessionCredentials(
                sessionId = row[YMusicSessions.sessionId],
                sessionId2 = row[YMusicSessions.sessionId2],
                sessGuard = row[YMusicSessions.sessGuard],
                yandexUid = row[YMusicSessions.yandexUid],
                uniqueUid = row[YMusicSessions.uniqueUid],
            )
        ) to row[Users.isActive]
}
