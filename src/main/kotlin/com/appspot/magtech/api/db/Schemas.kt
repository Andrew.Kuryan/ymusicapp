package com.appspot.magtech.api.db

import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table

object Playlists : Table("playlists") {
    val title = varchar("title", 255).uniqueIndex()
    val duration = integer("duration").default(0)
    val trackCount = integer("track_count").default(0)

    override val primaryKey = PrimaryKey(title, name = "pk_playlists_title")
}

object Tracks : Table("tracks") {
    val id = integer("id").uniqueIndex()
    val title = varchar("title", 255)
    val duration = integer("duration")
    val localPath = varchar("local_path", 255)
    val coverUri = varchar("cover_uri", 255)
    val playlistTitle = varchar("playlist_title", 255)
        .references(Playlists.title, onDelete = ReferenceOption.CASCADE, onUpdate = ReferenceOption.CASCADE)

    override val primaryKey = PrimaryKey(id, name = "pk_tracks_id")
}

object Albums : Table("albums") {
    val id = integer("id").uniqueIndex()
    val title = varchar("title", 255)
    val trackCount = integer("track_count")
    val coverUri = varchar("cover_uri", 255)

    override val primaryKey = PrimaryKey(id, name = "pk_albums_id")
}

object TrackAlbums : Table("track_albums") {
    val trackId = integer("track_id")
        .references(Tracks.id, onDelete = ReferenceOption.CASCADE, onUpdate = ReferenceOption.CASCADE)
    val albumId = integer("album_id")
        .references(Albums.id, onDelete = ReferenceOption.RESTRICT, onUpdate = ReferenceOption.CASCADE)

    override val primaryKey = PrimaryKey(trackId, albumId, name = "pk_track_albums")
}

object Artists : Table("artists") {
    val id = integer("id").uniqueIndex()
    val name = varchar("name", 255)
    val coverUri = varchar("cover_uri", 255)

    override val primaryKey = PrimaryKey(id, name = "pk_artists_id")
}

object TrackArtists : Table("track_artists") {
    val trackId = integer("track_id")
        .references(Tracks.id, onDelete = ReferenceOption.CASCADE, onUpdate = ReferenceOption.CASCADE)
    val artistId = integer("artist_id")
        .references(Artists.id, onDelete = ReferenceOption.RESTRICT, onUpdate = ReferenceOption.CASCADE)

    override val primaryKey = PrimaryKey(trackId, artistId, name = "pk_track_artists")
}

object Users : Table("users") {
    val uid = varchar("uid", 255)
    val login = varchar("login", 255)
    val avatarUrl = varchar("avatar_url", 255).nullable()
    val isActive = bool("is_active")

    override val primaryKey = PrimaryKey(uid, name = "pk_users")
}

object YMusicSessions : Table("sessions") {
    val sessionId = varchar("session_id", 255)
    val sessionId2 = varchar("session_id_2", 255)
    val sessGuard = varchar("sess_guard", 255)
    val yandexUid = varchar("yandex_uid", 255)
    val uniqueUid = varchar("unique_uid", 255)

    val userUid = varchar("user_uid", 255)
        .references(Users.uid, onDelete = ReferenceOption.CASCADE, onUpdate = ReferenceOption.CASCADE)
        .uniqueIndex()

    override val primaryKey = PrimaryKey(sessionId, sessionId, name = "pk_ymusic_sessions")
}