package com.appspot.magtech.api.db.triggers

import com.appspot.magtech.api.db.TrackAlbums
import com.appspot.magtech.extensions.db.AfterDeleteTrigger
import java.sql.Connection
import java.sql.ResultSet

class OnRemoveTrackAlbumTrigger : AfterDeleteTrigger(TrackAlbums) {

    override fun execute(conn: Connection, oldRow: ResultSet) {
        val albumId = oldRow.getInt("album_id")

        val isEmpty = conn.prepareStatement("""
           SELECT * FROM track_albums
           WHERE album_id = $albumId 
        """).use {
            it.execute()
            !it.resultSet.next()
        }

        if (isEmpty) {
            conn.prepareStatement("""
               DELETE FROM albums
               WHERE id = $albumId 
            """).use { it.execute() }
        }
    }
}
