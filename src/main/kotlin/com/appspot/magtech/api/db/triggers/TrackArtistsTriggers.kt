package com.appspot.magtech.api.db.triggers

import com.appspot.magtech.api.db.TrackArtists
import com.appspot.magtech.extensions.db.AfterDeleteTrigger
import java.sql.Connection
import java.sql.ResultSet

class OnRemoveTrackArtistTrigger : AfterDeleteTrigger(TrackArtists) {

    override fun execute(conn: Connection, oldRow: ResultSet) {
        val artistId = oldRow.getInt("artist_id")

        val isEmpty = conn.prepareStatement("""
           SELECT * FROM track_artists
           WHERE artist_id = $artistId 
        """).use {
            it.execute()
            !it.resultSet.next()
        }

        if (isEmpty) {
            conn.prepareStatement("""
               DELETE FROM artists
               WHERE id = $artistId 
            """).use { it.execute() }
        }
    }
}
