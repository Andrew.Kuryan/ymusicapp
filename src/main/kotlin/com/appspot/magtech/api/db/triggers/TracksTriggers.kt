package com.appspot.magtech.api.db.triggers

import com.appspot.magtech.api.db.Tracks
import com.appspot.magtech.extensions.db.AfterDeleteTrigger
import com.appspot.magtech.extensions.db.AfterInsertTrigger
import java.sql.Connection
import java.sql.ResultSet

class OnAddTrackTrigger : AfterInsertTrigger(Tracks) {

    override fun execute(conn: Connection, newRow: ResultSet) {
        val duration = newRow.getInt("duration")
        val playlistTitle = newRow.getString("playlist_title")

        conn.prepareStatement(
            """
            UPDATE playlists
            SET duration = duration + $duration,
            track_count = track_count + 1
            WHERE title = '$playlistTitle'
        """
        ).use { it.execute() }
    }
}

class OnRemoveTrackTrigger : AfterDeleteTrigger(Tracks) {

    override fun execute(conn: Connection, oldRow: ResultSet) {
        val duration = oldRow.getInt("duration")
        val playlistTitle = oldRow.getString("playlist_title")

        conn.prepareStatement(
            """
            UPDATE playlists
            SET duration = duration - $duration,
            track_count = track_count - 1
            WHERE title = '$playlistTitle'
        """
        ).use { it.execute() }
    }
}
