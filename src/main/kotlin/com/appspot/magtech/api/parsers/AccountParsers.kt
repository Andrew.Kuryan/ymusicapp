package com.appspot.magtech.api.parsers

import com.appspot.magtech.entities.SessionCredentials
import com.appspot.magtech.entities.User
import com.google.gson.annotations.SerializedName

data class AccountDisplayName(
        val name: String,
        val default_avatar: String?
)

data class AccountInfo(
        val status: Boolean,
        val uid: String,
        val login: String,
        val displayName: AccountDisplayName,
        val avatarHash: String?
)

data class UserInfo(
        val default_uid: String,
        val accounts: List<AccountInfo>,
        @SerializedName("can-add-more")
        val canAddMore: Boolean
)

fun parseUserInfo(userInfo: UserInfo, sessionCredentials: SessionCredentials): List<User> {
    return userInfo.accounts.map {
        User(
                uid = it.uid,
                login = it.login,
                avatarUrl = it.displayName.default_avatar ?: it.avatarHash,
                sessionCredentials = sessionCredentials
        )
    }
}
