package com.appspot.magtech.api.parsers

import com.appspot.magtech.api.UnknownApiException
import com.appspot.magtech.entities.FulfilledAnalysis
import com.appspot.magtech.entities.NotAvailableAnalysis
import com.appspot.magtech.entities.ProcessingAnalysis
import com.appspot.magtech.entities.TrackAnalysis
import com.appspot.magtech.extensions.getApiValue
import com.appspot.magtech.extensions.toRealString
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap

sealed class AnalysisTask {
    abstract val trackId: Int

    data class Ready(override val trackId: Int, val localPath: String) : AnalysisTask()
    data class Processing(override val trackId: Int, val processingPercent: Double) : AnalysisTask()
    data class Finished(override val trackId: Int, val analysis: FulfilledAnalysis) : AnalysisTask()
    data class Failed(override val trackId: Int) : AnalysisTask()
}

fun AnalysisTask.toTrackAnalysis(): TrackAnalysis {
    return when (this) {
        is AnalysisTask.Ready -> NotAvailableAnalysis
        is AnalysisTask.Processing -> ProcessingAnalysis(this.processingPercent)
        is AnalysisTask.Finished -> this.analysis
        is AnalysisTask.Failed -> NotAvailableAnalysis
    }
}

sealed class AnalysisListUpdate {
    data class AddItem(val task: AnalysisTask) : AnalysisListUpdate()
    data class ChangeItem(val oldTask: AnalysisTask, val newTask: AnalysisTask) : AnalysisListUpdate()
    data class RemoteItem(val task: AnalysisTask) : AnalysisListUpdate()
    object NoChange : AnalysisListUpdate()
}

fun parseAnalysisTask(json: LinkedTreeMap<Any, Any>): AnalysisTask {
    val type = json.getApiValue<String>("type")
    val trackId = json.getApiValue<Double>("trackId").toRealString().toInt()
    val data = json.getApiValue<LinkedTreeMap<Any, Any>>("data")
    return when (type) {
        "ready" -> AnalysisTask.Ready(trackId, data.getApiValue("localPath"))
        "processing" -> AnalysisTask.Processing(trackId, data.getApiValue("processingPercent"))
        "finished" -> AnalysisTask.Finished(trackId, parseFulfilledAnalysis(data))
        "failed" -> AnalysisTask.Failed(trackId)
        else -> throw UnknownApiException("Unknown AnalysisTask type: $type")
    }
}

fun parseAnalysisListUpdate(json: LinkedTreeMap<Any, Any>): AnalysisListUpdate {
    val type = json.getApiValue<String>("type")
    val data = json.getApiValue<LinkedTreeMap<Any, Any>>("data")
    return when (type) {
        "add" -> AnalysisListUpdate.AddItem(parseAnalysisTask(data.getApiValue("newItem")))
        "remove" -> AnalysisListUpdate.RemoteItem(parseAnalysisTask(data.getApiValue("oldItem")))
        "change" -> AnalysisListUpdate.ChangeItem(
            parseAnalysisTask(data.getApiValue("oldItem")),
            parseAnalysisTask(data.getApiValue("newItem"))
        )
        "no_change" -> AnalysisListUpdate.NoChange
        else -> throw UnknownApiException("Unknown AnalysisListUpdate type: $type")
    }
}

fun parseTrackAnalysisList(json: List<LinkedTreeMap<Any, Any>>): List<Pair<Int, TrackAnalysis>> {
    return json.map {
        val trackId = it.getApiValue<Double>("trackId").toRealString().toInt()
        val analysis = it.getApiValue<LinkedTreeMap<Any, Any>>("analysis")
        trackId to parseTrackAnalysis(analysis)
    }
}

fun parseTrackAnalysis(json: LinkedTreeMap<Any, Any>): TrackAnalysis {
    val type = json.getApiValue<String>("type")
    val data = json.getApiValue<LinkedTreeMap<Any, Any>>("data")
    return when (type) {
        "not_available" -> NotAvailableAnalysis
        "processing" -> ProcessingAnalysis(data.getApiValue("percent"))
        "fulfilled" -> parseFulfilledAnalysis(data)
        else -> throw UnknownApiException("Unknown TrackAnalysis type: $type")
    }
}

fun parseFulfilledAnalysis(json: LinkedTreeMap<Any, Any>): FulfilledAnalysis {
    return FulfilledAnalysis(
        bpm = json.getApiValue<Double>("bpm").toRealString().toInt(),
        key = json.getApiValue("key"),
        notePercents = mapOf(
            1 to json.getApiValue("x1Percent"),
            2 to json.getApiValue("x2Percent"),
            4 to json.getApiValue("x4Percent"),
            8 to json.getApiValue("x8Percent"),
        )
    )
}

fun parseStatus(response: String): String? {
    return try {
        val result = Gson().fromJson<LinkedTreeMap<Any, Any>>(response)
        result.getApiValue<String>("status")
    } catch (exc: Exception) {
        null
    }
}