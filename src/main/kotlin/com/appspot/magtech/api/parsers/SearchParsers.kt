package com.appspot.magtech.api.parsers

import com.appspot.magtech.entities.Album
import com.appspot.magtech.entities.Artist
import com.appspot.magtech.entities.YMusicTrack
import com.appspot.magtech.extensions.getApiValue
import com.google.gson.internal.LinkedTreeMap

data class FastSearchResult(val artists: List<Artist>, val albums: List<Album>, val tracks: List<YMusicTrack>)

@Suppress("UNUSED")
fun parseFastSearchResults(json: LinkedTreeMap<Any, Any>): FastSearchResult {
    val artistsList = mutableListOf<Artist>()
    val albumsList = mutableListOf<Album>()
    val tracksList = mutableListOf<YMusicTrack>()
    json
        .getApiValue<List<LinkedTreeMap<Any, Any>>>("entities")
        .forEach { entity ->
            val type = entity.getApiValue<String>("type")
            val results = entity.getApiValue<List<LinkedTreeMap<Any, Any>>>("results")

            when (type) {
                "artist" -> artistsList += results.map {
                    val artistRaw = it.getApiValue<LinkedTreeMap<Any, Any>>("artist")
                    parseArtist(artistRaw)
                }
                "track" -> tracksList += results.map {
                    val trackRaw = it.getApiValue<LinkedTreeMap<Any, Any>>("track")
                    parseTrackInfo(trackRaw)
                }
                "album" -> albumsList += results.map {
                    val albumRaw = it.getApiValue<LinkedTreeMap<Any, Any>>("album")
                    parseAlbum(albumRaw)
                }
            }
        }
    return FastSearchResult(artistsList, albumsList, tracksList)
}

fun parseTracksSearch(json: LinkedTreeMap<Any, Any>): List<YMusicTrack> {
    return json
        .getApiValue<LinkedTreeMap<Any, Any>>("tracks")
        .getApiValue<List<LinkedTreeMap<Any, Any>>>("items")
        .map { parseTrackInfo(it) }
}