package com.appspot.magtech.api.parsers

import com.appspot.magtech.api.UnknownApiException
import com.appspot.magtech.entities.*
import com.appspot.magtech.extensions.getApiValue
import com.appspot.magtech.extensions.getWithVariants
import com.appspot.magtech.extensions.toRealString
import com.google.gson.internal.LinkedTreeMap

fun parseTrackInfo(track: LinkedTreeMap<Any, Any>): YMusicTrack {
    val id = track.getWithVariants(
        { track.getApiValue<String>("realId").toInt() },
        { track.getApiValue<Double>("id").toRealString().toInt() }
    )
    val title = track.getApiValue<String>("title")
    val artists = track
        .getApiValue<List<LinkedTreeMap<Any, Any>>>("artists")
        .map { parseArtist(it) }
    val available = track.getApiValue<Boolean>("available")

    val duration = if (available) {
        track.getApiValue<Double>("durationMs").toRealString().toLong()
    } else 0

    val coverUri = if (available) {
        track.getWithVariants(
            { track.getApiValue<String>("coverUri") },
            { "" }
        )
    } else ""

    val albums = if (available) {
        track.getApiValue<List<LinkedTreeMap<Any, Any>>>("albums").map { parseAlbum(it) }
    } else listOf()

    return YMusicTrack(
        id = id,
        available = available,
        info = TrackInfo(
            title = title,
            duration = duration,
            coverUri = coverUri,
            artists = artists,
            albums = albums
        )
    )
}

fun parseTracks(tracks: List<LinkedTreeMap<Any, Any>>, withDump: Boolean = true): List<YMusicTrack> {
    return tracks.mapNotNull { tr ->
        try {
            parseTrackInfo(tr)
        } catch (exc: Exception) {
            if (withDump) {
                println(exc.message)
                println(tr)
            }
            null
        }
    }
}

fun parseArtist(json: LinkedTreeMap<Any, Any>): Artist {
    val id = json.getWithVariants(
        { json.getApiValue<Double>("id").toRealString().toInt() },
        { json.getApiValue<String>("id").toInt() }
    )
    val name = json.getApiValue<String>("name")
    val uri: String = try {
        val cover = json.getApiValue<LinkedTreeMap<Any, Any>>("cover")
        cover.getApiValue("uri")
    } catch (exc: UnknownApiException) {
        ""
    }
    return Artist(
        id = id,
        name = name,
        coverUri = uri
    )
}

fun parseAlbum(json: LinkedTreeMap<Any, Any>): Album {
    val id = json.getApiValue<Double>("id").toRealString().toInt()
    val title = json.getApiValue<String>("title")
    val coverUri = try {
        json.getApiValue<String>("coverUri")
    } catch (exc: Exception) {
        ""
    }
    val trackCount = json.getApiValue<Double>("trackCount").toRealString().toInt()
    return Album(
        id = id,
        title = title,
        coverUri = coverUri,
        trackCount = trackCount
    )
}

@Suppress("UNUSED")
fun parseTrackVideos(json: LinkedTreeMap<Any, Any>): List<TrackVideo> {
    val videos = json
        .getApiValue<List<LinkedTreeMap<Any, Any>>>("videos")
        .map {
            val cover = it.getApiValue<String>("cover")
            val title = it.getApiValue<String>("title")
            try {
                val url = it.getApiValue<String>("url")
                val embed = it.getApiValue<String>("embed")
                VideoLink(
                    title = title,
                    cover = cover,
                    url = url,
                    embed = embed,
                )
            } catch (exc: Exception) {
                val embedUrl = it.getApiValue<String>("embedUrl")
                EmbedVideo(
                    title = title,
                    cover = cover,
                    embedUrl = embedUrl,
                )
            }
        }
    return videos
}

fun parseTrackLyrics(json: LinkedTreeMap<Any, Any>): List<TrackLyrics> {
    val lyric = json
        .getApiValue<List<LinkedTreeMap<Any, Any>>>("lyric")
        .map {
            val id = it.getApiValue<Double>("id").toRealString().toInt()
            val fullLyrics = it.getApiValue<String>("fullLyrics")
            val lyrics = it.getApiValue<String>("lyrics")
            TrackLyrics(
                id = id,
                fullLyrics = fullLyrics,
                lyrics = lyrics,
            )
        }
    return lyric
}
