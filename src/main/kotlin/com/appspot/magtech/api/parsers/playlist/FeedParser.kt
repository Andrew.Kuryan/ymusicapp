package com.appspot.magtech.api.parsers.playlist

import com.appspot.magtech.api.parsers.parseTracks
import com.appspot.magtech.entities.FeedPlaylist
import com.appspot.magtech.entities.PlaylistInfo
import com.appspot.magtech.entities.YMusicTrack
import com.appspot.magtech.extensions.getApiValue
import com.google.gson.internal.LinkedTreeMap

fun parseFeed(json: LinkedTreeMap<Any, Any>): List<Pair<FeedPlaylist, List<YMusicTrack>>> {
    val generatedPlaylists =
            json.getApiValue<List<LinkedTreeMap<Any, Any>>>("generatedPlaylists")
    return generatedPlaylists.map { parseFeedPlaylist(it) }
}

fun parseFeedPlaylist(json: LinkedTreeMap<Any, Any>): Pair<FeedPlaylist, List<YMusicTrack>> {
    val playlist = json.getApiValue<LinkedTreeMap<Any, Any>>("data")
    val kind = playlist.getApiValue<Double>("kind").toInt()
    val title = playlist.getApiValue<String>("title")
    val trackCount = playlist.getApiValue<Double>("trackCount").toInt()
    val duration = playlist.getApiValue<Double>("durationMs").toLong()

    val tracks = parseTracks(playlist.getApiValue("tracks"))
    return FeedPlaylist(
            kind = kind,
            title = title,
            info = PlaylistInfo(
                    duration = duration,
                    trackCount = trackCount
            )
    ) to tracks
}

fun parseFeedPlaylistTracks(json: LinkedTreeMap<Any, Any>, playlist: FeedPlaylist): List<YMusicTrack> {
    val generatedPlaylists =
            json.getApiValue<List<LinkedTreeMap<Any, Any>>>("generatedPlaylists")
    val rawPlaylist = generatedPlaylists.find {
        val playlistData = it.getApiValue<LinkedTreeMap<Any, Any>>("data")
        val kind = playlistData.getApiValue<Double>("kind").toInt()
        kind == playlist.kind
    }
    return parseFeedPlaylist(rawPlaylist!!).second
}
