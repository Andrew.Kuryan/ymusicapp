package com.appspot.magtech.api.parsers.playlist

import com.appspot.magtech.api.parsers.parseTracks
import com.appspot.magtech.entities.*
import com.appspot.magtech.extensions.getApiValue
import com.appspot.magtech.extensions.getWithVariants
import com.google.gson.internal.LinkedTreeMap

sealed class ParseSavedPlaylistResult {
    data class Success(val playlist: UserPlaylist) : ParseSavedPlaylistResult()
    data class Failed(val kind: Int) : ParseSavedPlaylistResult()
}

fun parseSavedPlaylists(json: LinkedTreeMap<Any, Any>): List<ParseSavedPlaylistResult> {
    val playlists = json
            .getApiValue<List<LinkedTreeMap<Any, Any>>>("playlists")
    return playlists.map { parseSavedPlaylist(it) }
}

fun parseSavedPlaylist(playlist: LinkedTreeMap<Any, Any>): ParseSavedPlaylistResult {
    val kind = playlist.getApiValue<Double>("kind").toInt()
    try {
        val title = playlist.getApiValue<String>("title")
        val trackCount = playlist.getApiValue<Double>("trackCount").toInt()
        val duration = playlist.getApiValue<Double>("durationMs").toLong()
        val visibility = parsePlaylistVisibility(playlist)

        return ParseSavedPlaylistResult.Success(
                playlist = UserPlaylist(
                        kind = kind,
                        title = title,
                        visibility = visibility,
                        info = PlaylistInfo(
                                trackCount = trackCount,
                                duration = duration
                        )
                )
        )
    } catch (exc: Exception) {
        return ParseSavedPlaylistResult.Failed(kind)
    }
}

fun parseSavedPlaylistDetailedInfo(json: LinkedTreeMap<Any, Any>): Pair<UserPlaylist, List<YMusicTrack>> {
    val playlist = json.getApiValue<LinkedTreeMap<Any, Any>>("playlist")
    val kind = playlist.getApiValue<Double>("kind").toInt()
    val title = playlist.getApiValue<String>("title")
    val trackCount = playlist.getApiValue<Double>("trackCount").toInt()
    val visibility = parsePlaylistVisibility(playlist)

    val tracks = parseTracks(playlist.getApiValue("tracks"))

    return UserPlaylist(
            kind = kind,
            title = title,
            visibility = visibility,
            info = PlaylistInfo(
                    trackCount = trackCount,
                    duration = tracks.fold<Track, Long>(0) { acc, track -> acc + track.info.duration }
            )
    ) to tracks
}

fun parseSavedPlaylistTracks(json: LinkedTreeMap<Any, Any>): List<YMusicTrack> {
    return parseSavedPlaylistDetailedInfo(json).second
}

private fun parsePlaylistVisibility(playlist: LinkedTreeMap<Any, Any>): PlaylistVisibility {
    return playlist.getWithVariants(
            {
                when (getApiValue<String>("visibility")) {
                    "private" -> PlaylistVisibility.PRIVATE
                    "public" -> PlaylistVisibility.PUBLIC
                    else -> PlaylistVisibility.PUBLIC
                }
            },
            { PlaylistVisibility.PUBLIC }
    )
}
