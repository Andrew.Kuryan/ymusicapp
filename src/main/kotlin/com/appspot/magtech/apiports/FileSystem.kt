package com.appspot.magtech.apiports

interface FileSystem {

    fun getAbsolutePath(relativePath: String): String
    fun getURI(relativePath: String): String

    suspend fun writeBytes(path: String, bytes: ByteArray)
    suspend fun readAllBytes(path: String): ByteArray

    suspend fun writeLines(path: String, lines: List<String>)
    suspend fun readAllLines(path: String): List<String>

    suspend fun deleteFile(path: String)
}
