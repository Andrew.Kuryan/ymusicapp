package com.appspot.magtech.apiports

import com.appspot.magtech.entities.*

interface YMusicApi {

    val localApi: LocalApi
    val authApi: AuthApi
    val detectorApi: DetectorApi

    suspend fun getSavedPlaylists(user: User): List<Pair<UserPlaylist, List<YMusicTrack>?>>
    suspend fun getSelectedPlaylists(user: User): List<Pair<FeedPlaylist, List<YMusicTrack>>>
    suspend fun getPlaylistTracks(auth: Auth, playlist: Playlist): List<Track>

    suspend fun getTrackLink(auth: Auth, track: Track): String
    suspend fun getTrackDetailedInfo(user: User, track: Track): TrackDetailedInfo
    fun getImageLink(rawLink: String, size: Int): String

    suspend fun searchTracks(user: User, query: String): List<YMusicTrack>
}

interface DetectorApi {
    suspend fun connectToDetector(afterConnect: suspend () -> Unit)
    suspend fun notifyDetector(newTrack: LocalTrack)
    suspend fun getTrackAnalysis(track: Track): TrackAnalysis
    suspend fun removeTrackAnalysis(track: Track)
    suspend fun getAllTrackAnalysis(): List<Pair<Int, TrackAnalysis>>
}

interface AuthApi {
    suspend fun authorize(username: String, password: String): User
}

interface LocalApi {

    suspend fun downloadTrack(user: User, track: YMusicTrack): LocalTrack
    suspend fun removeTrack(track: LocalTrack): YMusicTrack

    suspend fun getLocalPlaylists(): List<Pair<LocalPlaylist, List<LocalTrack>>>
    suspend fun getLocalPlaylistTracks(playlist: LocalPlaylist): List<LocalTrack>

    suspend fun saveUser(user: User, isActive: Boolean): User
    suspend fun getSavedUsers(): List<Pair<User, Boolean>>
    suspend fun removeUser(user: User): User
    suspend fun markUserAsActive(user: User): User
    suspend fun unmarkUserAsActive(user: User): User
}
