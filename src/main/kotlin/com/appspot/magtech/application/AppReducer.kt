package com.appspot.magtech.application

import com.appspot.magtech.application.account.AccountAction
import com.appspot.magtech.application.account.AccountState
import com.appspot.magtech.application.account.accountReducer
import com.appspot.magtech.application.navigation.NavigationAction
import com.appspot.magtech.application.navigation.NavigationState
import com.appspot.magtech.application.navigation.navigationReducer
import com.appspot.magtech.application.player.PlayerAction
import com.appspot.magtech.application.player.PlayerState
import com.appspot.magtech.application.player.playerReducer
import com.appspot.magtech.application.section.SectionAction
import com.appspot.magtech.application.section.SectionState
import com.appspot.magtech.application.section.sectionReducer
import com.appspot.magtech.application.tracklyrics.TrackLyricsAction
import com.appspot.magtech.application.tracklyrics.TrackLyricsState
import com.appspot.magtech.application.tracklyrics.trackLyricsReducer
import com.appspot.magtech.application.tracks.*
import org.reduxkotlin.Reducer
import org.reduxkotlin.Store
import org.reduxkotlin.middleware

interface Action

inline fun <S, reified A> middlewareForAction(
    crossinline actionMiddleware: (store: Store<S>, action: A) -> Action
) = middleware<S> { store, next, action ->
    val result = when (action) {
        is A -> actionMiddleware(store, action)
        else -> action
    }
    next(result)
}

sealed class AppAction : Action {
    data class SetError(val err: java.lang.Exception) : AppAction()
    object ResetError : AppAction()
    data class SetMessage(val message: String) : AppAction()
}

data class AppState(
    val accountState: AccountState = AccountState(),
    val sectionsState: SectionState = SectionState(),
    val tracksState: TracksState = TracksState(),
    val playerState: PlayerState = PlayerState(),
    val trackLyricsState: TrackLyricsState = TrackLyricsState(),
    val navigationState: NavigationState = NavigationState(),
    val searchState: SearchState = SearchState(),
    val error: Exception? = null,
    val infoMessage: String? = null
)

val appReducer: Reducer<AppState> = { state, action ->
    when (action) {
        is AccountAction -> state.copy(accountState = accountReducer(state.accountState, action))
        is SectionAction -> state.copy(sectionsState = sectionReducer(state.sectionsState, action))
        is TracksAction -> state.copy(tracksState = tracksReducer(state.tracksState, action))
        is PlayerAction -> state.copy(playerState = playerReducer(state.playerState, action))
        is TrackLyricsAction -> state.copy(trackLyricsState = trackLyricsReducer(state.trackLyricsState, action))
        is NavigationAction -> state.copy(navigationState = navigationReducer(state.navigationState, action))
        is SearchAction -> state.copy(searchState = searchReducer(state.searchState, action))
        is AppAction.SetError -> state.copy(error = action.err)
        is AppAction.ResetError -> state.copy(error = null)
        is AppAction.SetMessage -> state.copy(infoMessage = action.message)
        else -> state
    }
}
