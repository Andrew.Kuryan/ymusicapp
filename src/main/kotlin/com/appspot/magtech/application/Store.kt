package com.appspot.magtech.application

import com.appspot.magtech.apiports.YMusicApi
import com.appspot.magtech.application.account.*
import com.appspot.magtech.application.navigation.navigateToHomeAfterLoginMiddleware
import com.appspot.magtech.application.navigation.navigateToLoginIfSavedUsersEmptyMiddleware
import com.appspot.magtech.application.player.fetchTrackLinkMiddleware
import com.appspot.magtech.application.player.selectNextTrackMiddleware
import com.appspot.magtech.application.player.selectPreviousTrackMiddleware
import com.appspot.magtech.application.section.feedsMiddleware
import com.appspot.magtech.application.section.local.*
import com.appspot.magtech.application.section.savedPlaylistsMiddleware
import com.appspot.magtech.application.tracklyrics.connectDetectorMiddleware
import com.appspot.magtech.application.tracklyrics.trackLyricsChangeTrackMiddleware
import com.appspot.magtech.application.tracklyrics.trackLyricsMiddleware
import com.appspot.magtech.application.tracks.cacheTracksMiddleware
import com.appspot.magtech.application.tracks.fetchTracksMiddleware
import com.appspot.magtech.application.tracks.setQueryMiddleware
import com.appspot.magtech.application.util.loggerMiddleware
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import org.reduxkotlin.applyMiddleware
import org.reduxkotlin.createThreadSafeStore

@ExperimentalCoroutinesApi
fun configureStore(api: YMusicApi, apiActions: Channel<Action>): Pair<Flow<AppState>, Channel<Action>> {
    val store = createThreadSafeStore(
        appReducer,
        AppState(),
        applyMiddleware(
            loggerMiddleware(false),
            authMiddleware(api),
            savedPlaylistsMiddleware(api),
            feedsMiddleware(api),
            downloadsMiddleware(api),
            fetchLocalPlaylistsMiddleware(api),
            fetchLocalPlaylistsMiddlewareWithoutAnalysis(api),
            markTracksAsDownloadedMiddleware,
            downloadTrackMiddleware(api),
            removeTrackMiddleware(api),
            fetchTracksMiddleware(api),
            downloadTrackSuccessMiddleware(api),
            cacheTracksMiddleware,
            fetchTrackLinkMiddleware(api),
            selectNextTrackMiddleware,
            selectPreviousTrackMiddleware,
            trackLyricsMiddleware(api),
            trackLyricsChangeTrackMiddleware(api),
            connectDetectorMiddleware(api),
            fetchSavedUsersMiddleware(api),
            removeUserMiddleware(api),
            markAuthUserAsActive(api),
            logoutMiddleware(api),
            navigateToHomeAfterLoginMiddleware,
            navigateToLoginIfSavedUsersEmptyMiddleware,
            setQueryMiddleware(api),
        )
    )

    val states = callbackFlow<AppState> {
        val unsubscribe = store.subscribe {
            sendBlocking(store.state)
        }
        sendBlocking(store.state)

        awaitClose { unsubscribe() }
    }

    val actions = Channel<Action>()
    CoroutineScope(Dispatchers.Main).launch {
        actions.receiveAsFlow().collect {
            store.dispatch(it)
        }
    }
    CoroutineScope(Dispatchers.Main).launch {
        apiActions.receiveAsFlow().collect {
            store.dispatch(it)
        }
    }

    return states to actions
}
