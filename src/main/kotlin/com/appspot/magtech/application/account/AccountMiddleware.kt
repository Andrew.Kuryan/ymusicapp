package com.appspot.magtech.application.account

import com.appspot.magtech.apiports.YMusicApi
import com.appspot.magtech.application.AppAction
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.middlewareForAction
import com.appspot.magtech.entities.User
import com.appspot.magtech.extensions.ApiRequestException
import com.appspot.magtech.extensions.AuthorizationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

val authMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, AccountAction.Auth> { store, action ->
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val result = withContext(Dispatchers.IO) {
                    api.authApi.authorize(action.login, action.password)
                }
                if (action.needToSave) {
                    try {
                        val newUser = withContext(Dispatchers.IO) {
                            api.localApi.saveUser(result, true)
                        }
                        store.dispatch(AccountAction.SaveUserSuccess(newUser))
                    } catch (exc: Exception) {
                        store.dispatch(AppAction.SetError(ApiRequestException("Can't save user")))
                    }
                }
                store.dispatch(AccountAction.AuthSuccess(result))
            } catch (exc: Exception) {
                store.dispatch(AccountAction.AuthFailed)
                store.dispatch(AppAction.SetError(AuthorizationException()))
            }
        }
        action
    }
}

val removeUserMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, AccountAction.RemoveUser> { store, action ->
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val user = withContext(Dispatchers.IO) { api.localApi.removeUser(action.user) }
                store.dispatch(AccountAction.RemoveUserSuccess(user))
            } catch (exc: Exception) {
                store.dispatch(AppAction.SetError(ApiRequestException("Can't remove user")))
            }
        }
        action
    }
}

val fetchSavedUsersMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, AccountAction.FetchSavedUsers> { store, action ->
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val users = withContext(Dispatchers.IO) { api.localApi.getSavedUsers() }
                users.forEach { (user, isActive) ->
                    if (isActive) {
                        store.dispatch(AccountAction.AuthSuccess(user))
                    }
                }
                store.dispatch(AccountAction.FetchSavedUsersSuccess(users))
            } catch (exc: Exception) {
                store.dispatch(AppAction.SetError(ApiRequestException("Can't fetch saved users")))
            }
        }
        action
    }
}

val logoutMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, AccountAction.Logout> { store, action ->
        val currentUser = store.state.accountState.auth
        CoroutineScope(Dispatchers.Main).launch {
            if (currentUser is User) {
                try {
                    withContext(Dispatchers.IO) { api.localApi.unmarkUserAsActive(currentUser) }
                } catch (exc: Exception) {
                    store.dispatch(AppAction.SetError(ApiRequestException("Can't access local DB")))
                }
            }
        }
        action
    }
}

val markAuthUserAsActive = { api: YMusicApi ->
    middlewareForAction<AppState, AccountAction.AuthSuccess> { store, action ->
        CoroutineScope(Dispatchers.Main).launch {
            try {
                withContext(Dispatchers.IO) { api.localApi.markUserAsActive(action.user) }
            } catch (exc: Exception) {
                store.dispatch(AppAction.SetError(ApiRequestException("Can't access local DB")))
            }
        }
        action
    }
}