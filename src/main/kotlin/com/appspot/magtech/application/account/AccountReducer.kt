package com.appspot.magtech.application.account

import com.appspot.magtech.application.Action
import com.appspot.magtech.entities.Auth
import com.appspot.magtech.entities.Offline
import com.appspot.magtech.entities.User
import org.reduxkotlin.ReducerForActionType

sealed class AccountAction : Action {
    data class Auth(val login: String, val password: String, val needToSave: Boolean) : AccountAction()
    data class AuthSuccess(val user: User) : AccountAction()
    object AuthFailed : AccountAction()

    object FetchSavedUsers : AccountAction()
    data class FetchSavedUsersSuccess(val users: List<Pair<User, Boolean>>) : AccountAction()

    object Logout : AccountAction()
    data class RemoveUser(val user: User) : AccountAction()
    data class RemoveUserSuccess(val user: User) : AccountAction()

    data class SaveUserSuccess(val user: User) : AccountAction()
}

data class AccountState(
    val auth: Auth = Offline,
    val isFetching: Boolean = false,
    val savedUsers: List<User> = listOf(),
)

val accountReducer: ReducerForActionType<AccountState, AccountAction> =
    { state, action ->
        when (action) {
            is AccountAction.Auth -> state.copy(isFetching = true)
            is AccountAction.AuthSuccess -> state.copy(auth = action.user, isFetching = false)
            is AccountAction.AuthFailed -> state.copy(auth = Offline, isFetching = false)
            is AccountAction.Logout -> state.copy(auth = Offline)

            is AccountAction.FetchSavedUsers -> state
            is AccountAction.FetchSavedUsersSuccess -> state.copy(savedUsers = action.users.map { it.first })

            is AccountAction.RemoveUser -> state
            is AccountAction.RemoveUserSuccess -> state.copy(savedUsers = state.savedUsers.filter { it != action.user })

            is AccountAction.SaveUserSuccess -> state.copy(savedUsers = state.savedUsers + listOf(action.user))
        }
    }
