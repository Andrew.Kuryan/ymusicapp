package com.appspot.magtech.application.navigation

import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.account.AccountAction
import com.appspot.magtech.application.middlewareForAction

val navigateToHomeAfterLoginMiddleware = middlewareForAction<AppState, AccountAction.AuthSuccess> { store, action ->
    store.dispatch(NavigationAction.NavigateTo(NavigationRoute.HOME))
    action
}

val navigateToLoginIfSavedUsersEmptyMiddleware =
    middlewareForAction<AppState, NavigationAction.NavigateTo> { store, action ->
        if (action.route == NavigationRoute.SESSIONS && store.state.accountState.savedUsers.isEmpty()) {
            NavigationAction.NavigateTo(NavigationRoute.LOGIN)
        } else {
            action
        }
    }