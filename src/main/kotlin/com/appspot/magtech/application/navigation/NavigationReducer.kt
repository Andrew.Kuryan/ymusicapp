package com.appspot.magtech.application.navigation

import com.appspot.magtech.application.Action
import org.reduxkotlin.ReducerForActionType

sealed class NavigationAction : Action {
    data class NavigateTo(val route: NavigationRoute) : NavigationAction()
}

enum class NavigationRoute { SESSIONS, LOGIN, HOME, }

data class NavigationState(
    val previousRoute: NavigationRoute? = null,
    val currentRoute: NavigationRoute = NavigationRoute.HOME
)

val navigationReducer: ReducerForActionType<NavigationState, NavigationAction> =
    { state, action ->
        when (action) {
            is NavigationAction.NavigateTo -> state.copy(
                previousRoute = state.currentRoute,
                currentRoute = action.route,
            )
        }
    }

