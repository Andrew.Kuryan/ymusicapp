package com.appspot.magtech.application.player

import com.appspot.magtech.apiports.YMusicApi
import com.appspot.magtech.application.AppAction
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.middlewareForAction
import com.appspot.magtech.application.tracks.getTrackById
import com.appspot.magtech.entities.isAvailable
import com.appspot.magtech.extensions.ApiRequestException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

val fetchTrackLinkMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, PlayerAction.SelectTrack> { store, action ->
        CoroutineScope(Dispatchers.Main).launch {
            val auth = store.state.accountState.auth
            try {
                val trackEl = store.state.tracksState
                        .getTrackById(action.trackId) ?: throw NoSuchElementException()
                val trackLink = withContext(Dispatchers.IO) {
                    api.getTrackLink(auth, trackEl.track)
                }
                val imageLink = api.getImageLink(trackEl.track.info.coverUri, 100)
                store.dispatch(PlayerAction.FetchTrackLinkSuccess(trackLink, imageLink))
            } catch (exc: Exception) {
                store.dispatch(PlayerAction.FetchTrackLinkFailed)
                store.dispatch(AppAction.SetError(ApiRequestException("Can't get track link")))
            }
        }
        action
    }
}

fun getCurrentTracksInfo(state: AppState): Pair<List<Int>?, Int?> {
    val currentTracksId = state.tracksState
            .tracks[state.playerState.selectedTrackPlaylistTitle]
            ?.filter { it.track.isAvailable() }
            ?.map { it.track.id }
    val currentIndex = currentTracksId?.indexOf(state.playerState.selectedTrackId)
    return currentTracksId to currentIndex
}

val selectNextTrackMiddleware =
        middlewareForAction<AppState, PlayerAction.SelectNextTrack> { store, action ->
            val (currentTracksId, currentIndex) = getCurrentTracksInfo(store.state)

            if (currentTracksId != null && currentIndex != null) {
                CoroutineScope(Dispatchers.Main).launch {
                    when (currentIndex) {
                        currentTracksId.size - 1 -> store.dispatch(PlayerAction.SelectTrack(
                                currentTracksId.first(),
                                store.state.playerState.selectedTrackPlaylistTitle!!
                        ))
                        else -> store.dispatch(PlayerAction.SelectTrack(
                                currentTracksId[currentIndex + 1],
                                store.state.playerState.selectedTrackPlaylistTitle!!
                        ))
                    }
                }
            }
            action
        }

val selectPreviousTrackMiddleware =
        middlewareForAction<AppState, PlayerAction.SelectPreviousTrack> { store, action ->
            val (currentTracksId, currentIndex) = getCurrentTracksInfo(store.state)

            if (currentTracksId != null && currentIndex != null) {
                CoroutineScope(Dispatchers.Main).launch {
                    when (currentIndex) {
                        0 -> store.dispatch(PlayerAction.SelectTrack(
                                currentTracksId.last(),
                                store.state.playerState.selectedTrackPlaylistTitle!!
                        ))
                        else -> store.dispatch(PlayerAction.SelectTrack(
                                currentTracksId[currentIndex - 1],
                                store.state.playerState.selectedTrackPlaylistTitle!!
                        ))
                    }
                }
            }
            action
        }
