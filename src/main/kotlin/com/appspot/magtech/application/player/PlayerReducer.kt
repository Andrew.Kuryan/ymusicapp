package com.appspot.magtech.application.player

import com.appspot.magtech.application.Action
import org.reduxkotlin.ReducerForActionType

enum class PlayerStatus { STOPPED, PLAYING, PAUSED }

sealed class PlayerAction : Action {
    data class SelectTrack(val trackId: Int, val playlistTitle: String) : PlayerAction()
    data class FetchTrackLinkSuccess(val trackLink: String, val imageLink: String) : PlayerAction()
    object FetchTrackLinkFailed : PlayerAction()

    object Start : PlayerAction()
    object Stop : PlayerAction()
    object Pause : PlayerAction()
    object Toggle : PlayerAction()

    object SelectNextTrack : PlayerAction()
    object SelectPreviousTrack : PlayerAction()
}

data class PlayerState(
        val isFetching: Boolean = false,
        val selectedTrackId: Int? = null,
        val selectedTrackPlaylistTitle: String? = null,
        val selectedTrackLink: String? = null,
        val selectedTrackImageLink: String? = null,
        val status: PlayerStatus = PlayerStatus.STOPPED
)

val playerReducer: ReducerForActionType<PlayerState, PlayerAction> = { state, action ->
    when (action) {
        is PlayerAction.SelectTrack -> state.copy(
                selectedTrackId = action.trackId,
                selectedTrackPlaylistTitle = action.playlistTitle,
                selectedTrackLink = null,
                selectedTrackImageLink = null,
                isFetching = true
        )
        is PlayerAction.FetchTrackLinkSuccess -> state.copy(
                selectedTrackLink = action.trackLink,
                selectedTrackImageLink = action.imageLink,
                isFetching = false,
                status = if (state.status == PlayerStatus.PLAYING) state.status else PlayerStatus.STOPPED
        )
        is PlayerAction.FetchTrackLinkFailed -> state.copy(
                isFetching = false,
                status = PlayerStatus.STOPPED
        )

        is PlayerAction.Start -> state.copy(status = PlayerStatus.PLAYING)
        is PlayerAction.Stop -> state.copy(status = PlayerStatus.STOPPED)
        is PlayerAction.Pause -> state.copy(status = PlayerStatus.PAUSED)
        is PlayerAction.Toggle -> state.copy(status = when (state.status) {
            PlayerStatus.STOPPED -> PlayerStatus.PLAYING
            PlayerStatus.PLAYING -> PlayerStatus.PAUSED
            PlayerStatus.PAUSED -> PlayerStatus.PLAYING
        })

        is PlayerAction.SelectNextTrack, is PlayerAction.SelectPreviousTrack -> state
    }
}
