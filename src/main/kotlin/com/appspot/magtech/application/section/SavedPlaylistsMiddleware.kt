package com.appspot.magtech.application.section

import com.appspot.magtech.apiports.YMusicApi
import com.appspot.magtech.application.AppAction
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.middlewareForAction
import com.appspot.magtech.entities.User
import com.appspot.magtech.extensions.ApiRequestException
import com.appspot.magtech.extensions.NotAuthorizedException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

const val SAVED_PLAYLISTS_TITLE = "Saved"

val savedPlaylistsMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, SectionAction.SelectSection> { store, action ->
        if (action.sectionTitle == SAVED_PLAYLISTS_TITLE) {
            CoroutineScope(Dispatchers.Main).launch {
                if (store.state.sectionsState.playlists.getValue(SAVED_PLAYLISTS_TITLE)
                        .isNotEmpty() && !action.reload
                ) {
                    store.dispatch(
                        SectionAction.FetchSuccess(
                            SAVED_PLAYLISTS_TITLE,
                            store.state.sectionsState.playlists.getValue(SAVED_PLAYLISTS_TITLE).map { it to null },
                            false,
                        )
                    )
                } else {
                    if (store.state.accountState.auth is User) {
                        store.dispatch(SectionAction.StartFetching)
                        try {
                            val user = store.state.accountState.auth as User
                            val playlists = withContext(Dispatchers.IO) {
                                api.getSavedPlaylists(user)
                            }
                            store.dispatch(SectionAction.FetchSuccess(SAVED_PLAYLISTS_TITLE, playlists, action.reload))
                        } catch (exc: Exception) {
                            store.dispatch(SectionAction.FetchFailed)
                            store.dispatch(AppAction.SetError(ApiRequestException("Can't load saved playlists")))
                        }
                    } else {
                        store.dispatch(AppAction.SetError(NotAuthorizedException()))
                    }
                }
            }
        }
        action
    }
}
