package com.appspot.magtech.application.section

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.section.local.DOWNLOADS_TITLE
import com.appspot.magtech.entities.LocalPlaylist
import com.appspot.magtech.entities.Playlist
import com.appspot.magtech.entities.Track
import org.reduxkotlin.ReducerForActionType

val availableSections = listOf(SAVED_PLAYLISTS_TITLE, FEEDS_TITLE, DOWNLOADS_TITLE)

sealed class SectionAction : Action {
    object StartFetching : SectionAction()
    data class SelectSection(val sectionTitle: String, val reload: Boolean) : SectionAction()
    data class FetchSuccess(
        val sectionTitle: String,
        val playlists: List<Pair<Playlist, List<Track>?>>,
        val reload: Boolean
    ) : SectionAction()

    data class UpdatePlaylistInfo(val sectionTitle: String, val title: String, val newPlaylist: Playlist) :
        SectionAction()

    object FetchFailed : SectionAction()
}

data class SectionState(
    val isFetching: Boolean = false,
    val playlists: Map<String, List<Playlist>> = availableSections.map { it to listOf<Playlist>() }.toMap(),
    val selectedSectionTitle: String = availableSections.first()
)

val sectionReducer: ReducerForActionType<SectionState, SectionAction> = { state, action ->
    when (action) {
        is SectionAction.StartFetching -> state.copy(isFetching = true)
        is SectionAction.SelectSection -> state.copy(selectedSectionTitle = action.sectionTitle)
        is SectionAction.FetchSuccess -> state
            .setPlaylists(action.sectionTitle, action.playlists.map { it.first })
            .copy(isFetching = false)
        is SectionAction.UpdatePlaylistInfo -> state
            .updatePlaylistInfo(
                sectionTitle = action.sectionTitle,
                playlistTitle = action.title,
                newPlaylist = action.newPlaylist
            )
        is SectionAction.FetchFailed -> state.copy(isFetching = false)
    }
}

fun SectionState.getLocalPlaylists() = playlists.values
    .flatten()
    .filterIsInstance<LocalPlaylist>()

fun SectionState.getPlaylistByTitle(title: String?) = playlists.values
    .flatten()
    .find { it.title == title }

fun SectionState.setPlaylists(sectionTitle: String, newPlaylists: List<Playlist>): SectionState =
    copy(playlists = playlists.mapValues {
        if (it.key == sectionTitle) {
            newPlaylists
        } else {
            it.value
        }
    })

fun SectionState.updatePlaylistInfo(
    sectionTitle: String,
    playlistTitle: String,
    newPlaylist: Playlist
): SectionState = copy(playlists = playlists.mapValues {
    if (it.key == sectionTitle) {
        it.value.map { playlist ->
            if (playlist.title == playlistTitle) {
                newPlaylist
            } else {
                playlist
            }
        }
    } else {
        it.value
    }
})
