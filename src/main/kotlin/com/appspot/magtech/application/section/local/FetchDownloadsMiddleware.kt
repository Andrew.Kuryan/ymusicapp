package com.appspot.magtech.application.section.local

import com.appspot.magtech.apiports.YMusicApi
import com.appspot.magtech.application.AppAction
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.middlewareForAction
import com.appspot.magtech.application.section.SectionAction
import com.appspot.magtech.application.section.getLocalPlaylists
import com.appspot.magtech.application.tracklyrics.TrackLyricsAction
import com.appspot.magtech.application.tracks.TracksAction
import com.appspot.magtech.entities.LocalTrack
import com.appspot.magtech.extensions.ApiRequestException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

val markTracksAsDownloadedMiddleware =
    middlewareForAction<AppState, TracksAction.FetchSuccess> { store, action ->
        val localPlaylistTitles = store.state.sectionsState
            .getLocalPlaylists()
            .map { it.title }
        val localTracks = localPlaylistTitles
            .map { store.state.tracksState.tracks[it] ?: listOf() }
            .flatten()
            .map { it.track as LocalTrack }
        action.copy(tracks = action.tracks.map { trackEl ->
            when (val track = localTracks.find { it.id == trackEl.track.id }) {
                null -> trackEl
                else -> trackEl.copy(track = track)
            }
        })
    }

val downloadsMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, SectionAction.SelectSection> { store, action ->
        if (action.sectionTitle == DOWNLOADS_TITLE) {
            CoroutineScope(Dispatchers.Main).launch {
                if (store.state.sectionsState.playlists.getValue(DOWNLOADS_TITLE).isNotEmpty() && !action.reload) {
                    store.dispatch(
                        SectionAction.FetchSuccess(
                            sectionTitle = DOWNLOADS_TITLE,
                            playlists = store.state.sectionsState.playlists.getValue(DOWNLOADS_TITLE)
                                .map { it to null },
                            false,
                        )
                    )
                } else {
                    store.dispatch(SectionAction.StartFetching)
                    try {
                        val playlists = withContext(Dispatchers.IO) {
                            api.localApi.getLocalPlaylists()
                        }
                        store.dispatch(SectionAction.FetchSuccess(DOWNLOADS_TITLE, playlists, action.reload))
                    } catch (exc: Exception) {
                        store.dispatch(SectionAction.FetchFailed)
                        store.dispatch(AppAction.SetError(ApiRequestException("Can't load downloads")))
                    }
                }
            }
        }
        action
    }
}

val fetchLocalPlaylistsMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, TrackLyricsAction.ConnectToDetectorSuccess> { store, action ->
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val localPlaylists = withContext(Dispatchers.IO) {
                    api.localApi.getLocalPlaylists()
                }
                val trackAnalysis = withContext(Dispatchers.IO) {
                    api.detectorApi.getAllTrackAnalysis()
                }.toMap()
                val combinedPlaylists = localPlaylists
                    .map { (playlist, tracks) ->
                        playlist to tracks.map {
                            if (trackAnalysis[it.id] != null) {
                                it.copy(analysis = trackAnalysis.getValue(it.id))
                            } else {
                                it
                            }
                        }
                    }
                println("Fetch downloads success")
                store.dispatch(SectionAction.FetchSuccess(DOWNLOADS_TITLE, combinedPlaylists, false))
            } catch (exc: Exception) {
                store.dispatch(AppAction.SetError(ApiRequestException("Can't load local playlists")))
            }
        }
        action
    }
}

val fetchLocalPlaylistsMiddlewareWithoutAnalysis = { api: YMusicApi ->
    middlewareForAction<AppState, TrackLyricsAction.ConnectToDetectorFailed> { store, action ->
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val localPlaylists = withContext(Dispatchers.IO) {
                    api.localApi.getLocalPlaylists()
                }
                println("Fetch downloads without analysis success")
                store.dispatch(SectionAction.FetchSuccess(DOWNLOADS_TITLE, localPlaylists, false))
            } catch (exc: Exception) {
                store.dispatch(AppAction.SetError(ApiRequestException("Can't load local playlists")))
            }
        }
        action
    }
}
