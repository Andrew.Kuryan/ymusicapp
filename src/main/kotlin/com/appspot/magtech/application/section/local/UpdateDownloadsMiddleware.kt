package com.appspot.magtech.application.section.local

import com.appspot.magtech.apiports.YMusicApi
import com.appspot.magtech.application.AppAction
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.middlewareForAction
import com.appspot.magtech.application.section.SectionAction
import com.appspot.magtech.application.section.getLocalPlaylists
import com.appspot.magtech.application.tracks.TrackElement
import com.appspot.magtech.application.tracks.TracksAction
import com.appspot.magtech.entities.User
import com.appspot.magtech.extensions.ApiRequestException
import com.appspot.magtech.extensions.NotAuthorizedException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

const val DOWNLOADS_TITLE = "Downloads"

val downloadTrackMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, TracksAction.DownloadTrack> { store, action ->
        CoroutineScope(Dispatchers.Main).launch {
            if (store.state.accountState.auth is User) {
                try {
                    val auth = store.state.accountState.auth
                    val localTrack = withContext(Dispatchers.IO) {
                        api.localApi.downloadTrack(auth as User, action.track)
                    }
                    store.state.sectionsState.getLocalPlaylists().forEach {
                        store.dispatch(
                            TracksAction.AddTrackToPlaylist(
                                playlistTitle = it.title,
                                trackElement = TrackElement(localTrack, false)
                            )
                        )
                        store.dispatch(
                            SectionAction.UpdatePlaylistInfo(
                                sectionTitle = DOWNLOADS_TITLE,
                                title = it.title,
                                newPlaylist = it.copy(
                                    info = it.info.copy(
                                        trackCount = it.info.trackCount + 1,
                                        duration = it.info.duration + localTrack.info.duration
                                    )
                                )
                            )
                        )
                    }
                    store.dispatch(TracksAction.DownloadTrackSuccess(localTrack))
                    store.dispatch(AppAction.SetMessage("Download track successfully: ${localTrack.info.title}"))
                } catch (exc: Exception) {
                    println(exc)
                    store.dispatch(TracksAction.FetchTrackFailed(action.track))
                    store.dispatch(AppAction.SetError(ApiRequestException("Can't download track")))
                }
            } else {
                store.dispatch(TracksAction.FetchTrackFailed(action.track))
                store.dispatch(AppAction.SetError(NotAuthorizedException()))
            }
        }
        action
    }
}

val removeTrackMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, TracksAction.RemoveTrack> { store, action ->
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val yMusicTrack = withContext(Dispatchers.IO) {
                    api.localApi.removeTrack(action.track)
                }
                withContext(Dispatchers.IO) { api.detectorApi.removeTrackAnalysis(action.track) }

                store.state.sectionsState.getLocalPlaylists().forEach {
                    store.dispatch(
                        TracksAction.RemoveTrackFromPlaylist(
                            playlistTitle = it.title,
                            track = yMusicTrack
                        )
                    )
                    store.dispatch(
                        SectionAction.UpdatePlaylistInfo(
                            sectionTitle = DOWNLOADS_TITLE,
                            title = it.title,
                            newPlaylist = it.copy(
                                info = it.info.copy(
                                    trackCount = it.info.trackCount - 1,
                                    duration = it.info.duration - yMusicTrack.info.duration
                                )
                            )
                        )
                    )
                }
                store.dispatch(TracksAction.RemoveTrackSuccess(yMusicTrack))
                store.dispatch(AppAction.SetMessage("Remove track successfully: ${yMusicTrack.info.title}"))
            } catch (exc: Exception) {
                println(exc)
                store.dispatch(TracksAction.FetchTrackFailed(action.track))
                store.dispatch(AppAction.SetError(ApiRequestException("Can't remove track")))
            }
        }
        action
    }
}

val downloadTrackSuccessMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, TracksAction.DownloadTrackSuccess> { _, action ->
        CoroutineScope(Dispatchers.IO).launch {
            try {
                api.detectorApi.notifyDetector(action.track)
            } catch (exc: Exception) {
                println(exc)
            }
        }
        action
    }
}