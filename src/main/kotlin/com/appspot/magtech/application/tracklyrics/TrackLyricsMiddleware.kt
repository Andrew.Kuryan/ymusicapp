package com.appspot.magtech.application.tracklyrics

import com.appspot.magtech.apiports.YMusicApi
import com.appspot.magtech.application.AppAction
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.middlewareForAction
import com.appspot.magtech.application.player.PlayerAction
import com.appspot.magtech.application.tracks.TrackElement
import com.appspot.magtech.application.tracks.TracksAction
import com.appspot.magtech.application.tracks.getTrackById
import com.appspot.magtech.entities.Auth
import com.appspot.magtech.entities.User
import com.appspot.magtech.extensions.NotAuthorizedException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.reduxkotlin.Store

fun loadTrackDetailedInfo(store: Store<AppState>, api: YMusicApi, auth: Auth, selectedElement: TrackElement?) {
    if (auth is User && selectedElement != null) {
        store.dispatch(TrackLyricsAction.StartFetching)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val trackDetailedInfo = withContext(Dispatchers.IO) {
                    api.getTrackDetailedInfo(auth, selectedElement.track)
                }
                val trackAnalysis = withContext(Dispatchers.IO) {
                    api.detectorApi.getTrackAnalysis(selectedElement.track)
                }
                store.dispatch(TrackLyricsAction.FetchDetailedInfoSuccess(trackDetailedInfo))
                store.dispatch(TracksAction.FetchTrackAnalysis(selectedElement.track.id, trackAnalysis))
            } catch (exc: Exception) {
                store.dispatch(TrackLyricsAction.FetchFailed)
            }
        }
    } else {
        store.dispatch(AppAction.SetError(NotAuthorizedException()))
    }
}

val trackLyricsChangeTrackMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, PlayerAction.SelectTrack> { store, action ->
        if (store.state.trackLyricsState.isOpened && action.trackId != store.state.playerState.selectedTrackId) {
            loadTrackDetailedInfo(
                store,
                api,
                store.state.accountState.auth,
                store.state.tracksState.getTrackById(action.trackId)
            )
        }
        action
    }
}

val trackLyricsMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, TrackLyricsAction.Open> { store, action ->
        loadTrackDetailedInfo(
            store,
            api,
            store.state.accountState.auth,
            store.state.tracksState.getTrackById(store.state.playerState.selectedTrackId)
        )
        action
    }
}

val connectDetectorMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, TrackLyricsAction.ConnectToDetector> { store, action ->
        CoroutineScope(Dispatchers.IO).launch {
            try {
                api.detectorApi.connectToDetector {
                    withContext(Dispatchers.Main) { store.dispatch(TrackLyricsAction.ConnectToDetectorSuccess) }
                }
            } catch (exc: Exception) {
                println(exc)
                withContext(Dispatchers.Main) { store.dispatch(TrackLyricsAction.ConnectToDetectorFailed) }
            }
        }
        action
    }
}