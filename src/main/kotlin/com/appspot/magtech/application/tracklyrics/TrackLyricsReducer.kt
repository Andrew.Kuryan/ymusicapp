package com.appspot.magtech.application.tracklyrics

import com.appspot.magtech.application.Action
import com.appspot.magtech.entities.TrackDetailedInfo
import org.reduxkotlin.ReducerForActionType

sealed class TrackLyricsAction : Action {
    object Open : TrackLyricsAction()
    object Close : TrackLyricsAction()

    object StartFetching : TrackLyricsAction()
    data class FetchDetailedInfoSuccess(val info: TrackDetailedInfo) : TrackLyricsAction()
    object FetchFailed : TrackLyricsAction()

    object Expand : TrackLyricsAction()
    object RollUp : TrackLyricsAction()

    object ConnectToDetector : TrackLyricsAction()
    object ConnectToDetectorSuccess : TrackLyricsAction()
    object ConnectToDetectorFailed : TrackLyricsAction()
}

data class TrackLyricsState(
    val isFetching: Boolean = false,
    val isOpened: Boolean = false,
    val trackDetailedInfo: TrackDetailedInfo? = null,
    val isExpanded: Boolean = false,
)

val trackLyricsReducer: ReducerForActionType<TrackLyricsState, TrackLyricsAction> = { state, action ->
    when (action) {
        is TrackLyricsAction.Open -> state.copy(isOpened = true)
        is TrackLyricsAction.Close -> state.copy(isOpened = false)
        is TrackLyricsAction.StartFetching -> state.copy(isFetching = true)
        is TrackLyricsAction.FetchDetailedInfoSuccess -> state.copy(
            trackDetailedInfo = action.info,
            isFetching = false
        )
        is TrackLyricsAction.FetchFailed -> state.copy(isFetching = false)
        is TrackLyricsAction.Expand -> state.copy(isExpanded = true)
        is TrackLyricsAction.RollUp -> state.copy(isExpanded = false)

        is TrackLyricsAction.ConnectToDetector -> state
        is TrackLyricsAction.ConnectToDetectorSuccess -> state
        is TrackLyricsAction.ConnectToDetectorFailed -> state
    }
}
