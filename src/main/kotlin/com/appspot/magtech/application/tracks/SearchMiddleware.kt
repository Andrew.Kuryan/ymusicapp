package com.appspot.magtech.application.tracks

import com.appspot.magtech.apiports.YMusicApi
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.middlewareForAction
import com.appspot.magtech.entities.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

val setQueryMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, SearchAction.SetQuery> { store, action ->
        CoroutineScope(Dispatchers.Main).launch {
            if (store.state.accountState.auth is User) {
                store.dispatch(SearchAction.StartFetchingResults)
                try {
                    val auth = store.state.accountState.auth
                    val searchResults = withContext(Dispatchers.IO) {
                        api.searchTracks(auth as User, action.query)
                    }
                    store.dispatch(TracksAction.FetchSuccess(
                        WEB_RESULTS_PLAYLIST_TITLE,
                        searchResults.map { TrackElement(it, false) }
                    ))
                } catch (exc: Exception) {
                }
                store.dispatch(SearchAction.StopFetchingResults)
            }
        }
        action
    }
}