package com.appspot.magtech.application.tracks

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.section.local.DOWNLOADS_TITLE
import com.appspot.magtech.entities.FulfilledAnalysis
import com.appspot.magtech.entities.LocalTrack
import com.appspot.magtech.entities.Tempo
import com.appspot.magtech.entities.isAvailable
import org.reduxkotlin.ReducerForActionType

sealed class SearchAction : Action {
    data class SetQuery(val query: String) : SearchAction()

    data class SetTempo(val tempo: Tempo) : SearchAction()
    data class ResetTempo(val tempo: Tempo) : SearchAction()
    object ResetTempos : SearchAction()
    object ApplyTempos : SearchAction()
    data class SelectTempo(val tempo: Tempo) : SearchAction()

    object StartFetchingResults : SearchAction()
    object StopFetchingResults : SearchAction()
}

const val WEB_RESULTS_PLAYLIST_TITLE = "WEB_RESULTS_PLAYLIST_TITLE"

data class SearchState(
    val query: String = "",
    val tempos: List<Tempo> = Tempo.available,
    val appliedTempos: List<Tempo> = Tempo.available,
    val isFetching: Boolean = false,
)

val searchReducer: ReducerForActionType<SearchState, SearchAction> = { state, action ->
    when (action) {
        is SearchAction.SetQuery -> state.copy(query = action.query)
        is SearchAction.StartFetchingResults -> state.copy(isFetching = true)
        is SearchAction.StopFetchingResults -> state.copy(isFetching = false)

        is SearchAction.SetTempo -> state.copy(tempos = (state.tempos + listOf(action.tempo)).distinct())
        is SearchAction.ResetTempo -> state.copy(tempos = state.tempos.filter { it != action.tempo })
        is SearchAction.ResetTempos -> state.copy(tempos = listOf())
        is SearchAction.ApplyTempos -> state.copy(
            appliedTempos = if (state.tempos.isNotEmpty()) state.tempos else Tempo.available,
            tempos = if (state.tempos.isNotEmpty()) state.tempos else Tempo.available,
        )
        is SearchAction.SelectTempo -> state.copy(
            tempos = listOf(action.tempo),
            appliedTempos = listOf(action.tempo),
        )
    }
}

fun Map<String, List<TrackElement>>.applyFilter(searchState: SearchState) =
    entries
        .filter { it.key != WEB_RESULTS_PLAYLIST_TITLE }
        .map { (key, value) ->
            value
                .filter { it.track.isAvailable() }
                .filter {
                    it.track.info.title.contains(searchState.query, true) ||
                            it.track.info.artists
                                .any { artist -> artist.name.contains(searchState.query, true) }
                }
                .filter { (track) ->
                    searchState.appliedTempos.size == Tempo.available.size ||
                            (track is LocalTrack && track.analysis is FulfilledAnalysis &&
                                    searchState.appliedTempos.contains(track.analysis.tempo))
                }
                .map { key to it }
        }
        .flatten()
        .groupBy { it.second.track.id }
        .mapValues { (_, value) ->
            if (value.all { it.first == DOWNLOADS_TITLE }) {
                value.distinctBy { it.second.track.id }.first()
            } else {
                value.filter { it.first != DOWNLOADS_TITLE }.distinctBy { it.second.track.id }.first()
            }
        }
        .values.toList()
