package com.appspot.magtech.application.tracks

import com.appspot.magtech.apiports.YMusicApi
import com.appspot.magtech.application.AppAction
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.middlewareForAction
import com.appspot.magtech.application.section.SectionAction
import com.appspot.magtech.application.section.getPlaylistByTitle
import com.appspot.magtech.extensions.ApiRequestException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

val cacheTracksMiddleware =
    middlewareForAction<AppState, SectionAction.FetchSuccess> { store, action ->
        action.playlists.forEach {
            if (it.second != null) {
                store.dispatch(TracksAction.FetchSuccess(
                    playlistTitle = it.first.title,
                    tracks = it.second!!.map { track -> TrackElement(track, false) }
                ))
            } else if (action.reload) {
                store.dispatch(
                    TracksAction.FetchSuccess(
                        playlistTitle = it.first.title,
                        tracks = listOf(),
                    )
                )
            }
        }
        action
    }

val fetchTracksMiddleware = { api: YMusicApi ->
    middlewareForAction<AppState, TracksAction.SelectPlaylist> { store, action ->
        if (action.playlistTitle != null) {
            CoroutineScope(Dispatchers.Main).launch {
                if (!store.state.tracksState.tracks[action.playlistTitle].isNullOrEmpty() && !action.reload) {
                    store.dispatch(
                        TracksAction.FetchSuccess(
                            playlistTitle = action.playlistTitle,
                            tracks = store.state.tracksState.tracks.getValue(action.playlistTitle)
                        )
                    )
                } else {
                    store.dispatch(TracksAction.StartFetching)
                    try {
                        val auth = store.state.accountState.auth
                        val playlist = store.state.sectionsState
                            .getPlaylistByTitle(action.playlistTitle) ?: throw NoSuchElementException()
                        val tracks = withContext(Dispatchers.IO) {
                            api.getPlaylistTracks(auth, playlist)
                        }
                        store.dispatch(TracksAction.FetchSuccess(
                            playlistTitle = action.playlistTitle,
                            tracks = tracks.map { TrackElement(it, false) }
                        ))
                    } catch (exc: Exception) {
                        store.dispatch(TracksAction.FetchFailed)
                        store.dispatch(AppAction.SetError(ApiRequestException("Can't load tracks")))
                    }
                }
            }
        }
        action
    }
}
