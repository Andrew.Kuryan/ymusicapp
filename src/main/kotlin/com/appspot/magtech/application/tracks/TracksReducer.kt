package com.appspot.magtech.application.tracks

import com.appspot.magtech.application.Action
import com.appspot.magtech.entities.LocalTrack
import com.appspot.magtech.entities.Track
import com.appspot.magtech.entities.TrackAnalysis
import com.appspot.magtech.entities.YMusicTrack
import org.reduxkotlin.ReducerForActionType

sealed class TracksAction : Action {
    data class SelectPlaylist(val playlistTitle: String?, val reload: Boolean) : TracksAction()
    object StartFetching : TracksAction()
    data class FetchSuccess(val playlistTitle: String, val tracks: List<TrackElement>) : TracksAction()
    object FetchFailed : TracksAction()

    data class AddTrackToPlaylist(val trackElement: TrackElement, val playlistTitle: String) : TracksAction()
    data class RemoveTrackFromPlaylist(val track: Track, val playlistTitle: String) : TracksAction()

    data class DownloadTrack(val track: YMusicTrack) : TracksAction()
    data class DownloadTrackSuccess(val track: LocalTrack) : TracksAction()
    data class RemoveTrack(val track: LocalTrack) : TracksAction()
    data class RemoveTrackSuccess(val track: YMusicTrack) : TracksAction()
    data class FetchTrackFailed(val track: Track) : TracksAction()

    data class FetchTrackAnalysis(val trackId: Int, val trackAnalysis: TrackAnalysis) : TracksAction()
}

data class TrackElement(
    val track: Track,
    val isFetching: Boolean,
)

val tracksReducer: ReducerForActionType<TracksState, TracksAction> = { state, action ->
    when (action) {
        is TracksAction.SelectPlaylist -> state.copy(selectedPlaylistTitle = action.playlistTitle)
        is TracksAction.StartFetching -> state.copy(isFetching = true)
        is TracksAction.FetchSuccess -> state
            .replaceTracks(action.playlistTitle, action.tracks)
            .copy(isFetching = false)
        is TracksAction.FetchFailed -> state.copy(isFetching = false)

        is TracksAction.DownloadTrack -> state
            .setTracksFetchingStatus(action.track, true)
        is TracksAction.DownloadTrackSuccess -> state
            .setTracksFetchingStatus(action.track, false)
            .replaceTrack(action.track.id, action.track)

        is TracksAction.RemoveTrack -> state
            .setTracksFetchingStatus(action.track, false)
        is TracksAction.RemoveTrackSuccess -> state
            .setTracksFetchingStatus(action.track, false)
            .replaceTrack(action.track.id, action.track)

        is TracksAction.FetchTrackFailed -> state
            .setTracksFetchingStatus(action.track, false)

        is TracksAction.AddTrackToPlaylist -> state
            .addTrackToPlaylist(action.trackElement, action.playlistTitle)
        is TracksAction.RemoveTrackFromPlaylist -> state
            .removeTrackFromPlaylist(action.track, action.playlistTitle)

        is TracksAction.FetchTrackAnalysis -> state
            .setTrackAnalysis(action.trackId, action.trackAnalysis)
    }
}
