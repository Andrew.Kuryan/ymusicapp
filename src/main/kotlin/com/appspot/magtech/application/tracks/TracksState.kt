package com.appspot.magtech.application.tracks

import com.appspot.magtech.entities.LocalTrack
import com.appspot.magtech.entities.Track
import com.appspot.magtech.entities.TrackAnalysis

data class TracksState(
    val isFetching: Boolean = false,
    val selectedPlaylistTitle: String? = null,
    val tracks: Map<String, List<TrackElement>> = mapOf(),
)

fun TracksState.getTrackById(trackId: Int?) = tracks.values
    .flatten()
    .find { it.track.id == trackId }

fun TracksState.replaceTracks(playlistTitle: String, newTracks: List<TrackElement>): TracksState =
    copy(tracks = if (tracks.containsKey(playlistTitle)) {
        tracks.mapValues { tracksEntry ->
            if (tracksEntry.key == playlistTitle) {
                newTracks
            } else {
                tracksEntry.value
            }
        }
    } else {
        tracks + (playlistTitle to newTracks)
    })

fun TracksState.addTrackToPlaylist(trackElement: TrackElement, playlistTitle: String): TracksState =
    copy(tracks = tracks.mapValues {
        if (it.key == playlistTitle) {
            it.value + trackElement
        } else {
            it.value
        }
    })

fun TracksState.removeTrackFromPlaylist(track: Track, playlistTitle: String): TracksState =
    copy(tracks = tracks.mapValues {
        if (it.key == playlistTitle) {
            it.value.filter { trackEl -> trackEl.track.id != track.id }
        } else {
            it.value
        }
    })

fun TracksState.replaceTrack(trackId: Int, newTrack: Track): TracksState =
    copy(tracks = tracks.mapValues {
        it.value.map { trackElement ->
            if (trackElement.track.id == trackId) {
                trackElement.copy(track = newTrack)
            } else {
                trackElement
            }
        }
    })

fun TracksState.setTracksFetchingStatus(track: Track, newStatus: Boolean): TracksState =
    copy(tracks = tracks.mapValues {
        it.value.map { trackElement ->
            if (trackElement.track.id == track.id) {
                trackElement.copy(isFetching = newStatus)
            } else {
                trackElement
            }
        }
    })

fun TracksState.setTrackAnalysis(trackId: Int, analysis: TrackAnalysis): TracksState =
    copy(tracks = tracks.mapValues {
        it.value.map { trackElement ->
            val (track) = trackElement
            if (track.id == trackId && track is LocalTrack) {
                trackElement.copy(track = track.copy(analysis = analysis))
            } else {
                trackElement
            }
        }
    })
