package com.appspot.magtech.application.util

import com.appspot.magtech.application.AppState
import org.reduxkotlin.middleware

val loggerMiddleware = { enabled: Boolean ->
    middleware<AppState> { store, next, action ->
        val result = next(action)
        if (enabled) {
            println("DISPATCH action: ${action::class.simpleName}: $action")
            println("next state: ${store.state.searchState}")
        }
        result
    }
}
