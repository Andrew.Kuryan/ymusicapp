package com.appspot.magtech.dependencies

import com.appspot.magtech.uiports.NotificationManager
import fr.jcgay.notification.Icon
import fr.jcgay.notification.Notification
import fr.jcgay.notification.SendNotification
import java.net.URL

class JcgayNotificationManager(private val iconPath: URL) : NotificationManager {

    private val notifier = SendNotification().initNotifier()

    override fun showInfo(message: String) {
        notifier.send(Notification.builder()
                .title("Yandex Music")
                .message(message)
                .icon(Icon.create(iconPath, "logo"))
                .level(Notification.Level.INFO)
                .build())
    }

    override fun close() {
        notifier.close()
    }
}
