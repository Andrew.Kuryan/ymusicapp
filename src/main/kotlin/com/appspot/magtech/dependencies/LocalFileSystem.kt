package com.appspot.magtech.dependencies

import com.appspot.magtech.apiports.FileSystem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.nio.file.Files
import java.nio.file.Paths

class LocalFileSystem : FileSystem {

    override fun getAbsolutePath(relativePath: String) =
            Paths.get(relativePath).toAbsolutePath().toString()

    override fun getURI(relativePath: String) =
            Paths.get(relativePath).toAbsolutePath().toUri().toString()

    override suspend fun writeBytes(path: String, bytes: ByteArray) {
        withContext(Dispatchers.IO) {
            Files.write(Paths.get(path), bytes)
        }
    }

    override suspend fun readAllBytes(path: String): ByteArray =
            withContext(Dispatchers.IO) {
                Files.readAllBytes(Paths.get(path))
            }

    override suspend fun writeLines(path: String, lines: List<String>) {
        withContext(Dispatchers.IO) {
            Files.write(Paths.get(path), lines)
        }
    }

    override suspend fun readAllLines(path: String): List<String> =
            withContext(Dispatchers.IO) {
                Files.readAllLines(Paths.get(path))
            }

    override suspend fun deleteFile(path: String) =
            withContext(Dispatchers.IO) {
                Files.delete(Paths.get(path))
            }
}
