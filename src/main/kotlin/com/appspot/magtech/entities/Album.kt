package com.appspot.magtech.entities

// { id: 4581247,
//      title: 'Live At AllSaints Studios',
//      type: 'single',
//      year: 2017,
//      releaseDate: '2017-08-04T00:00:00+03:00',
//      coverUri:
//       'avatars.yandex.net/get-music-content/95061/48ef8ec1.a.4581247-1/%%',
//      ogImage:
//       'avatars.yandex.net/get-music-content/95061/48ef8ec1.a.4581247-1/%%',
//      genre: 'rock',
//      buy: [],
//      trackCount: 4,
//      recent: false,
//      veryImportant: false,
//      artists: [Array],
//      labels: [Array],
//      available: true,
//      availableForPremiumUsers: true,
//      availableForMobile: true,
//      availablePartially: false,
//      bests: [Array],
//      trackPosition: [Object] }

data class Album(
        val id: Int,
        val title: String,     // название
        val trackCount: Int,   // количество треков
        val coverUri: String   // изображение обложки
)
