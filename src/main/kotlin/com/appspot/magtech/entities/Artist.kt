package com.appspot.magtech.entities

//{ id: 675068,
//    name: 'Imagine Dragons',
//    various: false,
//    composer: false,
//    cover:
//     { type: 'from-artist-photos',
//       prefix: 'ac812083.p.675068/',
//       uri:
//        'avatars.yandex.net/get-music-content/176019/ac812083.p.675068/%%' },
//    genres: [] }

data class Artist(
        val id: Int,
        val name: String,     // имя
        val coverUri: String  // изображение
)
