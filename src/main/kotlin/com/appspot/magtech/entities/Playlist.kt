package com.appspot.magtech.entities

//{ owner:
//   { uid: 1130000022209335,
//     login: 'andrew.kuryan@tut.by',
//     name: 'andrew.kuryan@tut.by',
//     sex: 'male',
//     verified: false },
//  uid: 1130000022209335,
//  kind: 1000,
//  title: 'Imagine dragons',
//  revision: 24,
//  snapshot: 17,
//  trackCount: 17,
//  visibility: 'public',
//  collective: false,
//  created: '2018-08-03T15:47:35+00:00',
//  modified: '2019-06-23T10:48:22+00:00',
//  available: true,
//  isBanner: false,
//  isPremiere: false,
//  durationMs: 3728900,
//  cover:
//   { type: 'mosaic',
//     itemsUri:
//      [ 'avatars.yandex.net/get-music-content/117546/b5c6945b.a.6017186-1/%%',
//        'avatars.yandex.net/get-music-content/193823/d9734372.a.7806706-1/%%',
//        'avatars.yandex.net/get-music-content/28589/aba9e76b.a.1597165-1/%%',
//        'avatars.yandex.net/get-music-content/63210/3548229c.a.3015939-1/%%' ],
//     custom: false },
//  ogImage:
//   'avatars.yandex.net/get-music-content/117546/b5c6945b.a.6017186-1/%%',
//  tracks: [],
//  tags: [],
//  likesCount: 0,
//  prerolls: [],
//  lastOwnerPlaylists: [] }

data class PlaylistInfo(
        val duration: Long, // длительность
        val trackCount: Int  // количество треков
)

sealed class Playlist {
    abstract val title: String
    abstract val info: PlaylistInfo
}

enum class PlaylistVisibility {
    PUBLIC, PRIVATE
}

data class UserPlaylist(
        val kind: Int,
        val visibility: PlaylistVisibility,
        override val title: String,
        override val info: PlaylistInfo
) : Playlist()

data class FeedPlaylist(
        val kind: Int,
        override val title: String,
        override val info: PlaylistInfo
) : Playlist()

data class LocalPlaylist(
        override val title: String,
        override val info: PlaylistInfo
) : Playlist()
