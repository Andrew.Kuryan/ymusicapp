package com.appspot.magtech.entities

//{ id: '54234210',
//       realId: '54234210',
//       title: 'Birds',
//       major: [Object],
//       available: true,
//       availableForPremiumUsers: true,
//       availableFullWithoutPermission: false,
//       durationMs: 219480,
//       storageDir: '1776350_51f4862b.80899848.1.54234210',
//       fileSize: 5268165,
//       normalization: [Object],
//       previewDurationMs: 30000,
//       artists: [Array],
//       albums: [Array],
//       coverUri:
//        'avatars.yandex.net/get-music-content/193823/d9734372.a.7806706-1/%%',
//       ogImage:
//        'avatars.yandex.net/get-music-content/193823/d9734372.a.7806706-1/%%',
//       lyricsAvailable: true,
//       type: 'music' }

data class TrackInfo(
    val title: String,
    val duration: Long,
    val coverUri: String,
    val artists: List<Artist>,
    val albums: List<Album>,
)

sealed class Track {
    abstract val id: Int
    abstract val info: TrackInfo
}

data class YMusicTrack(
    override val id: Int,
    override val info: TrackInfo,
    val available: Boolean,
) : Track()

data class LocalTrack(
    override val id: Int,
    override val info: TrackInfo,
    val localPath: String,
    val analysis: TrackAnalysis,
) : Track()

fun Track.displayInfo() = info.title +
        if (!info.artists.isNullOrEmpty()) {
            " – ${info.artists.joinToString(", ") { it.name }}"
        } else ""

fun Track.isAvailable() = this is LocalTrack || (this is YMusicTrack && this.available)

fun LocalTrack.toOriginal() = YMusicTrack(
    id = id,
    info = info,
    available = true
)

fun YMusicTrack.toLocal(localPath: String, analysis: TrackAnalysis) = LocalTrack(
    id = id,
    info = info,
    localPath = localPath,
    analysis = analysis,
)

fun Long.toMinutes() = String.format("%02d:%02d", this / 1000 / 60, this / 1000 % 60)

fun Long.toHours() = String.format("%02d:%02d:%02d", this / 1000 / 60 / 60, (this / 1000 / 60) % 60, (this / 1000) % 60)
