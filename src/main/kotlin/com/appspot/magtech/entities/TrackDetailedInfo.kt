package com.appspot.magtech.entities

sealed class TrackVideo {
    abstract val cover: String
    abstract val title: String
}

data class VideoLink(
    override val title: String,
    override val cover: String,
    val url: String,
    val embed: String,
) : TrackVideo()

data class EmbedVideo(
    override val title: String,
    override val cover: String,
    val embedUrl: String,
) : TrackVideo()

data class TrackLyrics(
    val id: Int,
    val fullLyrics: String,
    val lyrics: String,
)

data class TrackDetailedInfo(
    val lyric: List<TrackLyrics>,
    val videos: List<TrackVideo>,
)
