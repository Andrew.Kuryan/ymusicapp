package com.appspot.magtech.entities

// { now: '2019-07-07T18:46:17+00:00',
//  uid: 1130000022209335,
//  login: 'andrew.kuryan@tut.by',
//  region: 149,
//  fullName: 'Курьян Андрей',
//  secondName: 'Курьян',
//  firstName: 'Андрей',
//  displayName: 'andrew.kuryan@tut.by',
//  serviceAvailable: true,
//  hostedUser: false,
//  registeredAt: '2017-10-19T13:35:34+00:00' }

sealed class Auth

object Offline : Auth()

data class User(
        val uid: String,
        val login: String,
        val avatarUrl: String?,
        val sessionCredentials: SessionCredentials
) : Auth()

data class SessionCredentials(
        val sessionId: String,
        val sessionId2: String,
        val sessGuard: String,
        val yandexUid: String,
        val uniqueUid: String
)
