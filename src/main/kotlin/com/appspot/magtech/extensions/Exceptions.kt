package com.appspot.magtech.extensions

class AuthorizationException : Exception("Can't authorize")
class NotAuthorizedException : Exception("Not authorized")
class ApiRequestException(override val message: String) : Exception()
