package com.appspot.magtech.extensions

import com.appspot.magtech.api.UnknownApiException
import com.google.gson.internal.LinkedTreeMap

inline fun <reified T> LinkedTreeMap<Any, Any>.getApiValue(name: String): T {
    val fieldRaw = this[name] ?: throw UnknownApiException(name)
    val field = try {
        fieldRaw as? T ?: throw UnknownApiException(name)
    } catch (exc: Exception) {
        throw UnknownApiException(name)
    }
    return field
}

fun Double.toRealString() = String.format("%.0f", this)

fun <T : Any> LinkedTreeMap<Any, Any>.getWithVariants(vararg variants: LinkedTreeMap<Any, Any>.() -> T): T {
    val results = variants.mapNotNull {
        try {
            it()
        } catch (exc: Exception) {
            null
        }
    }
    if (results.isEmpty()) {
        throw UnknownApiException()
    } else {
        return results.first()
    }
}
