package com.appspot.magtech.extensions

import com.jfoenix.controls.*
import javafx.beans.property.Property
import javafx.beans.value.ObservableValue
import javafx.event.EventTarget
import javafx.scene.Node
import tornadofx.attachTo
import tornadofx.bind

fun EventTarget.jfxButton(text: String = "", graphic: Node? = null, op: JFXButton.() -> Unit = {}) =
    JFXButton(text).kfxAttachTo(this, op) {
        if (graphic != null) it.graphic = graphic
    }

fun EventTarget.jfxTextField(value: String? = null, op: JFXTextField.() -> Unit = {}) =
    JFXTextField().kfxAttachTo(this, op) {
        if (value != null) it.text = value
    }

fun EventTarget.jfxTextField(property: ObservableValue<String>, op: JFXTextField.() -> Unit = {}) =
    jfxTextField().apply {
        bind(property)
        op(this)
    }

fun EventTarget.jfxPasswordField(value: String? = null, op: JFXPasswordField.() -> Unit = {}) =
    JFXPasswordField().kfxAttachTo(this, op) {
        if (value != null) it.text = value
    }

fun EventTarget.jfxPasswordField(property: ObservableValue<String>, op: JFXPasswordField.() -> Unit = {}) =
    jfxPasswordField().apply {
        bind(property)
        op(this)
    }

fun EventTarget.jfxCheckbox(
    text: String? = null,
    property: Property<Boolean>? = null,
    op: JFXCheckBox.() -> Unit = {}
) =
    JFXCheckBox(text).kfxAttachTo(this, op) {
        if (property != null) it.bind(property)
    }

fun EventTarget.jfxProgressBar(initialValue: Double? = null, op: JFXProgressBar.() -> Unit = {}) =
    JFXProgressBar().kfxAttachTo(this, op) {
        if (initialValue != null) it.progress = initialValue
    }

fun EventTarget.jfxSpinner(op: JFXSpinner.() -> Unit = {}) = JFXSpinner().attachTo(this, op)

internal inline fun <T : Node> T.kfxAttachTo(
    parent: EventTarget,
    after: T.() -> Unit,
    before: (T) -> Unit
) = this.also(before).attachTo(parent, after)
