package com.appspot.magtech.extensions

class MatchBuilder<T, R>(private val values: List<T>) {

    private val variants =
            mutableListOf<Pair<List<T>, suspend () -> R?>>()

    fun comb(t1: T, t2: T, op: suspend () -> R?) =
            variants.add(listOf(t1, t2) to op)

    suspend fun complete(): R? {
        var result: R? = null
        for (variant in variants) {
            if (variant.first.zip(values)
                            .all { it.first == it.second }) {
                result = variant.second()
            }
        }
        return result
    }
}

suspend fun <T, R> match(t1: T, t2: T, t3: T, op: MatchBuilder<T, R>.() -> Unit): R? {
    return MatchBuilder<T, R>(listOf(t1, t2, t3)).apply(op).complete()
}
