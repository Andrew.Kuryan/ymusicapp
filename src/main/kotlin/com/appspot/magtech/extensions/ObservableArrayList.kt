package com.appspot.magtech.extensions

class ObservableArrayList<T> : ArrayList<T>() {

    private val onAddListeners = mutableListOf<(T) -> Unit>()
    private val onRemoveListeners = mutableListOf<(T) -> Unit>()

    fun onAdd(listener: (T) -> Unit) {
        onAddListeners.add(listener)
    }

    fun onRemove(listener: (T) -> Unit) {
        onRemoveListeners.add(listener)
    }

    operator fun timesAssign(element: T) {
        this.add(element)
        onAddListeners.forEach { it(element) }
    }

    operator fun divAssign(element: T) {
        this.remove(element)
        onRemoveListeners.forEach { it(element) }
    }
}

fun <T> observableArrayList() = ObservableArrayList<T>()
fun <T> observableArrayList(op: ObservableArrayList<T>.() -> Unit) =
        ObservableArrayList<T>().apply(op)
