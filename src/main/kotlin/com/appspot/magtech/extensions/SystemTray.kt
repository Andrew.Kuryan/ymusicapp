package com.appspot.magtech.extensions

import dorkbox.systemTray.Menu
import dorkbox.systemTray.MenuItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.JSeparator

@ExperimentalCoroutinesApi
val MenuItem.userActions: Flow<ActionEvent>
    get() = callbackFlow {
        val callback = ActionListener {
            sendBlocking(it)
        }
        setCallback(callback)
        awaitClose { setCallback(null) }
    }

class MenuBuilder(private val menu: Menu) {

    fun menuItem(text: String? = null, op: MenuItem.() -> Unit = {}) =
            menu.add(MenuItem(text).apply(op))

    fun separator() = menu.add(JSeparator())
}

fun <T> menuBuilder(menu: Menu, op: MenuBuilder.() -> T) = MenuBuilder(menu).op()
