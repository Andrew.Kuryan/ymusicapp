package com.appspot.magtech.extensions.db

import org.h2.tools.TriggerAdapter
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import java.sql.Connection
import java.sql.ResultSet
import kotlin.reflect.full.createInstance

inline fun <reified T : H2Trigger> Transaction.createh2trigger(name: String? = T::class.simpleName) {
    val inst = T::class.createInstance()
    exec(
        """ CREATE TRIGGER IF NOT EXISTS $name ${inst.statement} ON ${inst.table.tableName}
        FOR EACH ROW CALL "${T::class.java.name}" """
    )
}


abstract class H2Trigger(val statement: String, val table: Table) : TriggerAdapter()

abstract class AfterDeleteTrigger(table: Table) : H2Trigger("AFTER DELETE", table) {

    override fun fire(conn: Connection, oldRow: ResultSet, newRow: ResultSet?) =
        execute(conn, oldRow)

    abstract fun execute(conn: Connection, oldRow: ResultSet)
}

abstract class AfterInsertTrigger(table: Table) : H2Trigger("AFTER INSERT", table) {

    override fun fire(conn: Connection, oldRow: ResultSet?, newRow: ResultSet) =
        execute(conn, newRow)

    abstract fun execute(conn: Connection, newRow: ResultSet)
}
