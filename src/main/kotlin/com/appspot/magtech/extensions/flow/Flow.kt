package com.appspot.magtech.extensions.flow

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.*

fun <T> Flow<T>.watchDisposable(action: (T) -> Unit): AutoCloseable {
    val job = CoroutineScope(Dispatchers.Main).launch {
        collect {
            action(it)
        }
    }
    return AutoCloseable { job.cancel() }
}

fun <T> Flow<T>.watchOnce(action: (T) -> Unit) {
    CoroutineScope(Dispatchers.Main).launch {
        collect {
            action(it)
            cancel()
        }
    }
}

fun <T> Flow<T>.sendToDisposable(channel: Channel<T>) =
        watchDisposable { channel.sendBlocking(it) }

@FlowPreview
fun <T, R> Flow<T>.mapDistinctBy(extractor: (T) -> R): Flow<R> =
        distinctUntilChangedBy { extractor(it) }
                .map { extractor(it) }

@ExperimentalCoroutinesApi
val <T> Flow<T>.zipLast: (Int) -> Flow<List<T?>>
    get() = { n ->
        val buffer: MutableList<T?> = (1..n).map { null }.toMutableList()
        transform {
            for (i in 1 until n) {
                buffer[i] = buffer[i - 1]
            }
            buffer[0] = it
            emit(buffer.toList())
        }
    }
