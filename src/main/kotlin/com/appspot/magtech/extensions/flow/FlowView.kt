package com.appspot.magtech.extensions.flow

import javafx.beans.property.Property
import javafx.beans.property.SimpleObjectProperty
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import tornadofx.View
import tornadofx.cleanBind

abstract class FlowView(title: String? = null) : View(title) {

    private val flowJobs = mutableListOf<Job>()

    infix fun <T> Property<T>.bindFlow(flow: Flow<T>) {
        cleanBind(flow.collectAsProperty())
    }

    fun <T> Flow<T>.watch(action: (T) -> Unit) {
        val job = CoroutineScope(Dispatchers.Main).launch {
            collect {
                action(it)
            }
        }
        flowJobs.add(job)
    }

    fun <T> Flow<T>.sendTo(channel: Channel<T>) =
            watch { channel.sendBlocking(it) }

    fun <T> Flow<T>.collectAsProperty(): Property<T> {
        val property = SimpleObjectProperty<T>()

        val job = CoroutineScope(Dispatchers.Main).launch {
            collect {
                property.set(it)
            }
        }
        flowJobs.add(job)

        return property
    }

    override fun onUndock() {
        flowJobs.forEach { it.cancel() }
    }
}
