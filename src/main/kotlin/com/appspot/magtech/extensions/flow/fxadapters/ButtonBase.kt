package com.appspot.magtech.extensions.flow.fxadapters

import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.control.ButtonBase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

@ExperimentalCoroutinesApi
val ButtonBase.userActions: Flow<ActionEvent>
    get() = callbackFlow {
        onAction = EventHandler {
            sendBlocking(it)
        }
        awaitClose { onAction = null }
    }
