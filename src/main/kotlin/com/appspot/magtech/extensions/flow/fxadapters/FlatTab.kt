package com.appspot.magtech.extensions.flow.fxadapters

import com.appspot.magtech.ui.components.flattabpane.FlatTab
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

@ExperimentalCoroutinesApi
val FlatTab.userActions: Flow<Unit>
    get() = callbackFlow {
        val action = {
            sendBlocking(Unit)
        }
        actions.add(action)
        awaitClose { actions.remove(action) }
    }
