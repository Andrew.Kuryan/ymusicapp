package com.appspot.magtech.extensions.flow.fxadapters

import javafx.beans.Observable
import javafx.scene.control.ListView
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

@ExperimentalCoroutinesApi
val ListView<*>.selectedIndices: Flow<Int>
    get() = callbackFlow {
        val listener = { _: Observable, _: Number, newValue: Number ->
            sendBlocking(newValue.toInt())
        }
        selectionModel.selectedIndexProperty().addListener(listener)
        awaitClose { selectionModel.selectedIndexProperty().removeListener(listener) }
    }
