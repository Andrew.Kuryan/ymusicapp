package com.appspot.magtech.extensions.flow.fxadapters

import javafx.scene.media.MediaPlayer
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

@ExperimentalCoroutinesApi
val MediaPlayer.endsOfMedia: Flow<Unit>
    get() = callbackFlow {
        val runnable = kotlinx.coroutines.Runnable {
            sendBlocking(Unit)
        }
        onEndOfMedia = runnable
        awaitClose { onEndOfMedia = null }
    }
