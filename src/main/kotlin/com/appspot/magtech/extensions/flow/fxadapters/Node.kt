package com.appspot.magtech.extensions.flow.fxadapters

import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.input.MouseEvent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

@ExperimentalCoroutinesApi
val Node.clicks: Flow<MouseEvent>
    get() = callbackFlow {
        val handler = EventHandler<MouseEvent> {
            sendBlocking(it)
        }
        onMouseClicked = handler
        awaitClose { onMouseClicked = null }
    }
