package com.appspot.magtech.extensions.flow.fxadapters

import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.input.KeyEvent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

@ExperimentalCoroutinesApi
val Scene.keyPresses: Flow<KeyEvent>
    get() = callbackFlow {
        val callback = EventHandler<KeyEvent> {
            sendBlocking(it)
        }
        onKeyPressed = callback
        awaitClose { onKeyPressed = null }
    }

@ExperimentalCoroutinesApi
val Scene.keyReleases: Flow<KeyEvent>
    get() = callbackFlow {
        val callback = EventHandler<KeyEvent> {
            sendBlocking(it)
        }
        onKeyReleased = callback
        awaitClose { onKeyReleased = null }
    }
