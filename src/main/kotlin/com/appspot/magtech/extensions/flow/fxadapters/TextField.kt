package com.appspot.magtech.extensions.flow.fxadapters

import javafx.beans.Observable
import javafx.scene.control.TextField
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

@ExperimentalCoroutinesApi
val TextField.texts: Flow<String>
    get() = callbackFlow {
        val listener = { _: Observable, _: String, newValue: String ->
            sendBlocking(newValue)
        }
        textProperty().addListener(listener)
        awaitClose { textProperty().removeListener(listener) }
    }