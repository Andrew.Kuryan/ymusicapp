package com.appspot.magtech.ui

import com.appspot.magtech.api.YMusicKtorApi
import com.appspot.magtech.api.parsers.parseStatus
import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.account.AccountAction
import com.appspot.magtech.application.configureStore
import com.appspot.magtech.application.player.PlayerAction
import com.appspot.magtech.application.tracklyrics.TrackLyricsAction
import com.appspot.magtech.application.tracks.TracksAction
import com.appspot.magtech.application.tracks.getTrackById
import com.appspot.magtech.dependencies.JcgayNotificationManager
import com.appspot.magtech.dependencies.LocalFileSystem
import com.appspot.magtech.entities.LocalTrack
import com.appspot.magtech.entities.YMusicTrack
import com.appspot.magtech.extensions.flow.fxadapters.keyPresses
import com.appspot.magtech.extensions.flow.sendToDisposable
import com.appspot.magtech.extensions.flow.zipLast
import com.appspot.magtech.extensions.match
import com.appspot.magtech.ui.views.buildToolbarMenu
import com.appspot.magtech.ui.views.main.MainView
import dorkbox.systemTray.SystemTray
import dorkbox.systemTray.Tray
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.*
import io.ktor.client.features.websocket.*
import io.ktor.client.request.*
import io.ktor.util.*
import javafx.scene.image.Image
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.stage.Stage
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import org.zeroturnaround.exec.ProcessExecutor
import org.zeroturnaround.exec.stream.LogOutputStream
import tornadofx.*
import kotlin.system.exitProcess

@ExperimentalCoroutinesApi
fun Flow<KeyEvent>.mapKeyActions(states: Flow<AppState>): Flow<Action> = map { it.code }
    .zipLast(3)
    .filterIsInstance<List<KeyCode?>>()
    .mapNotNull { (first, second, third) ->
        match<KeyCode?, Action>(first, second, third) {
            comb(KeyCode.W, KeyCode.CONTROL) { PlayerAction.SelectNextTrack }
            comb(KeyCode.S, KeyCode.CONTROL) { PlayerAction.SelectPreviousTrack }
            comb(KeyCode.P, KeyCode.CONTROL) { PlayerAction.Toggle }
            comb(KeyCode.D, KeyCode.CONTROL) {
                states.map { it.tracksState.getTrackById(it.playerState.selectedTrackId) }
                    .first()?.let {
                        if (!it.isFetching && it.track is YMusicTrack) {
                            TracksAction.DownloadTrack(it.track)
                        } else null
                    }
            }
            comb(KeyCode.R, KeyCode.CONTROL) {
                states.map { it.tracksState.getTrackById(it.playerState.selectedTrackId) }
                    .first()?.let {
                        if (!it.isFetching && it.track is LocalTrack) {
                            TracksAction.RemoveTrack(it.track)
                        } else null
                    }
            }
        }
    }

@Suppress("UNUSED")
class Application : App() {

    private val client = HttpClient(CIO) {
        install(HttpTimeout)
        install(WebSockets)
    }
    private val fileSystem = LocalFileSystem()
    private val apiActions = Channel<Action>()
    private val api = YMusicKtorApi(client, fileSystem, "downloads/", apiActions)

    private val iconPath = "/images/common/icon.png"
    private val notificationManager = JcgayNotificationManager(this::class.java.getResource(iconPath))

    private var toolbarJobs: List<AutoCloseable>? = null

    override fun init() {
        super.init()

        addStageIcon(Image(this::class.java.getResourceAsStream(iconPath)))
    }

    @InternalAPI
    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun start(stage: Stage) {
        /*try {
            System.setProperty("prism.verbose", "true")
            System.setProperty("prism.dirtyopts", "false")
            //System.setProperty("javafx.animation.fullspeed", "true");
            System.setProperty("javafx.animation.pulse", "10")
        } catch (e: AccessControlException) {
        }*/

        val (states, actions) = configureStore(api, apiActions)

        val mainView = MainView(states, actions, notificationManager)

        FX.registerApplication(scope, this, stage)
        stage.apply {
            stage.aboutToBeShown = true
            scene = createPrimaryScene(mainView)

            /*val tracker = PerformanceTracker.getSceneTracker(scene)
            Timeline(KeyFrame(Duration.seconds(1.0), EventHandler { event ->
                val fps = tracker.instantFPS
                // tracker.resetAverageFPS()
                println(String.format("Current frame rate: %.3f fps", fps))
            })).apply {
                cycleCount = Animation.INDEFINITE
                play()
            }*/

            FX.applyStylesheetsTo(scene)
            titleProperty().bind(mainView.titleProperty)
            hookGlobalShortcuts()
            mainView.onBeforeShow()
            show()
            stage.aboutToBeShown = false
            stage.setOnCloseRequest {
                stopApplication(toolbarJobs ?: listOf())
            }
        }
        FX.initialized.value = true

        CoroutineScope(Dispatchers.Main).launch {
            actions.send(AccountAction.FetchSavedUsers)
        }

        stage.scene.keyPresses.mapKeyActions(states)
            .sendToDisposable(actions)

        val systemTray = SystemTray.get()
        systemTray.setImage(javaClass.getResourceAsStream("/images/common/float_icon.png"))
        systemTray.setEnabled(true)
        systemTray.setTooltip("Yandex Music")
        SystemTray.AUTO_SIZE = false
        (systemTray?.menu as? Tray)?.apply {
            toolbarJobs = buildToolbarMenu(states, actions) { stopApplication(it) }
        }

        CoroutineScope(Dispatchers.Default).launch {
            try {
                val result = withContext(Dispatchers.IO) {
                    client.get<String>("http://localhost:3355/status")
                }
                if (parseStatus(result) == "working") {
                    withContext(Dispatchers.Main) { actions.send(TrackLyricsAction.ConnectToDetector) }
                }
            } catch (exc: Exception) {
                try {
                    ProcessExecutor().command("java", "-jar", "detector.jar")
                        .redirectError(null)
                        .redirectOutput(object : LogOutputStream() {
                            override fun processLine(line: String?) {
                                if (parseStatus(line ?: "") == "working") {
                                    CoroutineScope(Dispatchers.Main).launch {
                                        actions.send(TrackLyricsAction.ConnectToDetector)
                                    }
                                }
                            }
                        }).execute()
                } catch (processExc: Throwable) {
                    println(processExc)
                }
            }
        }
    }

    private fun stopApplication(jobs: List<AutoCloseable>) {
        jobs.forEach { it.close() }
        exitProcess(0)
    }
}
