package com.appspot.magtech.ui.components

import javafx.beans.property.SimpleBooleanProperty
import javafx.event.EventTarget
import javafx.scene.Node
import javafx.scene.layout.StackPane
import tornadofx.*

open class PlaceholderPane(
        contentNode: Node,
        private val placeholderNode: Node
) : StackPane() {

    val isActiveProperty = SimpleBooleanProperty(true).apply {
        addListener { _, oldValue, newValue ->
            if (!oldValue && newValue) {
                children.remove(placeholderNode)
            } else if (oldValue && !newValue) {
                children.add(placeholderNode)
            }
        }
    }

    init {
        add(contentNode)
    }
}

fun EventTarget.placeholderPane(contentNode: Node, placeholderNode: Node, op: PlaceholderPane.() -> Unit) =
        PlaceholderPane(contentNode, placeholderNode).apply(op).apply {
            this@placeholderPane.add(this)
        }
