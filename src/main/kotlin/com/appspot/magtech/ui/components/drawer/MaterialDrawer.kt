package com.appspot.magtech.ui.components.drawer

import com.appspot.magtech.extensions.flow.fxadapters.clicks
import javafx.animation.Interpolator
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.event.EventTarget
import javafx.scene.Node
import javafx.scene.layout.BorderPane
import javafx.scene.layout.StackPane
import javafx.util.Duration
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import tornadofx.*

@ExperimentalCoroutinesApi
class MaterialDrawer(private val widthPercent: Double) : StackPane() {

    private val overlayOpacityProperty = SimpleDoubleProperty(0.5)
    private val contentWidthProperty = SimpleDoubleProperty(1.0)

    var contentWrapper: BorderPane
    lateinit var closedActions: Flow<Unit>

    var sideContent: Node
        set(value) {
            contentWrapper.center = value
        }
        get() = contentWrapper.center

    val isOpenedProperty = SimpleBooleanProperty(true).apply {
        addListener { _, oldValue, newValue ->
            if (oldValue && !newValue) {
                timeline {
                    cycleCount = 1
                    isAutoReverse = true
                    keyframe(Duration.millis(500.0)) {
                        keyvalue(overlayOpacityProperty, 0.0, Interpolator.LINEAR)
                        keyvalue(contentWidthProperty, 0.0, Interpolator.EASE_BOTH)
                    }
                    setOnFinished { toBack() }
                }.play()

            } else if (!oldValue && newValue) {
                toFront()
                timeline {
                    cycleCount = 1
                    isAutoReverse = true
                    keyframe(Duration.millis(500.0)) {
                        keyvalue(overlayOpacityProperty, 0.5, Interpolator.LINEAR)
                        keyvalue(contentWidthProperty, 1.0, Interpolator.EASE_BOTH)
                    }
                }.play()
            }
        }
    }

    init {
        importStylesheet<MaterialDrawerStyle>()

        addClass(MaterialDrawerStyle.drawerRoot)
        flowpane {
            opacityProperty().bind(overlayOpacityProperty)
            addClass(MaterialDrawerStyle.overlayRoot)

            closedActions = clicks.map { Unit }
        }
        contentWrapper = borderpane {
            maxWidthProperty().bind(this@MaterialDrawer
                    .widthProperty()
                    .multiply(widthPercent)
                    .multiply(contentWidthProperty))
            addClass(MaterialDrawerStyle.content)
        }
    }
}

@ExperimentalCoroutinesApi
fun EventTarget.materialDrawer(widthPercent: Double = 0.7, op: MaterialDrawer.() -> Unit) =
        MaterialDrawer(widthPercent).attachTo(this, op)
