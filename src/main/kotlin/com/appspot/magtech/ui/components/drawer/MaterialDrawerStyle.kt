package com.appspot.magtech.ui.components.drawer

import javafx.geometry.Pos
import javafx.scene.paint.Color
import tornadofx.Stylesheet
import tornadofx.c
import tornadofx.cssclass

class MaterialDrawerStyle : Stylesheet() {

    companion object {
        val drawerRoot by cssclass()
        val overlayRoot by cssclass()
        val content by cssclass()
    }

    init {
        drawerRoot {
            alignment = Pos.CENTER_LEFT
            overlayRoot {
                backgroundColor += c(0, 0, 0)
            }
            content {
                backgroundColor += Color.WHITE
            }
        }
    }
}
