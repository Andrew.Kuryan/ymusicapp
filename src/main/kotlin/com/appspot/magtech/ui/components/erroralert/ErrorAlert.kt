package com.appspot.magtech.ui.components.erroralert

import com.appspot.magtech.extensions.flow.fxadapters.clicks
import com.appspot.magtech.extensions.flow.watchOnce
import com.appspot.magtech.extensions.jfxButton
import javafx.scene.layout.Pane
import javafx.scene.layout.Priority
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import tornadofx.*
import tornadofx.Stylesheet.Companion.content

class ErrorAlert(text: String) : View() {

    init {
        importStylesheet<ErrorAlertStyle>()
    }

    lateinit var closeActions: Flow<Unit>

    @ExperimentalCoroutinesApi
    override val root = flowpane {
        addClass(ErrorAlertStyle.shadowWrapper)
        vbox {
            setId(ErrorAlertStyle.errorAlert)
            addClass(ErrorAlertStyle.errorAlertRoot)
            flowpane {
                addClass(content)
                vgrow = Priority.ALWAYS
                label(text)
            }
            hbox {
                addClass(ErrorAlertStyle.buttonBox)
                stackpane { hgrow = Priority.ALWAYS }
                jfxButton("ОК") {
                    closeActions = clicks.map { Unit }
                }
            }
        }
    }
}

@ExperimentalCoroutinesApi
fun Pane.showErrorAlert(error: String, op: ErrorAlert.() -> Unit) {
    children.add(ErrorAlert(error).apply {
        op()
        root.layoutX = this@showErrorAlert.width / 2 - ErrorAlertStyle.alertWidth / 2
        root.layoutY = this@showErrorAlert.height / 2 - ErrorAlertStyle.alertHeight / 2
        closeActions.watchOnce {
            children.removeAt(children.size - 1)
        }
    }.root)
}
