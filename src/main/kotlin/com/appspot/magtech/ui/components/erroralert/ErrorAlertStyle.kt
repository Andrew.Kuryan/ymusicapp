package com.appspot.magtech.ui.components.erroralert

import com.appspot.magtech.ui.values.Colors
import com.appspot.magtech.ui.values.Fonts
import com.appspot.magtech.ui.values.labelFont
import javafx.geometry.Pos
import javafx.scene.effect.DropShadow
import javafx.scene.paint.Color
import tornadofx.*

class ErrorAlertStyle : Stylesheet() {

    companion object {
        const val alertWidth = 490
        const val alertHeight = 180

        val shadowWrapper by cssclass()
        val errorAlert by cssid()
        val errorAlertRoot by cssclass()
        val buttonBox by cssclass()
    }

    init {
        shadowWrapper {
            alignment = Pos.CENTER
            prefWidth = alertWidth.px
            prefHeight = alertHeight.px
            backgroundColor += Color.TRANSPARENT
        }
        errorAlertRoot {
            prefWidth = 450.px
            prefHeight = 140.px
            alignment = Pos.CENTER
            backgroundColor += Colors.deepDark
            backgroundRadius += box(4.px)
            effect = DropShadow(12.0, 0.0, 0.0, Color.BLACK)
            label {
                labelFont = Fonts.errorAlertText
            }
            content {
                alignment = Pos.CENTER_LEFT
                padding = box(35.px, 0.px, 0.px, 60.px)
            }
            buttonBox {
                padding = box(0.px, 30.px, 20.px, 30.px)
                button {
                    prefWidth = 100.px
                    prefHeight = 10.px
                    borderRadius += box(4.px)
                    borderWidth += box(1.px)
                    borderColor += box(Colors.beetroot)
                    labelFont = Fonts.errorAlertButton
                }
            }
        }
    }
}
