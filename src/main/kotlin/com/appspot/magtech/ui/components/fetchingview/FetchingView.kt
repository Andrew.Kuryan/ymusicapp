package com.appspot.magtech.ui.components.fetchingview

import com.appspot.magtech.extensions.jfxSpinner
import com.appspot.magtech.ui.components.PlaceholderPane
import javafx.beans.property.SimpleBooleanProperty
import javafx.event.EventTarget
import tornadofx.*

class FetchingView<C : UIComponent>(component: C) :
        PlaceholderPane(component.root, PlaceholderView().root) {

    val isFetchingProperty = SimpleBooleanProperty(false)

    init {
        importStylesheet<FetchingViewStyle>()

        isActiveProperty.bind(isFetchingProperty.not())
    }

    class PlaceholderView : View() {

        override val root = flowpane {
            setId(FetchingViewStyle.fetchingView)
            addClass(FetchingViewStyle.maskRoot)
            jfxSpinner {
                radius = FetchingViewStyle.spinnerRadius
            }
        }
    }
}

fun EventTarget.fetchingWrapper(view: View, op: FetchingView<*>.() -> Unit) =
        FetchingView(view).apply(op).apply {
            this@fetchingWrapper.add(this)
        }
