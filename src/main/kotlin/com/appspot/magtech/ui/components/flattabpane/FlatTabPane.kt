package com.appspot.magtech.ui.components.flattabpane

import com.appspot.magtech.extensions.jfxButton
import com.appspot.magtech.extensions.observableArrayList
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.event.EventTarget
import javafx.scene.Node
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.layout.StackPane
import tornadofx.*
import tornadofx.Stylesheet.Companion.tabDownButton
import tornadofx.Stylesheet.Companion.tabHeaderArea

class FlatTab(var text: String = "", val content: Node, val graphic: Node? = null) {

    val graphicProperty = SimpleObjectProperty(graphic).apply {
        addListener { _, oldValue, newValue ->
            if (oldValue != null) {
                header?.root?.children?.remove(oldValue)
            }
            if (newValue != null) {
                header?.root?.children?.add(0, newValue)
            }
        }
    }

    var header: FlatTabPane.HeaderButton? = null
    var actions = mutableListOf<() -> Unit>()
        private set
    var isSelected = false

    fun action(op: () -> Unit) {
        actions.add(op)
    }

    fun toggleClass(classRule: CssRule, predicate: Boolean) = apply {
        if (predicate) {
            if (header?.root?.hasClass(classRule) == false) header?.root?.addClass(classRule)
        } else {
            header?.root?.removeClass(classRule)
        }
    }
}

class FlatTabPane : View() {

    val tabs = observableArrayList<FlatTab> {
        onAdd { tab ->
            stackpane.add(tab.content)
            val headerBtn = HeaderButton(tab)
            header.add(headerBtn.root)
            tab.header = headerBtn
        }
        onRemove { tab ->
            stackpane.children.remove(tab.content)
            header.children.remove(tab.header?.root)
        }
    }

    private var selectedTab: FlatTab? = null
        set(value) {
            value?.content?.toFront()
            tabs.forEach { it.header?.root?.removeClass(FlatTabPaneStyle.selectedTab) }
            value?.header?.root?.addClass(FlatTabPaneStyle.selectedTab)
            value?.isSelected = true
            field = value
        }

    val selectedTabTitleProperty = SimpleStringProperty(null).apply {
        addListener { _, _, newValue ->
            selectedTab = tabs.find { it.text == newValue }
        }
    }

    private lateinit var stackpane: StackPane
    private lateinit var header: HBox

    init {
        importStylesheet<FlatTabPaneStyle>()
    }

    inner class HeaderButton(tab: FlatTab) : View() {

        override val root = hbox {
            addClass(tabDownButton)
            prefWidthProperty().bind(this@FlatTabPane.root.widthProperty().divide(3))
            if (tab.graphic != null) {
                add(tab.graphic)
            }
            anchorpane {
                hgrow = Priority.ALWAYS
                jfxButton(tab.text) {
                    anchorpaneConstraints {
                        topAnchor = 0.0
                        leftAnchor = 0.0
                        rightAnchor = 0.0
                        bottomAnchor = 0.0
                    }
                    action {
                        selectedTab = tab
                        tab.actions.forEach { it() }
                    }
                }
            }
        }
    }

    fun addTab(text: String = "", content: Node, isSelected: Boolean, graphic: Node?, op: FlatTab.() -> Unit) {
        val newTab = FlatTab(text, content, graphic)
        newTab.op()
        tabs *= newTab
        selectedTab = if (isSelected) newTab else selectedTab ?: newTab
    }

    override val root = vbox {
        addClass(FlatTabPaneStyle.flatTabPaneRoot)
        setId(FlatTabPaneStyle.flatTabPane)
        header = hbox {
            addClass(tabHeaderArea)
        }
        stackpane = stackpane {
            vgrow = Priority.ALWAYS
        }
    }
}

fun EventTarget.flatTabPane(op: FlatTabPane.() -> Unit = {}) {
    add(FlatTabPane().apply(op).root)
}

fun FlatTabPane.tab(
    uiComponent: UIComponent,
    isSelected: Boolean = false,
    graphic: Node? = null,
    op: FlatTab.() -> Unit = {}
): FlatTab {
    addTab(content = uiComponent.root, isSelected = isSelected, graphic = graphic, op = op)
    return tabs.last()
}
