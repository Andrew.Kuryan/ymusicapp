package com.appspot.magtech.ui.components.flattabpane

import javafx.scene.paint.Color
import tornadofx.*

class FlatTabPaneStyle : Stylesheet() {

    companion object {
        const val headerHeight = 60

        val flatTabPane by cssid()
        val flatTabPaneRoot by cssclass()
        val selectedTab by cssclass()
    }

    init {
        flatTabPaneRoot {
            tabHeaderArea {
                prefHeight = headerHeight.px
                tabDownButton {
                    prefHeight = headerHeight.px
                    borderWidth += box(0.px, 0.px, 1.px, 0.px)
                    borderColor += box(backgroundColor.elements.getOrNull(0) ?: Color.BLUE)
                    borderRadius += box(0.px)
                    backgroundRadius += box(0.px)
                }
                selectedTab {
                    borderColor += box(backgroundColor.elements.getOrNull(0) ?: Color.BLUE,
                            backgroundColor.elements.getOrNull(0) ?: Color.BLUE,
                            Color.CORAL,
                            backgroundColor.elements.getOrNull(0) ?: Color.BLUE)
                }
            }
        }
    }
}
