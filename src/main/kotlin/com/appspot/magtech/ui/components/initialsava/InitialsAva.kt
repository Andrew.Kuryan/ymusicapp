package com.appspot.magtech.ui.components.initialsava

import javafx.beans.property.SimpleStringProperty
import javafx.event.EventTarget
import javafx.scene.layout.AnchorPane
import tornadofx.*

class InitialsAva(name: String) : AnchorPane() {

    val nameProperty = SimpleStringProperty(name)

    init {
        importStylesheet<InitialsAvaStyle>()
        addClass(InitialsAvaStyle.initialsAvaRoot)
        setId(InitialsAvaStyle.initialsAva)
        this.prefWidthProperty().addListener { _, _, newValue ->
            style = "-fx-background-radius: ${newValue.toInt() / 2}px"
        }
        this.prefHeightProperty().addListener { _, _, newValue ->
            style = "-fx-background-radius: ${newValue.toInt() / 2}px"
        }
        label(nameProperty.stringBinding { it?.firstOrNull()?.toString()?.toUpperCase() ?: "" }) {
            addClass(InitialsAvaStyle.initials)
            anchorpaneConstraints {
                topAnchor = 0.0
                rightAnchor = 0.0
                bottomAnchor = 0.0
                leftAnchor = 0.0
            }
        }
    }
}

fun EventTarget.initialsAva(name: String = "", op: InitialsAva.() -> Unit = {}) =
        InitialsAva(name).attachTo(this, op)
