package com.appspot.magtech.ui.components.initialsava

import com.appspot.magtech.ui.values.Colors
import com.appspot.magtech.ui.values.Fonts
import com.appspot.magtech.ui.values.labelFont
import javafx.geometry.Pos
import javafx.scene.text.TextAlignment
import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.cssid

class InitialsAvaStyle : Stylesheet() {

    companion object {
        val initialsAva by cssid()
        val initialsAvaRoot by cssclass()
        val initials by cssclass()
    }

    init {
        initialsAvaRoot {
            backgroundColor += Colors.primeYellow

            initials {
                labelFont = Fonts.avaInitials
                textAlignment = TextAlignment.CENTER
                alignment = Pos.CENTER
            }
        }
    }
}
