package com.appspot.magtech.ui.components.notification

import com.appspot.magtech.uiports.NotificationManager
import javafx.scene.layout.Pane
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import tornadofx.*

class InfoToast(message: String) : View() {

    init {
        importStylesheet<InfoToastStyle>()
    }

    override val root = flowpane {
        addClass(InfoToastStyle.shadowWrapper)
        vbox {
            addClass(InfoToastStyle.toastRoot)
            label(message)
        }
    }
}

fun Pane.showNotification(message: String, isFocused: Boolean, manager: NotificationManager) {
    if (isFocused) {
        children.add(InfoToast(message).apply {
            root.layoutX = this@showNotification.width / 2 - InfoToastStyle.toastWidth / 2
            root.layoutY = this@showNotification.height - InfoToastStyle.toastHeight
        }.root)

        CoroutineScope(Dispatchers.Default).launch {
            delay(2000)
            CoroutineScope(Dispatchers.Main).launch {
                children.removeAt(children.size - 1)
            }
        }
    } else {
        manager.showInfo(message)
    }
}
