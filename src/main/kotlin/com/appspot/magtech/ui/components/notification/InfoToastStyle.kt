package com.appspot.magtech.ui.components.notification

import com.appspot.magtech.ui.values.Colors
import com.appspot.magtech.ui.values.Fonts
import com.appspot.magtech.ui.values.labelFont
import javafx.geometry.Pos
import javafx.scene.effect.DropShadow
import javafx.scene.paint.Color
import javafx.scene.text.TextAlignment
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.px

class InfoToastStyle : Stylesheet() {

    companion object {
        const val toastWidth = 280
        const val toastHeight = 120
        val shadowWrapper by cssclass()
        val toastRoot by cssclass()
    }

    init {
        shadowWrapper {
            alignment = Pos.CENTER
            prefWidth = toastWidth.px
            prefHeight = toastHeight.px
            backgroundColor += Color.TRANSPARENT
        }
        toastRoot {
            prefWidth = 250.px
            prefHeight = 90.px
            padding = box(8.px)
            alignment = Pos.CENTER
            effect = DropShadow(8.0, 0.0, 0.0, Color.BLACK)
            backgroundColor += Colors.white
            borderWidth += box(4.px)
            borderColor += box(Colors.lightGray)
            borderRadius += box(8.px)
            backgroundRadius += box(11.px)
            label {
                labelFont = Fonts.trackTitle
                wrapText = true
                textAlignment = TextAlignment.CENTER
            }
        }
    }
}
