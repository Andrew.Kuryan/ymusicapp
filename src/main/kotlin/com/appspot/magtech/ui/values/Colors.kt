package com.appspot.magtech.ui.values

import tornadofx.c

object Colors {

    val darkGray = c(55, 55, 55)
    val maskDark = c("#080808", 0.5)
    val deepDark = c("#333333")

    val primeYellow = c("#ffcc00")
    val lightYellow = c("#e7be88")
    val semiLightYellow = c("#e6c082")
    val primeGray = c("#3b3b3b")
    val semiDarkGray = c("#4d4d4d")
    val lightGray = c("#6f6f6f")
    val pearlescentLightGray = c(156, 156, 156)
    val silver = c(192, 192, 192)
    val extraLightGray = c("#dcdcdc")
    val white = c("#ffffff")
    val beetroot = c("#ab6061")
    val gridpear = c(231, 233, 232)
}
