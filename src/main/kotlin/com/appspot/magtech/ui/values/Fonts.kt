package com.appspot.magtech.ui.values

import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import javafx.scene.text.Font
import javafx.scene.text.FontPosture
import javafx.scene.text.FontWeight
import tornadofx.CssSelectionBlock
import tornadofx.px

data class LabelFont(
    val fontFamily: String = "System",
    val size: Double,
    val fontWeight: FontWeight = FontWeight.NORMAL,
    val fontStyle: FontPosture = FontPosture.REGULAR,
    val color: Paint = Color.BLACK
)

var CssSelectionBlock.labelFont: LabelFont
    get() = LabelFont(
        fontFamily = fontFamily,
        size = fontSize.value,
        fontWeight = fontWeight,
        fontStyle = fontStyle,
        color = textFill
    )
    set(value) {
        fontFamily = value.fontFamily
        fontStyle = value.fontStyle
        fontWeight = value.fontWeight
        fontSize = value.size.px
        textFill = value.color
    }

object Fonts {

    private val fonts = arrayOf(
        "gabriola/Gabriola.ttf",
        "TimesNewRoman/times.ttf",
        "SegoePrint/SegoePrint.ttf"
    )

    init {
        fonts.forEach {
            try {
                Font.loadFont(this::class.java.getResourceAsStream("/fonts/$it"), 10.0)
            } catch (exc: Exception) {
                println(exc)
            }
        }
        /*Font.getFontNames().forEach {
            println(it)
        }*/
    }

    val errorAlertText = LabelFont(
        fontFamily = "Segoe Print",
        size = 16.0,
        color = Colors.beetroot
    )
    val errorAlertButton = LabelFont(
        fontFamily = "Times New Roman",
        size = 15.0,
        color = Colors.beetroot
    )
    val trackTitle = LabelFont(
        fontFamily = "Segoe Print",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 15.0,
        color = Color.BLACK
    )
    val trackTitleDisabled = trackTitle.copy(
        color = Colors.pearlescentLightGray
    )
    val trackListParagraph = LabelFont(
        fontFamily = "Segoe Print",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 15.0,
        color = Color.BLACK
    )
    val tempoItem = LabelFont(
        fontFamily = "Segoe Print",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 14.0,
        color = Color.BLACK
    )
    val filterButton = LabelFont(
        fontFamily = "Segoe Print",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 15.0,
        color = Color.BLACK
    )
    val playlistsInfo = LabelFont(
        fontFamily = "Segoe Print",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 14.0,
        color = Color.BLACK
    )
    val playerTitle = LabelFont(
        fontFamily = "Gabriola",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 24.0,
        color = Color.WHITE
    )
    val trackTime = LabelFont(
        fontFamily = "Gabriola",
        fontWeight = FontWeight.EXTRA_LIGHT,
        size = 16.0
    )
    val noPlaylistsContent = LabelFont(
        fontFamily = "Gabriola",
        fontStyle = FontPosture.ITALIC,
        size = 30.0,
        color = Color.WHITE
    )
    val noTracksContent = LabelFont(
        fontFamily = "Gabriola",
        fontStyle = FontPosture.ITALIC,
        size = 28.0,
        color = Color.BLACK
    )
    val playlistTitle = LabelFont(
        fontFamily = "Times New Roman",
        size = 18.0,
        fontWeight = FontWeight.BOLD,
        color = Color.WHITE
    )
    val playlistTrackCount = LabelFont(
        fontFamily = "Times New Roman",
        size = 16.0,
        color = Color.WHITE
    )
    val playlistDuration = LabelFont(
        fontFamily = "Times New Roman",
        size = 16.0,
        color = Color.WHITE
    )
    val login = LabelFont(
        fontFamily = "Times New Roman",
        size = 16.0,
        fontWeight = FontWeight.BOLD,
        color = Color.WHITE
    )
    val avaInitials = LabelFont(
        fontFamily = "Times New Roman",
        size = 20.0,
        color = Color.WHITE
    )
    val userContextMenuItem = LabelFont(
        fontFamily = "Times New Roman",
        size = 18.0,
        fontWeight = FontWeight.SEMI_BOLD,
        color = Color.BLACK
    )
    val tabTitle = LabelFont(
        fontFamily = "Times New Roman",
        size = 20.0,
        color = Color.WHITE
    )

    val lyricsTitle = LabelFont(
        fontFamily = "Segoe Print",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 17.0,
        color = Color.BLACK
    )
    val lyricsTextTitle = LabelFont(
        fontFamily = "Segoe Print",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 15.0,
        color = Color.BLACK
    )
    val lyricsText = LabelFont(
        fontFamily = "Segoe Print",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 14.0,
        color = Color.BLACK
    )
    val lyricsExpandLabel = LabelFont(
        fontFamily = "Segoe Print",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 12.0,
        color = Color.BLACK
    )
    val downloadInfoText = LabelFont(
        fontFamily = "Segoe Print",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 16.0,
        color = Color.BLACK
    )

    val loginLogoTitle = LabelFont(
        fontFamily = "Times New Roman",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 20.0,
        color = Color.BLACK,
    )
    val loginFormLabel = LabelFont(
        fontFamily = "Times New Roman",
        size = 18.0,
        color = Color.BLACK,
    )
    val loginSubmitButton = LabelFont(
        fontFamily = "Times New Roman",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 20.0,
        color = Color.BLACK,
    )
    val userItemLogin = LabelFont(
        fontFamily = "Times New Roman",
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 18.0,
        color = Color.BLACK,
    )
}
