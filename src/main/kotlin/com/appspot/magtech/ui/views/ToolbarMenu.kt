package com.appspot.magtech.ui.views

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.player.PlayerAction
import com.appspot.magtech.application.player.PlayerStatus
import com.appspot.magtech.application.tracks.getTrackById
import com.appspot.magtech.entities.LocalTrack
import com.appspot.magtech.entities.YMusicTrack
import com.appspot.magtech.entities.displayInfo
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.extensions.flow.sendToDisposable
import com.appspot.magtech.extensions.flow.watchDisposable
import com.appspot.magtech.extensions.menuBuilder
import com.appspot.magtech.extensions.userActions
import com.appspot.magtech.ui.views.player.mapDownloadRemoveActions
import dorkbox.systemTray.Tray
import io.ktor.util.*
import javafx.animation.Animation
import javafx.animation.KeyFrame
import javafx.animation.Timeline
import javafx.event.EventHandler
import javafx.util.Duration
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.map

@InternalAPI
@ExperimentalCoroutinesApi
@FlowPreview
fun Tray.buildToolbarMenu(states: Flow<AppState>, actions: Channel<Action>, onClose: (List<AutoCloseable>) -> Unit) =
        menuBuilder(this) {
            val jobs = mutableListOf<AutoCloseable>()

            var timeline: Timeline? = null
            var index: Int

            jobs += states.distinctUntilChangedBy { it.playerState.selectedTrackId }
                    .map { it.tracksState.getTrackById(it.playerState.selectedTrackId)?.track }
                    .watchDisposable { track ->
                        timeline?.stop()
                        index = 0

                        if (track != null) {
                            val sequence = sequence {
                                while (true) {
                                    yieldAll(track.displayInfo().toCharArray().toList())
                                    yieldAll(listOf(' ', ' ', ' '))
                                }
                            }

                            timeline = Timeline(KeyFrame(Duration.seconds(0.2), EventHandler {
                                status = sequence.drop(index)
                                        .take(21)
                                        .joinToString(separator = "")
                                index++
                            })).apply {
                                cycleCount = Animation.INDEFINITE
                                play()
                            }
                        } else {
                            status = "No track selected"
                        }
                    }

            separator()
            menuItem("Previous track" + " ".repeat(9) + "Ctrl + S") {
                setImage(javaClass.getResource("/images/vector/media-seek-backward-symbolic.svg"))
                jobs += userActions.map { PlayerAction.SelectPreviousTrack }
                        .sendToDisposable(actions)
            }
            menuItem("Stop") {
                setImage(javaClass.getResource("/images/vector/media-playback-stop-symbolic.svg"))
                jobs += userActions.map { PlayerAction.Stop }
                        .sendToDisposable(actions)
            }
            menuItem {
                jobs += states.mapDistinctBy { it.playerState.status }
                        .watchDisposable { status ->
                            text = when (status) {
                                PlayerStatus.STOPPED -> {
                                    setImage(javaClass.getResource("/images/vector/media-playback-start-symbolic.svg"))
                                    "Play" + " ".repeat(27) + "Ctrl + P"
                                }
                                PlayerStatus.PAUSED -> {
                                    setImage(javaClass.getResource("/images/vector/media-playback-start-symbolic.svg"))
                                    "Resume" + " ".repeat(20) + "Ctrl + P"
                                }
                                PlayerStatus.PLAYING -> {
                                    setImage(javaClass.getResource("/images/vector/media-playback-pause-symbolic.svg"))
                                    "Pause" + " ".repeat(24) + "Ctrl + P"
                                }
                            }
                        }

                jobs += userActions.map { PlayerAction.Toggle }
                        .sendToDisposable(actions)
            }
            menuItem("Next track" + " ".repeat(15) + "Ctrl + W") {
                setImage(javaClass.getResource("/images/vector/media-seek-backward-symbolic-rtl.svg"))
                jobs += userActions.map { PlayerAction.SelectNextTrack }
                        .sendToDisposable(actions)
            }
            separator()
            menuItem {
                jobs += states.mapDistinctBy { it.tracksState.getTrackById(it.playerState.selectedTrackId) }
                        .watchDisposable {
                            if (it == null || it.track is YMusicTrack) {
                                setImage(javaClass.getResource("/images/vector/document-save-symbolic.svg"))
                                text = "Download" + " ".repeat(16) + "Ctrl + D"
                            } else if (it.track is LocalTrack) {
                                setImage(javaClass.getResource("/images/vector/edit-delete-symbolic.svg"))
                                text = "Remove" + " ".repeat(20) + "Ctrl + R"
                            }
                        }

                jobs += userActions.mapDownloadRemoveActions(states)
                        .sendToDisposable(actions)
            }
            separator()
            menuItem("Quit") {
                setImage(javaClass.getResource("/images/vector/window-close-symbolic.svg"))
                setCallback { onClose(jobs) }
            }
            jobs
        }
