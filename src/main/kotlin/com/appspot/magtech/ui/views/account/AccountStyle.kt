package com.appspot.magtech.ui.views.account

import com.appspot.magtech.ui.values.Colors
import com.appspot.magtech.ui.values.Fonts
import com.appspot.magtech.ui.values.labelFont
import javafx.geometry.Pos
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.px

class AccountStyle : Stylesheet() {

    companion object {
        const val toolbarHeight = 42

        val accountRoot by cssclass()
        val controls by cssclass()
        val login by cssclass()

        val userContextMenu by cssclass()
    }

    init {
        accountRoot {
            padding = box(5.px)
            backgroundColor += Colors.semiDarkGray
            minHeight = toolbarHeight.px

            controls {
                alignment = Pos.CENTER
                spacing = 8.px
                login {
                    labelFont = Fonts.login
                }
            }
        }

        userContextMenu {
            prefWidth = 160.px
            padding = box(5.px, 0.px, 5.px, 0.px)
            button {
                prefWidth = 200.px
                labelFont = Fonts.userContextMenuItem
                alignment = Pos.CENTER_LEFT
                wrapText = true
            }
        }
    }
}
