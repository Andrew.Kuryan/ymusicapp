package com.appspot.magtech.ui.views.account

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.account.AccountAction
import com.appspot.magtech.application.navigation.NavigationAction
import com.appspot.magtech.application.navigation.NavigationRoute
import com.appspot.magtech.entities.Offline
import com.appspot.magtech.entities.User
import com.appspot.magtech.extensions.flow.FlowView
import com.appspot.magtech.extensions.flow.fxadapters.userActions
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.extensions.flow.watchDisposable
import com.appspot.magtech.extensions.jfxButton
import com.jfoenix.controls.JFXPopup
import com.jfoenix.controls.JFXPopup.PopupHPosition
import com.jfoenix.controls.JFXPopup.PopupVPosition
import javafx.geometry.Orientation
import javafx.scene.Node
import javafx.scene.layout.VBox
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import tornadofx.*

@ExperimentalCoroutinesApi
private fun Flow<*>.transformShowPopupActions(
    states: Flow<AppState>,
    node: Node,
    offlinePopup: JFXPopup,
    userPopup: JFXPopup
) {
    map { states.map { it.accountState.auth }.first() }
        .watchDisposable {
            when (it) {
                is Offline -> offlinePopup.show(
                    node, PopupVPosition.TOP, PopupHPosition.RIGHT,
                    0.0, UserAvaStyle.avaSize + (AccountStyle.toolbarHeight - UserAvaStyle.avaSize) / 2.0
                )
                is User -> userPopup.show(
                    node, PopupVPosition.TOP, PopupHPosition.RIGHT,
                    0.0, UserAvaStyle.avaSize + (AccountStyle.toolbarHeight - UserAvaStyle.avaSize) / 2.0
                )
            }
        }
}

class AccountView(private val states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

    init {
        importStylesheet<AccountStyle>()
    }

    @ExperimentalCoroutinesApi
    private val offlinePopup = JFXPopup().apply {
        val targetPopup = this
        popupContent = VBox().apply {
            addClass(AccountStyle.userContextMenu)
            jfxButton("Log in") {
                userActions.map { NavigationAction.NavigateTo(NavigationRoute.SESSIONS) }
                    .onEach { targetPopup.hide() }
                    .sendTo(actions)
            }
        }
    }

    @ExperimentalCoroutinesApi
    private val userPopup = JFXPopup().apply {
        val targetPopup = this
        popupContent = VBox().apply {
            addClass(AccountStyle.userContextMenu)
            jfxButton("Log in to another account") {
                userActions.map { NavigationAction.NavigateTo(NavigationRoute.SESSIONS) }
                    .onEach { targetPopup.hide() }
                    .sendTo(actions)
            }
            separator(orientation = Orientation.HORIZONTAL)
            jfxButton("Logout") {
                userActions.map { AccountAction.Logout }
                    .onEach { targetPopup.hide() }
                    .sendTo(actions)
            }
        }
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = anchorpane {
        addClass(AccountStyle.accountRoot)
        hbox {
            addClass(AccountStyle.controls)
            anchorpaneConstraints {
                topAnchor = 0.0
                rightAnchor = 5.0
                bottomAnchor = 0.0
            }
            label {
                addClass(AccountStyle.login)
                textProperty() bindFlow states
                    .mapDistinctBy { it.accountState.auth.displayName() }
            }
            add(
                UserAvaView(
                    states,
                    { this.accountState.auth },
                    { transformShowPopupActions(states, it, offlinePopup, userPopup) }
                ).root
            )
        }
    }
}
