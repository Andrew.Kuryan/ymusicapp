package com.appspot.magtech.ui.views.account

import com.appspot.magtech.ui.values.Colors
import com.appspot.magtech.ui.values.Fonts
import com.appspot.magtech.ui.values.labelFont
import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.px

class UserAvaStyle : Stylesheet() {

    companion object {
        const val avaSize = 32

        val ava by cssclass()
    }

    init {
        ava {
            backgroundColor += Colors.semiLightYellow
            prefWidth = avaSize.px
            prefHeight = avaSize.px
            maxHeight = avaSize.px
            maxWidth = avaSize.px
            label {
                labelFont = Fonts.avaInitials
            }
        }
    }
}