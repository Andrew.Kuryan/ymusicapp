package com.appspot.magtech.ui.views.account

import com.appspot.magtech.application.AppState
import com.appspot.magtech.entities.Auth
import com.appspot.magtech.entities.Offline
import com.appspot.magtech.entities.User
import com.appspot.magtech.extensions.flow.FlowView
import com.appspot.magtech.extensions.flow.fxadapters.clicks
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.ui.components.initialsava.initialsAva
import com.appspot.magtech.ui.components.placeholderPane
import javafx.scene.Node
import javafx.scene.image.Image
import javafx.scene.shape.Circle
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import tornadofx.addClass
import tornadofx.imageview
import tornadofx.importStylesheet
import tornadofx.pane

fun Auth.displayName() =
    when (this) {
        is User -> this.login
        is Offline -> "Not authorized"
    }

class ActiveAvaView(
    states: Flow<AppState>,
    extractUserFromState: AppState.() -> Auth?,
    transformClick: Flow<*>.(Node) -> Unit
) : FlowView() {

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = pane {
        maxWidth = UserAvaStyle.avaSize.toDouble()
        maxHeight = UserAvaStyle.avaSize.toDouble()
        imageview {
            fitWidth = UserAvaStyle.avaSize.toDouble()
            fitHeight = UserAvaStyle.avaSize.toDouble()

            clip = Circle(fitWidth / 2, fitHeight / 2, fitWidth / 2)

            imageProperty() bindFlow states
                .mapDistinctBy { it.extractUserFromState() }
                .filterIsInstance<User>()
                .mapNotNull { it.avatarUrl }
                .map { Image("https://avatars.mds.yandex.net/get-yapic/$it/islands-retina-middle", true) }

            clicks.transformClick(this@pane)
        }
    }
}

class PlaceholderAvaView(
    states: Flow<AppState>,
    extractUserFromState: AppState.() -> Auth?,
    transformClick: Flow<*>.(Node) -> Unit
) : FlowView() {

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = initialsAva {
        addClass(UserAvaStyle.ava)
        nameProperty bindFlow states
            .mapDistinctBy { it.extractUserFromState() }
            .map { it?.displayName() }

        clicks.transformClick(this)
    }
}

class UserAvaView(
    states: Flow<AppState>,
    extractUserFromState: AppState.() -> Auth?,
    transformClick: Flow<*>.(Node) -> Unit = {}
) : FlowView() {

    init {
        importStylesheet<UserAvaStyle>()
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    override val root = placeholderPane(
        ActiveAvaView(states, extractUserFromState, transformClick).root,
        PlaceholderAvaView(states, extractUserFromState, transformClick).root
    ) {
        isActiveProperty bindFlow states
            .mapDistinctBy { it.extractUserFromState() }
            .mapNotNull { it is User && it.avatarUrl != null }
    }
}