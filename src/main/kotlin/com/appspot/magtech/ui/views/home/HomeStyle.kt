package com.appspot.magtech.ui.views.home

import com.appspot.magtech.ui.values.Colors
import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.px

class HomeStyle : Stylesheet() {

    companion object {
        val centerBox by cssclass()
    }

    init {
        centerBox {
            backgroundColor += Colors.white
            minHeight = 0.px
        }
    }
}