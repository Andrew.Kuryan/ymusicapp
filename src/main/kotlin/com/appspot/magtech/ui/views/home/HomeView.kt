package com.appspot.magtech.ui.views.home

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppState
import com.appspot.magtech.extensions.flow.FlowView
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.ui.components.fetchingview.fetchingWrapper
import com.appspot.magtech.ui.views.account.AccountView
import com.appspot.magtech.ui.views.main.MainStyle
import com.appspot.magtech.ui.views.player.PlayerView
import com.appspot.magtech.ui.views.sections.SectionsView
import com.appspot.magtech.ui.views.tracklyrics.TrackLyricsView
import com.appspot.magtech.ui.views.tracks.CombinedView
import javafx.scene.layout.ColumnConstraints
import javafx.scene.layout.Priority
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import tornadofx.*

class HomeView(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

    init {
        importStylesheet<HomeStyle>()
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = vbox {
        anchorpaneConstraints {
            topAnchor = 0.0
            leftAnchor = 0.0
            bottomAnchor = 0.0
            rightAnchor = 0.0
        }
        fetchingWrapper(AccountView(states, actions)) {
            isFetchingProperty bindFlow states
                .mapDistinctBy { it.accountState.isFetching }
        }
        gridpane {
            vgrow = Priority.ALWAYS
            addClass(MainStyle.centerBox)
            columnConstraints.addAll(arrayOf(55.0, 45.0).map {
                ColumnConstraints().apply { percentWidth = it }
            })
            row {
                fetchingWrapper(CombinedView(states, actions)) {
                    isFetchingProperty bindFlow states
                        .mapDistinctBy { it.tracksState.isFetching }
                }
                stackpane {
                    fetchingWrapper(SectionsView(states, actions)) {
                        isFetchingProperty bindFlow states
                            .mapDistinctBy { it.sectionsState.isFetching }
                    }
                    add(TrackLyricsView(states, actions))
                }
            }
        }
        fetchingWrapper(PlayerView(states, actions)) {
            isFetchingProperty bindFlow states
                .mapDistinctBy { it.playerState.isFetching }
        }
    }
}