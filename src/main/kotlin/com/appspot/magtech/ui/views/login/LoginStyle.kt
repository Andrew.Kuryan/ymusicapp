package com.appspot.magtech.ui.views.login

import com.appspot.magtech.ui.values.Colors
import com.appspot.magtech.ui.values.Fonts
import com.appspot.magtech.ui.values.labelFont
import com.appspot.magtech.ui.views.main.MainStyle
import javafx.geometry.Pos
import javafx.scene.effect.DropShadow
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.paint.Color
import tornadofx.*

class LoginStyle : Stylesheet() {

    companion object {
        const val contentCardWidth = 350
        const val contentCardHeight = 500

        val loginRoot by cssclass()

        val logoHeader by cssclass()
        val logoWrapper by cssclass()
        val logoTitle by cssclass()

        val formBlock by cssclass()
        val formField by cssclass()
        val submitButton by cssclass()
        val goToSessionsButton by cssclass()

        val buttonsBlock by cssclass()
        val contentCard by cssclass()
        val homeButton by cssclass()

        val jfxFocusColor by cssproperty<Color>("-jfx-focus-color")
        val jfxCheckedColor by cssproperty<Color>("-jfx-checked-color")
    }

    init {
        loginRoot {
            prefWidth = MainStyle.prefWinWidth.px
            prefHeight = MainStyle.prefWinHeight.px
            alignment = Pos.CENTER

            logoHeader {
                alignment = Pos.TOP_CENTER
                prefWidth = contentCardWidth.px
                spacing = 10.px
                logoWrapper {
                    prefWidth = 80.px
                    prefHeight = 80.px
                    backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                    backgroundPosition += BackgroundPosition.CENTER
                    backgroundSize += BackgroundSize(
                        80.0, 80.0,
                        false, false, false, false
                    )
                    backgroundImage += this::class.java.getResource("/images/common/yandex.png").toURI()
                }
                logoTitle {
                    labelFont = Fonts.loginLogoTitle
                    alignment = Pos.CENTER
                }
            }

            formBlock {
                spacing = 18.px
                formField {
                    label {
                        labelFont = Fonts.loginFormLabel
                    }
                    checkBox {
                        labelFont = Fonts.loginFormLabel
                        jfxCheckedColor.set(Colors.primeYellow)
                    }
                    textField {
                        jfxFocusColor.set(Colors.primeYellow)
                        fontSize = 18.px
                    }
                    passwordField {
                        fontSize = 16.px
                    }
                }
            }

            buttonsBlock {
                spacing = 10.px
                submitButton {
                    prefWidth = contentCardWidth.px
                    prefHeight = 40.px
                    backgroundColor += Colors.lightYellow
                    backgroundRadius += box(10.px)
                    labelFont = Fonts.loginSubmitButton
                }

                goToSessionsButton {
                    prefWidth = contentCardWidth.px
                    prefHeight = 40.px
                    borderWidth += box(1.px)
                    borderColor += box(Colors.lightGray)
                    borderRadius += box(10.px)
                    labelFont = Fonts.loginSubmitButton
                }
            }
        }

        contentCard {
            prefWidth = contentCardWidth.px
            prefHeight = contentCardHeight.px
            backgroundColor += Color.WHITE
            backgroundRadius += box(12.px)
            effect = DropShadow(12.0, 0.0, 0.0, c(0, 0, 0, 0.5))
            padding = box(20.px)
            spacing = 40.px
        }

        homeButton {
            val butSize = 40
            prefWidth = butSize.px
            prefHeight = butSize.px
            backgroundColor += Color.WHITE
            backgroundRadius += box((butSize / 2).px)
            translateX = (-(butSize / 2)).px
            translateY = 50.px
            effect = DropShadow(butSize / 2.0, 0.0, 0.0, c(0, 0, 0, 0.1))

            backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
            backgroundPosition += BackgroundPosition.CENTER
            backgroundSize += BackgroundSize(
                butSize - 15.0, butSize - 15.0,
                false, false, false, false
            )
            backgroundImage += this::class.java.getResource("/images/common/home.png").toURI()
        }
    }
}