package com.appspot.magtech.ui.views.login

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.account.AccountAction
import com.appspot.magtech.application.navigation.NavigationAction
import com.appspot.magtech.application.navigation.NavigationRoute
import com.appspot.magtech.extensions.flow.FlowView
import com.appspot.magtech.extensions.flow.fxadapters.userActions
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.extensions.jfxButton
import com.appspot.magtech.extensions.jfxCheckbox
import com.appspot.magtech.extensions.jfxPasswordField
import com.appspot.magtech.extensions.jfxTextField
import com.appspot.magtech.ui.components.fetchingview.fetchingWrapper
import com.appspot.magtech.ui.views.main.MainStyle
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import tornadofx.*

class LoginModel {
    val loginProperty = SimpleStringProperty("")
    val passwordProperty = SimpleStringProperty("")
    val needToSaveProperty = SimpleBooleanProperty(true)
}

class LoginViewModel : ItemViewModel<LoginModel>() {
    val login = bind(LoginModel::loginProperty)
    val password = bind(LoginModel::passwordProperty)
    val needToSave = bind(LoginModel::needToSaveProperty)
}

class LoginView(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

    init {
        importStylesheet<LoginStyle>()
    }

    class LoginViewContent(actions: Channel<Action>) : FlowView() {

        private val viewModel: LoginViewModel by inject()

        @ExperimentalCoroutinesApi
        override val root = flowpane {
            addClass(LoginStyle.loginRoot)

            vbox {
                addClass(LoginStyle.contentCard)
                vbox {
                    addClass(LoginStyle.logoHeader)
                    pane { addClass(LoginStyle.logoWrapper) }
                    label("Log in with Yandex ID") { addClass(LoginStyle.logoTitle) }
                }
                vbox {
                    addClass(LoginStyle.formBlock)
                    vbox {
                        addClass(LoginStyle.formField)
                        label("Login")
                        jfxTextField(viewModel.login)
                    }
                    vbox {
                        addClass(LoginStyle.formField)
                        label("Password")
                        jfxPasswordField(viewModel.password)
                    }
                    vbox {
                        addClass(LoginStyle.formField)
                        jfxCheckbox("Remember this account", viewModel.needToSave)
                    }
                }
                vbox {
                    addClass(LoginStyle.buttonsBlock)
                    jfxButton("Log in") {
                        addClass(LoginStyle.submitButton)
                        userActions.map {
                            AccountAction.Auth(
                                viewModel.login.get() ?: "",
                                viewModel.password.get() ?: "",
                                viewModel.needToSave.get()
                            )
                        }.sendTo(actions)
                    }
                    jfxButton("Choose existing account") {
                        addClass(LoginStyle.goToSessionsButton)
                        userActions.map { NavigationAction.NavigateTo(NavigationRoute.SESSIONS) }
                            .sendTo(actions)
                    }
                }
            }
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    override val root = anchorpane {
        anchorpaneConstraints {
            topAnchor = 0.0
            leftAnchor = 0.0
            bottomAnchor = 0.0
            rightAnchor = 0.0
        }

        fetchingWrapper(LoginViewContent(actions)) {
            isFetchingProperty bindFlow states
                .mapDistinctBy { it.accountState.isFetching }
        }

        jfxButton {
            addClass(LoginStyle.homeButton)
            layoutX = MainStyle.prefWinWidth / 2.0 - LoginStyle.contentCardWidth / 2.0
            layoutY = MainStyle.prefWinHeight / 2.0 - LoginStyle.contentCardHeight / 2.0
            userActions.map { NavigationAction.NavigateTo(NavigationRoute.HOME) }
                .sendTo(actions)
        }
    }
}