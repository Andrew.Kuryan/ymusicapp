package com.appspot.magtech.ui.views.main

import com.appspot.magtech.ui.values.Colors
import javafx.scene.paint.Color
import tornadofx.*

class MainStyle : Stylesheet() {

    companion object {
        const val prefWinWidth = 981.0
        const val prefWinHeight = 700.0

        val mainWrapper by cssclass()
        val centerBox by cssclass()
        lateinit var materialListView: CssSelectionBlock
    }

    init {
        mainWrapper {
            prefWidth = prefWinWidth.px
            prefHeight = prefWinHeight.px

            materialListView = mixin {
                focusColor = Color.TRANSPARENT
                faintFocusColor = Color.TRANSPARENT
                borderWidth += box(0.px)
                backgroundInsets += box(0.px)
                padding = box(0.px)
                and(focused) {
                    backgroundColor += Color.TRANSPARENT
                }
                corner {
                    backgroundColor += Colors.darkGray
                }
                scrollBar {
                    track {
                        backgroundColor += Colors.darkGray
                        borderColor += box(Color.TRANSPARENT)
                        backgroundRadius += box(0.em)
                        borderRadius += box(2.em)
                    }
                    s(incrementButton, decrementButton) {
                        backgroundRadius += box(0.em)
                    }
                    s(incrementArrow, decrementArrow) {
                        shape = " "
                        padding = box(0.px)
                    }
                    thumb {
                        backgroundColor += Colors.lightYellow
                        backgroundRadius += box(0.em)
                    }
                }
                scrollBar and vertical {
                    s(incrementButton, decrementButton) {
                        padding = box(0.px, 8.px, 0.px, 0.px)
                    }
                }
                scrollBar and horizontal {
                    s(incrementButton, decrementButton) {
                        padding = box(0.px, 0.px, 8.px, 0.px)
                    }
                }
            }

            s(scrollPane, listView) {
                +materialListView
            }
        }
    }
}
