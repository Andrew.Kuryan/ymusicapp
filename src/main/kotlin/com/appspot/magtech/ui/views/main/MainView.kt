package com.appspot.magtech.ui.views.main

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppAction
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.navigation.NavigationRoute
import com.appspot.magtech.extensions.flow.FlowView
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.ui.components.erroralert.showErrorAlert
import com.appspot.magtech.ui.components.notification.showNotification
import com.appspot.magtech.ui.views.home.HomeView
import com.appspot.magtech.ui.views.login.LoginView
import com.appspot.magtech.ui.views.sessions.SessionsView
import com.appspot.magtech.uiports.NotificationManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import tornadofx.addClass
import tornadofx.anchorpane
import tornadofx.importStylesheet

@ExperimentalCoroutinesApi
@FlowPreview
class MainView(
    states: Flow<AppState>,
    actions: Channel<Action>,
    notificationManager: NotificationManager
) : FlowView("Yandex Music") {

    init {
        importStylesheet<MainStyle>()
    }

    private val routes = mapOf(
        NavigationRoute.HOME to HomeView(states, actions),
        NavigationRoute.SESSIONS to SessionsView(states, actions),
        NavigationRoute.LOGIN to LoginView(states, actions)
    )

    override val root = anchorpane {
        addClass(MainStyle.mainWrapper)
        states.mapDistinctBy { it.navigationState }
            .watch {
                if (it.previousRoute != null) {
                    this.children.remove(routes.getValue(it.previousRoute).root)
                }
                this.children.add(routes.getValue(it.currentRoute).root)
            }
    }

    init {
        states.mapDistinctBy { it.infoMessage }
            .filterNotNull()
            .watch { root.showNotification(it, currentWindow?.isFocused ?: false, notificationManager) }

        states.mapDistinctBy { it.error }
            .filterNotNull()
            .watch {
                root.showErrorAlert(it.message ?: "Something went wrong") {
                    closeActions.map { AppAction.ResetError }
                        .sendTo(actions)
                }
            }
    }
}
