package com.appspot.magtech.ui.views.player

import javafx.beans.property.SimpleObjectProperty
import javafx.util.Duration
import tornadofx.ItemViewModel
import tornadofx.doubleBinding
import tornadofx.stringBinding

class PlayerModel {
    val currentProgressTimeProperty = SimpleObjectProperty(Duration.ZERO)
    val bufferedProgressProperty = SimpleObjectProperty(Duration.ZERO)
    val totalTimeProperty = SimpleObjectProperty(Duration.ZERO)
}

class PlayerViewModel : ItemViewModel<PlayerModel>() {
    val currentProgressTime = bind(PlayerModel::currentProgressTimeProperty)
    val bufferedProgress = bind(PlayerModel::bufferedProgressProperty)
    val totalTime = bind(PlayerModel::totalTimeProperty)

    val currentProgressValue = currentProgressTime.doubleBinding {
        if (it != null) {
            it.toMillis() / (totalTime.value?.toMillis() ?: 1.0)
        } else 0.0
    }
    val bufferedProgressValue = bufferedProgress.doubleBinding {
        if (it != null) {
            it.toMillis() / (totalTime.value?.toMillis() ?: 1.0)
        } else 0.0
    }
    val currentProgressTimeFormatted = currentProgressTime.stringBinding {
        if (it != null) {
            val currentSeconds = it.toMillis().toLong() / 1000
            String.format("%02d:%02d", currentSeconds / 60, currentSeconds % 60)
        } else "00:00"
    }
}
