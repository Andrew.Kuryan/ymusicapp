package com.appspot.magtech.ui.views.player

import com.appspot.magtech.ui.values.Colors
import com.appspot.magtech.ui.values.Fonts
import com.appspot.magtech.ui.values.labelFont
import javafx.geometry.Pos
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.paint.Color
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.px

class PlayerStyle : Stylesheet() {

    companion object {
        const val playerHeight = 110
        const val controlSize = 32
        const val progressHeight = 15

        val playerRoot by cssclass()

        val progressWrapper by cssclass()
        val secondaryBar by cssclass()
        val labTime by cssclass()

        val controls by cssclass()
        val controlButtonBox by cssclass()
        val butPrev by cssclass()
        val butNext by cssclass()
        val butPlay by cssclass()
        val butPause by cssclass()
        val butStop by cssclass()

        val trackAppereanceBlock by cssclass()
        val trackInfo by cssclass()
        val trackTitle by cssclass()
        val artistName by cssclass()

        val advancedControls by cssclass()
        val butSave by cssclass()
        val butRemove by cssclass()
    }

    init {
        playerRoot {
            minHeight = playerHeight.px
            prefHeight = playerHeight.px
            backgroundColor += Colors.primeGray
            alignment = Pos.CENTER

            progressWrapper {
                prefHeight = progressHeight.px
                progressBar {
                    prefHeight = progressHeight.px
                    backgroundColor += Colors.lightGray
                    bar {
                        backgroundColor += Colors.lightYellow
                    }
                    secondaryBar {
                        backgroundColor += Colors.silver
                    }
                }
                labTime {
                    labelFont = Fonts.trackTime
                }
            }

            controls {
                alignment = Pos.CENTER_LEFT
                spacing = 60.px
                padding = box(0.px, 35.px, 0.px, 20.px)
                prefHeight = (playerHeight - progressHeight).px

                button {
                    prefWidth = controlSize.px
                    prefHeight = controlSize.px
                    backgroundColor += Color.TRANSPARENT
                    backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                    backgroundPosition += BackgroundPosition.CENTER
                    backgroundSize += BackgroundSize(controlSize.toDouble(), controlSize.toDouble(),
                            false, false, false, false)
                }

                controlButtonBox {
                    alignment = Pos.CENTER
                    spacing = 8.px

                    butPrev {
                        backgroundImage += this::class.java.getResource("/images/common/prev.png").toURI()
                    }
                    butNext {
                        backgroundImage += this::class.java.getResource("/images/common/next.png").toURI()
                    }
                    butPlay {
                        backgroundImage += this::class.java.getResource("/images/common/play.png").toURI()
                    }
                    butPause {
                        backgroundImage += this::class.java.getResource("/images/common/media-playback-pause.png").toURI()
                    }
                    butStop {
                        backgroundImage += this::class.java.getResource("/images/common/stop.png").toURI()
                    }
                }

                trackAppereanceBlock {
                    alignment = Pos.CENTER_LEFT
                    spacing = 20.px

                    imageView {
                        backgroundColor += Colors.lightGray
                    }

                    trackInfo {
                        alignment = Pos.CENTER_LEFT
                        spacing = 4.px
                        trackTitle {
                            labelFont = Fonts.playerTitle
                        }
                        artistName {
                            labelFont = Fonts.playerTitle
                        }
                    }
                }
            }

            advancedControls {
                spacing = 25.px
                alignment = Pos.CENTER_LEFT
                button {
                    prefHeight = 30.px; prefWidth = 30.px
                    backgroundSize += BackgroundSize(30.0, 30.0,
                            false, false, false, false)
                    backgroundPosition += BackgroundPosition.CENTER
                }

                butSave {
                    backgroundImage += this::class.java.getResource("/images/standard/save-new.png").toURI()
                }
                butRemove {
                    backgroundImage += this::class.java.getResource("/images/standard/delete-new.png").toURI()
                }
            }
        }
    }
}
