package com.appspot.magtech.ui.views.player

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.player.PlayerAction
import com.appspot.magtech.application.player.PlayerStatus
import com.appspot.magtech.application.tracks.TracksAction
import com.appspot.magtech.application.tracks.getTrackById
import com.appspot.magtech.entities.LocalTrack
import com.appspot.magtech.entities.YMusicTrack
import com.appspot.magtech.extensions.flow.FlowView
import com.appspot.magtech.extensions.flow.fxadapters.endsOfMedia
import com.appspot.magtech.extensions.flow.fxadapters.userActions
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.extensions.flow.sendToDisposable
import com.appspot.magtech.extensions.jfxButton
import com.appspot.magtech.extensions.jfxProgressBar
import javafx.event.EventHandler
import javafx.scene.image.Image
import javafx.scene.layout.Priority
import javafx.scene.media.Media
import javafx.scene.media.MediaPlayer
import javafx.util.Duration
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import tornadofx.*

@ExperimentalCoroutinesApi
@FlowPreview
class PlayerView(states: Flow<AppState>, private val actions: Channel<Action>) : FlowView() {

    private val viewModel: PlayerViewModel by inject()

    private fun createMediaPlayer(link: String) = MediaPlayer(Media(link)).apply {
        viewModel.currentProgressTime.bind(currentTimeProperty())
        viewModel.bufferedProgress.bind(bufferProgressTimeProperty())
        viewModel.totalTime.bind(totalDurationProperty())

        val job = endsOfMedia.map { PlayerAction.SelectNextTrack }
                .sendToDisposable(actions)

        statusProperty().addListener { _, _, newValue ->
            if (newValue == MediaPlayer.Status.DISPOSED) {
                job.close()
            }
        }
    }

    private var player: MediaPlayer? = null

    init {
        importStylesheet<PlayerStyle>()

        states.mapDistinctBy { it.playerState.selectedTrackLink }
                .onEach {
                    player?.stop()
                    player?.dispose()
                }
                .onEach {
                    player = if (it != null) createMediaPlayer(it) else null
                }
                .combine(states.mapDistinctBy { it.playerState.status }) { link, status -> link to status }
                .watch { (_, status) ->
                    when (status) {
                        PlayerStatus.PLAYING -> player?.play()
                        PlayerStatus.PAUSED -> player?.pause()
                        PlayerStatus.STOPPED -> player?.stop()
                    }
                }
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = vbox {
        addClass(PlayerStyle.playerRoot)
        anchorpane {
            addClass(PlayerStyle.progressWrapper)
            jfxProgressBar {
                anchorpaneConstraints {
                    topAnchor = 0.0
                    bottomAnchor = 0.0
                    leftAnchor = 0.0
                    rightAnchor = 0.0
                }

                onMouseClicked = EventHandler {
                    player?.seek(Duration((viewModel.totalTime.value?.toMillis() ?: 1.0) * (it.x / width)))
                }

                progressProperty().bind(viewModel.currentProgressValue)
                secondaryProgressProperty().bind(viewModel.bufferedProgressValue)
            }
            label {
                addClass(PlayerStyle.labTime)
                anchorpaneConstraints {
                    topAnchor = -2.0
                    leftAnchor = 5.0
                }
                isMouseTransparent = true
                textProperty() bindFlow states
                        .map { it.tracksState.getTrackById(it.playerState.selectedTrackId) }
                        .mapDistinctBy { it?.track?.info?.duration }
                        .map {
                            if (it != null) {
                                String.format("%02d:%02d", (it / 1000) / 60, (it / 1000) % 60)
                            } else "00:00"
                        }
            }
            label(viewModel.currentProgressTimeFormatted) {
                addClass(PlayerStyle.labTime)
                anchorpaneConstraints {
                    topAnchor = -2.0
                    rightAnchor = 5.0
                }
                isMouseTransparent = true
            }
        }
        hbox {
            addClass(PlayerStyle.controls)
            hbox {
                addClass(PlayerStyle.controlButtonBox)
                jfxButton {
                    addClass(PlayerStyle.butPrev)
                    userActions.map { PlayerAction.SelectPreviousTrack }
                            .sendTo(actions)
                }
                jfxButton {
                    addClass(PlayerStyle.butStop)
                    userActions.map { PlayerAction.Stop }
                            .sendTo(actions)
                }
                jfxButton {
                    userActions.map { PlayerAction.Toggle }
                            .sendTo(actions)

                    states.mapDistinctBy { it.playerState.status }
                            .watch {
                                toggleClass(PlayerStyle.butPlay, it == PlayerStatus.STOPPED || it == PlayerStatus.PAUSED)
                                toggleClass(PlayerStyle.butPause, it == PlayerStatus.PLAYING)
                            }
                }
                jfxButton {
                    addClass(PlayerStyle.butNext)
                    userActions.map { PlayerAction.SelectNextTrack }
                            .sendTo(actions)
                }
            }
            hbox {
                addClass(PlayerStyle.trackAppereanceBlock)
                hgrow = Priority.ALWAYS
                imageview {
                    fitWidth = 64.0
                    fitHeight = 64.0
                    imageProperty() bindFlow states
                            .mapDistinctBy { it.playerState.selectedTrackImageLink }
                            .map { if (it != null) Image(it, true) else null }
                }
                vbox {
                    addClass(PlayerStyle.trackInfo)
                    label {
                        addClass(PlayerStyle.trackTitle)
                        textProperty() bindFlow states
                                .map { it.tracksState.getTrackById(it.playerState.selectedTrackId) }
                                .mapDistinctBy { it?.track?.info?.title }
                    }
                    label {
                        addClass(PlayerStyle.artistName)
                        vgrow = Priority.ALWAYS
                        textProperty() bindFlow states
                                .map { it.tracksState.getTrackById(it.playerState.selectedTrackId) }
                                .mapDistinctBy { it?.track?.info?.artists }
                                .map { it?.joinToString(separator = ", ") { artist -> artist.name } }
                    }
                }
            }

            hbox {
                addClass(PlayerStyle.advancedControls)
                jfxButton {
                    states.mapDistinctBy { it.tracksState.getTrackById(it.playerState.selectedTrackId) }
                            .watch {
                                toggleClass(PlayerStyle.butSave,
                                        it == null || it.track is YMusicTrack)
                                toggleClass(PlayerStyle.butRemove,
                                        it != null && it.track is LocalTrack)
                            }

                    userActions.mapDownloadRemoveActions(states)
                            .sendTo(actions)
                }
            }
        }
    }
}

fun Flow<*>.mapDownloadRemoveActions(states: Flow<AppState>) =
        map {
            states.map { it.tracksState.getTrackById(it.playerState.selectedTrackId) }
                    .first()
        }.filterNotNull().filterNot { it.isFetching }.map {
            when (it.track) {
                is LocalTrack -> TracksAction.RemoveTrack(it.track)
                is YMusicTrack -> TracksAction.DownloadTrack(it.track)
            }
        }
