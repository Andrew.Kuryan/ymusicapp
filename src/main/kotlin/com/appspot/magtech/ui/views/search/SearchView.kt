package com.appspot.magtech.ui.views.search

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.player.PlayerAction
import com.appspot.magtech.application.tracklyrics.TrackLyricsAction
import com.appspot.magtech.application.tracks.TrackElement
import com.appspot.magtech.application.tracks.WEB_RESULTS_PLAYLIST_TITLE
import com.appspot.magtech.application.tracks.applyFilter
import com.appspot.magtech.entities.isAvailable
import com.appspot.magtech.extensions.flow.FlowView
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.ui.components.fetchingview.fetchingWrapper
import com.appspot.magtech.ui.components.placeholderPane
import com.appspot.magtech.ui.views.tracks.*
import javafx.scene.input.MouseEvent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import tornadofx.*

fun Flow<AppState>.mapFilteredTracks(): Flow<List<Pair<String, TrackElement>>> =
    map { it.tracksState.tracks to it.searchState }
        .map { (tracks, searchState) -> tracks.applyFilter(searchState) }

@FlowPreview
fun Flow<AppState>.mapTrackElementFromCache(index: Int): Flow<TrackElement> =
    mapFilteredTracks()
        .mapDistinctBy { it.getOrNull(index)?.second }
        .filterNotNull()

@FlowPreview
fun Flow<AppState>.mapTrackElementFromWeb(index: Int): Flow<TrackElement> =
    map { it.tracksState }
        .mapDistinctBy { it.tracks[WEB_RESULTS_PLAYLIST_TITLE]?.getOrNull(index) }
        .filterNotNull()

fun Flow<MouseEvent>.mapFilteredItemClicks(states: Flow<AppState>, index: Int): Flow<Action> =
    map {
        states.mapFilteredTracks().first() to states
            .map { it.playerState }
            .map { it.selectedTrackId }
            .first()
    }.mapNotNull { (tracks, selectedTrackId) ->
        if (tracks[index].second.track.isAvailable()) {
            if (selectedTrackId == tracks[index].second.track.id) {
                TrackLyricsAction.Open
            } else {
                PlayerAction.SelectTrack(tracks[index].second.track.id, tracks[index].first)
            }
        } else null
    }

fun Flow<MouseEvent>.mapWebItemClick(states: Flow<AppState>, index: Int): Flow<Action> =
    map {
        states.map { it.tracksState }
            .map { it.tracks[WEB_RESULTS_PLAYLIST_TITLE] }
            .first() to states
            .map { it.playerState }
            .map { it.selectedTrackId }
            .first()
    }.mapNotNull { (tracks, selectedTrackId) ->
        if (tracks != null && tracks[index].track.isAvailable()) {
            if (selectedTrackId == tracks[index].track.id) {
                TrackLyricsAction.Open
            } else {
                PlayerAction.SelectTrack(tracks[index].track.id, WEB_RESULTS_PLAYLIST_TITLE)
            }
        } else null
    }

class SearchView(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

    private class CacheResultsView(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

        @FlowPreview
        override val root = vbox {
            label("Results from cache:") {
                addClass(TracksStyle.listParagraph)
            }
            add(
                TrackItemsListView(
                    states, actions,
                    { this.tracksState.tracks.applyFilter(this.searchState) },
                    { this.second.track.id },
                    Flow<AppState>::mapTrackElementFromCache,
                    Flow<MouseEvent>::mapFilteredItemClicks
                )
            )
        }
    }

    private class WebResultsView(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

        @FlowPreview
        override val root = vbox {
            label("Results from web:") {
                addClass(TracksStyle.listParagraph)
            }
            fetchingWrapper(
                TrackItemsListView(
                    states, actions,
                    { this.tracksState.tracks[WEB_RESULTS_PLAYLIST_TITLE] },
                    { this.track.id },
                    Flow<AppState>::mapTrackElementFromWeb,
                    Flow<MouseEvent>::mapWebItemClick,
                )
            ) {
                isFetchingProperty bindFlow states
                    .mapDistinctBy { it.searchState.isFetching }
            }
        }
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = anchorpane {
        addClass(TracksStyle.tracksRoot)
        vbox {
            anchorpaneConstraints {
                topAnchor = 0.0
                leftAnchor = 0.0
                rightAnchor = 0.0
                bottomAnchor = 0.0
            }
            placeholderPane(
                CacheResultsView(states, actions).root,
                TracksPlaceholderView("No results from cache").root
            ) {
                isActiveProperty bindFlow states.mapFilteredTracks()
                    .mapDistinctBy { it.isNotEmpty() }
            }
            placeholderPane(
                WebResultsView(states, actions).root,
                TracksPlaceholderView("No results from web").root
            ) {
                isActiveProperty bindFlow states.map { it.tracksState }
                    .mapDistinctBy { it.tracks[WEB_RESULTS_PLAYLIST_TITLE]?.isNotEmpty() }
            }
        }
    }
}