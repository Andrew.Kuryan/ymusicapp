package com.appspot.magtech.ui.views.sections

import com.appspot.magtech.ui.components.flattabpane.FlatTabPaneStyle.Companion.flatTabPane
import com.appspot.magtech.ui.components.flattabpane.FlatTabPaneStyle.Companion.selectedTab
import com.appspot.magtech.ui.values.Colors
import com.appspot.magtech.ui.values.Fonts
import com.appspot.magtech.ui.values.labelFont
import com.appspot.magtech.ui.views.main.MainStyle
import javafx.geometry.Pos
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.paint.Color
import tornadofx.*

class SectionsStyle : Stylesheet() {

    companion object {
        val playlistsRoot by cssclass()

        val tabInactive by cssclass()
        val refreshButton by cssclass()

        val playlist by cssclass()
        val titleBox by cssclass()
        val inactiveItem by cssclass()
        val infoBox by cssclass()
        val labPlaylists by cssclass()
        val labCount by cssclass()
        val labDuration by cssclass()

        val emptyPlaylistsPane by cssclass()
        val emptyMessage by cssclass()

        val newPlaylistBox by cssclass()
        lateinit var newPlaylistPane: CssSelectionBlock
    }

    init {
        playlistsRoot {
            prefHeight = MainStyle.prefWinHeight.px
            prefWidth = MainStyle.prefWinWidth.px

            refreshButton {
                maxHeight = 28.px
                minHeight = 28.px
                maxWidth = 28.px
                minWidth = 28.px
                backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                backgroundPosition += BackgroundPosition.CENTER
                backgroundSize += BackgroundSize(
                    16.0, 16.0,
                    false, false, false, false)
                backgroundImage += this::class.java.getResource("/images/common/refresh.png").toURI()
            }

            flatTabPane {
                borderWidth += box(0.px)
                tabHeaderArea {
                    tabDownButton {
                        alignment = Pos.CENTER_LEFT
                        prefHeight = 65.px
                        minHeight = 65.px
                        backgroundColor += Colors.lightGray
                        borderColor += box(Colors.white)
                        borderWidth += box(0.px, 0.px, 0.2.px, 0.px)
                        padding = box(0.px, 0.px, 0.px, 5.px)

                        and(tabInactive) {
                            button {
                                padding = box(0.px, 0.px, 0.px, 28.px)
                            }
                        }

                        button {
                            prefHeight = 65.px
                            labelFont = Fonts.tabTitle
                            alignment = Pos.CENTER_LEFT
                            padding = box(0.px)
                        }
                    }
                    selectedTab {
                        borderWidth += box(0.px, 0.px, 2.px, 0.px)
                        borderColor += box(Colors.lightYellow)
                    }
                }
                backgroundColor += Colors.lightGray
            }
        }

        playlist {
            backgroundColor += Colors.lightGray
            padding = box(0.px)
            listCell {
                prefHeight = 60.px
                padding = box(5.px)
                backgroundColor += Colors.lightGray
                and(filled) {
                    borderWidth += box(0.2.px, 0.px, 0.2.px, 0.px)
                    borderColor += box(Colors.white)
                }
                and(selected) {
                    borderWidth += box(2.px)
                    borderColor += box(Colors.lightYellow)
                    padding = box(3.px)
                }
                titleBox {
                    alignment = Pos.CENTER_LEFT
                    padding = box(0.px, 0.px, 0.px, 28.px)

                    and(inactiveItem) {
                        padding = box(0.px, 0.px, 0.px, 0.px)
                    }

                    labPlaylists {
                        labelFont = Fonts.playlistTitle
                    }
                }
                infoBox {
                    alignment = Pos.CENTER
                    spacing = 3.px
                    labCount {
                        labelFont = Fonts.playlistTrackCount
                    }
                    labDuration {
                        labelFont = Fonts.playlistDuration
                    }
                }
            }
        }

        newPlaylistPane = mixin {
            borderColor += box(Colors.white)
            button {
                backgroundColor += Color.TRANSPARENT
                backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                backgroundPosition += BackgroundPosition.CENTER
                backgroundImage += this::class.java.getResource("/images/common/plus.png").toURI()
            }
        }

        newPlaylistBox {
            +newPlaylistPane
            borderWidth += box(0.8.px, 0.px, 0.px, 0.px)
            prefHeight = 65.px
            backgroundColor += Colors.lightGray
            label {
                labelFont = Fonts.playlistTitle
            }
            val butSize = 32.0
            button {
                prefWidth = butSize.px
                prefHeight = butSize.px
                maxHeight = butSize.px
                backgroundSize += BackgroundSize(butSize, butSize,
                        false, false, false, false)
            }
        }

        emptyPlaylistsPane {
            prefHeight = MainStyle.prefWinHeight.px
            prefWidth = MainStyle.prefWinWidth.px
            alignment = Pos.TOP_CENTER
            padding = box(30.px, 0.px, 0.px, 0.px)
            backgroundColor += Colors.lightGray
            emptyMessage {
                labelFont = Fonts.noPlaylistsContent
            }
        }
    }
}
