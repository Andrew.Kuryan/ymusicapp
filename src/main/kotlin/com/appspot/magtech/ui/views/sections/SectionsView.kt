package com.appspot.magtech.ui.views.sections

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.section.SectionAction
import com.appspot.magtech.application.section.availableSections
import com.appspot.magtech.application.tracks.TracksAction
import com.appspot.magtech.entities.toMinutes
import com.appspot.magtech.extensions.flow.FlowView
import com.appspot.magtech.extensions.flow.fxadapters.selectedIndices
import com.appspot.magtech.extensions.flow.fxadapters.userActions
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.extensions.jfxButton
import com.appspot.magtech.ui.components.flattabpane.flatTabPane
import com.appspot.magtech.ui.components.flattabpane.tab
import com.appspot.magtech.ui.components.placeholderPane
import com.appspot.magtech.ui.values.Colors
import com.jfoenix.controls.JFXButton
import javafx.collections.FXCollections
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import tornadofx.*

class SectionsView(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

    init {
        importStylesheet<SectionsStyle>()
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = stackpane {
        addClass(SectionsStyle.playlistsRoot)
        flatTabPane {
            availableSections.forEach { section ->
                tab(PlaylistsView(section, states, actions)) {
                    text = section
                    graphicProperty bindFlow states
                        .mapDistinctBy { it.sectionsState.selectedSectionTitle }
                        .map {
                            when (it) {
                                section -> JFXButton().apply {
                                    addClass(SectionsStyle.refreshButton)
                                    userActions.map { SectionAction.SelectSection(section, true) }
                                        .sendTo(actions)
                                }
                                else -> null
                            }
                        }
                    states.mapDistinctBy { it.sectionsState.selectedSectionTitle }
                        .watch { toggleClass(SectionsStyle.tabInactive, it != section) }
                    userActions.map { SectionAction.SelectSection(section, false) }
                        .sendTo(actions)
                }
            }
            selectedTabTitleProperty bindFlow states
                .mapDistinctBy { it.sectionsState.selectedSectionTitle }
        }
    }
}

class PlaylistsView(sectionTitle: String, states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

    class ContentView(sectionTitle: String, states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

        private val playlistItems = FXCollections.observableArrayList<View>()

        @FlowPreview
        @ExperimentalCoroutinesApi
        override val root = anchorpane {
            listview<View>(playlistItems) {
                addClass(SectionsStyle.playlist)
                anchorpaneConstraints {
                    topAnchor = 0.0
                    leftAnchor = 0.0
                    rightAnchor = 0.0
                    bottomAnchor = 0.0
                }
                cellCache { it.root }
                selectedIndices.filter { it >= 0 }
                    .map { index ->
                        index to states.map { it.sectionsState }
                            .map { it.playlists.getValue(sectionTitle) }
                            .first()
                    }
                    .map { (index, playlists) ->
                        TracksAction.SelectPlaylist(playlists[index].title, false)
                    }
                    .sendTo(actions)

                states.distinctUntilChangedBy { it.tracksState.selectedPlaylistTitle }
                    .filterNot {
                        it.sectionsState.playlists.getValue(sectionTitle)
                            .map { playlist -> playlist.title }
                            .contains(it.tracksState.selectedPlaylistTitle)
                    }
                    .watch { selectionModel.clearSelection() }

                states.mapDistinctBy { it.sectionsState.playlists.getValue(sectionTitle).size }
                    .watch { newSize ->
                        if (newSize < playlistItems.size) {
                            playlistItems.subList(newSize, playlistItems.size)
                                .forEach { it.onUndock() }
                            playlistItems.remove(newSize, playlistItems.size)
                        } else {
                            playlistItems.addAll((playlistItems.size until newSize)
                                .map { index -> PlaylistItem(sectionTitle, index, states, actions) })
                        }
                    }
            }
        }
    }

    class PlaceholderView : View() {

        override val root = flowpane {
            addClass(SectionsStyle.emptyPlaylistsPane)
            label("No available playlists") {
                addClass(SectionsStyle.emptyMessage)
            }
        }
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = placeholderPane(
        ContentView(sectionTitle, states, actions).root,
        PlaceholderView().root
    ) {
        isActiveProperty bindFlow states
            .mapDistinctBy { it.sectionsState.playlists.getValue(sectionTitle).size }
            .map { it > 0 }
    }
}

class PlaylistItem(sectionTitle: String, index: Int, states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = anchorpane {
        hbox {
            addClass(SectionsStyle.titleBox)
            addClass(SectionsStyle.inactiveItem)
            anchorpaneConstraints {
                leftAnchor = 3.0
                topAnchor = 0.0
                bottomAnchor = 0.0
            }
            jfxButton {
                addClass(SectionsStyle.refreshButton)
                visibleProperty() bindFlow states
                    .distinctUntilChangedBy { it.tracksState.selectedPlaylistTitle }
                    .map {
                        it.tracksState.selectedPlaylistTitle ==
                                it.sectionsState.playlists.getValue(sectionTitle)[index].title
                    }

                userActions.map {
                    states.map { it.sectionsState }
                        .map { it.playlists.getValue(sectionTitle) }
                        .first()
                }.map { playlists ->
                    TracksAction.SelectPlaylist(playlists[index].title, true)
                }.sendTo(actions)
            }
            label {
                addClass(SectionsStyle.labPlaylists)
                textProperty() bindFlow states
                    .map { it.sectionsState.playlists.getValue(sectionTitle)[index] }
                    .mapDistinctBy { it.title }
            }
        }
        hbox {
            addClass(SectionsStyle.infoBox)
            anchorpaneConstraints {
                rightAnchor = 35.0
                topAnchor = 0.0
                bottomAnchor = 0.0
            }
            label {
                addClass(SectionsStyle.labCount)
                textProperty() bindFlow states
                    .map { it.sectionsState.playlists.getValue(sectionTitle)[index] }
                    .mapDistinctBy { it.info.trackCount.toString() }
            }
            label("/") {
                textFill = Colors.white
            }
            label {
                addClass(SectionsStyle.labDuration)
                textProperty() bindFlow states
                    .map { it.sectionsState.playlists.getValue(sectionTitle)[index] }
                    .mapDistinctBy { it.info.duration.toMinutes() }
            }
        }
    }
}
