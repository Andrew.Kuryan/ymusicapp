package com.appspot.magtech.ui.views.sessions

import com.appspot.magtech.ui.values.Colors
import com.appspot.magtech.ui.values.Fonts
import com.appspot.magtech.ui.values.labelFont
import com.appspot.magtech.ui.views.login.LoginStyle
import com.appspot.magtech.ui.views.main.MainStyle
import javafx.geometry.Pos
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.paint.Color
import tornadofx.*

class SessionsStyle : Stylesheet() {

    companion object {
        val sessionsRoot by cssclass()

        val logoHeader by cssclass()
        val logoWrapper by cssclass()
        val logoTitle by cssclass()

        val usersList by cssclass()
        val userItem by cssclass()
        val userItemRightBox by cssclass()
        val butRemove by cssclass()

        val goToLoginButton by cssclass()

        val jfxRippler by cssclass("jfx-rippler")
        val jfxRipplerFill by cssproperty<Color>("-jfx-rippler-fill")
    }

    init {
        sessionsRoot {
            prefWidth = MainStyle.prefWinWidth.px
            prefHeight = MainStyle.prefWinHeight.px
            alignment = Pos.CENTER

            logoHeader {
                alignment = Pos.TOP_CENTER
                prefWidth = LoginStyle.contentCardWidth.px
                spacing = 10.px
                logoWrapper {
                    prefWidth = 80.px
                    prefHeight = 80.px
                    backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                    backgroundPosition += BackgroundPosition.CENTER
                    backgroundSize += BackgroundSize(
                        80.0, 80.0,
                        false, false, false, false
                    )
                    backgroundImage += this::class.java.getResource("/images/common/icon.png").toURI()
                }
                logoTitle {
                    labelFont = Fonts.loginLogoTitle
                    alignment = Pos.CENTER
                }
            }

            usersList {
                prefHeight = 213.px

                listCell {
                    prefHeight = 50.px
                    padding = box(0.px)
                    borderWidth += box(0.px)
                    and(odd) {
                        backgroundColor += Colors.white
                    }
                    and(even) {
                        backgroundColor += Colors.white
                    }
                    and(selected) {
                        backgroundColor += Colors.white
                    }
                    and(pressed) {
                        backgroundColor += Colors.lightYellow
                    }
                }
            }

            userItem {
                alignment = Pos.CENTER_LEFT
                spacing = 10.px
                padding = box(0.px, 8.px, 0.px, 8.px)

                label {
                    labelFont = Fonts.userItemLogin
                }

                userItemRightBox {
                    val butSize = 20
                    prefHeight = butSize.px
                    maxHeight = butSize.px

                    butRemove {
                        prefWidth = butSize.px
                        prefHeight = butSize.px
                        backgroundColor += Color.TRANSPARENT
                        backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                        backgroundPosition += BackgroundPosition.CENTER
                        backgroundSize += BackgroundSize(
                            butSize.toDouble(), butSize.toDouble(),
                            false, false, false, false
                        )
                        backgroundImage += this::class.java.getResource("/images/standard/delete.png").toURI()
                    }
                }
            }

            goToLoginButton {
                prefWidth = LoginStyle.contentCardWidth.px
                prefHeight = 40.px
                borderWidth += box(1.px)
                borderColor += box(Colors.lightGray)
                borderRadius += box(10.px)
                labelFont = Fonts.loginSubmitButton
            }
        }
    }
}