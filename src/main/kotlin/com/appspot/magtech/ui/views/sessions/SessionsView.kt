package com.appspot.magtech.ui.views.sessions

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.account.AccountAction
import com.appspot.magtech.application.navigation.NavigationAction
import com.appspot.magtech.application.navigation.NavigationRoute
import com.appspot.magtech.entities.User
import com.appspot.magtech.extensions.flow.FlowView
import com.appspot.magtech.extensions.flow.fxadapters.clicks
import com.appspot.magtech.extensions.flow.fxadapters.userActions
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.extensions.jfxButton
import com.appspot.magtech.ui.views.account.UserAvaView
import com.appspot.magtech.ui.views.login.LoginStyle
import com.appspot.magtech.ui.views.main.MainStyle
import javafx.collections.FXCollections
import javafx.scene.layout.Priority
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import tornadofx.*

class SessionsView(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

    init {
        importStylesheet<LoginStyle>()
        importStylesheet<SessionsStyle>()
    }

    private val userItems = FXCollections.observableArrayList<View>()

    @FlowPreview
    @ExperimentalCoroutinesApi
    override val root = anchorpane {
        anchorpaneConstraints {
            topAnchor = 0.0
            leftAnchor = 0.0
            bottomAnchor = 0.0
            rightAnchor = 0.0
        }

        flowpane {
            addClass(SessionsStyle.sessionsRoot)
            vbox {
                addClass(LoginStyle.contentCard)
                vbox {
                    addClass(SessionsStyle.logoHeader)
                    pane { addClass(SessionsStyle.logoWrapper) }
                    label("Choose account to log in") { addClass(SessionsStyle.logoTitle) }
                }
                listview<View>(userItems) {
                    addClass(SessionsStyle.usersList)
                    cellCache { it.root }
                    states.mapDistinctBy { it.accountState.savedUsers }
                        .watch { users ->
                            userItems.forEach { it.onUndock() }
                            userItems.clear()
                            userItems.addAll(users.mapIndexed { index, _ -> UserItemView(states, actions, index) })
                        }
                }
                jfxButton("Log in to another account") {
                    addClass(SessionsStyle.goToLoginButton)
                    userActions.map { NavigationAction.NavigateTo(NavigationRoute.LOGIN) }
                        .sendTo(actions)
                }
            }
        }

        jfxButton {
            addClass(LoginStyle.homeButton)
            layoutX = MainStyle.prefWinWidth / 2.0 - LoginStyle.contentCardWidth / 2.0
            layoutY = MainStyle.prefWinHeight / 2.0 - LoginStyle.contentCardHeight / 2.0
            userActions.map { NavigationAction.NavigateTo(NavigationRoute.HOME) }
                .sendTo(actions)
        }
    }
}

@FlowPreview
fun Flow<AppState>.mapNonNullUserElement(index: Int): Flow<User> =
    map { it.accountState }
        .mapDistinctBy { it.savedUsers.getOrNull(index) }
        .filterNotNull()

class UserItemView(states: Flow<AppState>, actions: Channel<Action>, index: Int) : FlowView() {

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = hbox {
        addClass(SessionsStyle.userItem)
        clicks.map { states.mapNonNullUserElement(index).first() }
            .mapNotNull { user -> AccountAction.AuthSuccess(user) }
            .sendTo(actions)

        add(UserAvaView(states, { accountState.savedUsers.getOrNull(index) }).root)

        label {
            textProperty() bindFlow states.mapNonNullUserElement(index)
                .mapDistinctBy { it.login }
        }

        anchorpane {
            addClass(SessionsStyle.userItemRightBox)
            hgrow = Priority.ALWAYS
            jfxButton {
                addClass(SessionsStyle.butRemove)
                anchorpaneConstraints {
                    rightAnchor = 0.0
                }

                userActions.map { states.mapNonNullUserElement(index).first() }
                    .map { AccountAction.RemoveUser(it) }
                    .sendTo(actions)
            }
        }
    }
}