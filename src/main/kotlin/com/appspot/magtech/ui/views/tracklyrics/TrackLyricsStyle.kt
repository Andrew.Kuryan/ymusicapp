package com.appspot.magtech.ui.views.tracklyrics

import com.appspot.magtech.ui.values.Colors
import com.appspot.magtech.ui.values.Fonts
import com.appspot.magtech.ui.values.labelFont
import javafx.geometry.Pos
import javafx.scene.Cursor
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.paint.Color
import javafx.scene.text.TextAlignment
import tornadofx.*

class TrackLyricsStyle : Stylesheet() {

    companion object {
        val trackLyricsRoot by cssclass()
        val trackLyricsHead by cssclass()
        val trackLyricsContent by cssclass()

        val lyricsTitle by cssclass()

        val trackLyricsMainInfo by cssclass()
        val lyricsTextTitle by cssclass()
        val lyricsText by cssclass()
        val lyricsExpandLabel by cssclass()

        val emptyLyricsPane by cssclass()
        val emptyMessage by cssclass()

        val emptyAnalysisPane by cssclass()
        val downloadButton by cssclass()
        val removeButton by cssclass()
        val arc by cssclass()

        val analysisPane by cssclass()
        val linkLabel by cssclass()
        val notesGrid by cssclass()
        val noteInfoWrapper by cssclass()
        val x1Note by cssclass()
        val x2Note by cssclass()
        val x4Note by cssclass()
        val x8Note by cssclass()

        val jfxRadius by cssproperty<Number>("-jfx-radius")
    }

    init {
        trackLyricsRoot {
            viewport {
                backgroundColor += Colors.gridpear
            }
            trackLyricsContent {
                padding = box(15.px)
                spacing = 20.px
                trackLyricsHead {
                    alignment = Pos.TOP_CENTER
                    lyricsTitle {
                        labelFont = Fonts.lyricsTitle
                        textAlignment = TextAlignment.CENTER
                        alignment = Pos.CENTER
                        wrapText = true
                    }
                }
                trackLyricsMainInfo {
                    spacing = 10.px
                    lyricsTextTitle {
                        labelFont = Fonts.lyricsTextTitle
                    }
                    lyricsText {
                        labelFont = Fonts.lyricsText
                        wrapText = true
                    }
                    lyricsExpandLabel {
                        labelFont = Fonts.lyricsExpandLabel
                        cursor = Cursor.HAND
                    }
                }
                emptyLyricsPane {
                    alignment = Pos.TOP_CENTER
                    backgroundColor += Colors.gridpear
                    emptyMessage {
                        labelFont = Fonts.lyricsTextTitle
                    }
                }

                emptyAnalysisPane {
                    alignment = Pos.TOP_CENTER
                    backgroundColor += Colors.gridpear
                    spacing = 8.px
                    label {
                        labelFont = Fonts.downloadInfoText
                    }
                    button {
                        val butSize = 36
                        prefWidth = butSize.px
                        prefHeight = butSize.px
                        backgroundColor += Color.TRANSPARENT
                        backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                        backgroundPosition += BackgroundPosition.CENTER
                        backgroundSize += BackgroundSize(
                            butSize.toDouble(), butSize.toDouble(),
                            false, false, false, false
                        )
                    }
                    downloadButton {
                        backgroundImage += this::class.java.getResource("/images/standard/download.png").toURI()
                    }
                    removeButton {
                        backgroundImage += this::class.java.getResource("/images/standard/delete.png").toURI()
                    }
                    progressIndicator {
                        prefWidth = 60.px
                        prefHeight = 60.px
                        jfxRadius.set(30.0)
                        arc {
                            stroke = Colors.lightYellow
                        }
                        percentage {
                            labelFont = Fonts.downloadInfoText
                            fill = Fonts.downloadInfoText.color
                            textAlignment = TextAlignment.CENTER
                            alignment = Pos.CENTER
                        }
                    }
                }

                analysisPane {
                    alignment = Pos.TOP_CENTER
                    backgroundColor += Colors.gridpear
                    spacing = 5.px
                    label {
                        labelFont = Fonts.lyricsTextTitle
                    }
                    linkLabel {
                        and(hover) {
                            cursor = Cursor.HAND
                            underline = true
                        }
                    }
                    notesGrid {
                        padding = box(0.px, 55.px, 0.px, 0.px)
                        val imageSize = 42
                        val noteImage = mixin {
                            prefWidth = imageSize.px
                            prefHeight = imageSize.px
                            backgroundColor += Color.TRANSPARENT
                            backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                            backgroundPosition += BackgroundPosition.CENTER
                            backgroundSize += BackgroundSize(
                                imageSize.toDouble(), imageSize.toDouble(),
                                false, false, false, false
                            )
                        }
                        noteInfoWrapper {
                            alignment = Pos.BOTTOM_RIGHT
                            label {
                                maxHeight = 25.px
                                minHeight = 0.px
                            }
                        }
                        x1Note {
                            +noteImage
                            backgroundImage += this::class.java.getResource("/images/common/x1.png").toURI()
                        }
                        x2Note {
                            +noteImage
                            backgroundImage += this::class.java.getResource("/images/common/x2.png").toURI()
                        }
                        x4Note {
                            +noteImage
                            backgroundImage += this::class.java.getResource("/images/common/x4.png").toURI()
                        }
                        x8Note {
                            +noteImage
                            backgroundImage += this::class.java.getResource("/images/common/x8.png").toURI()
                        }
                    }
                }
            }
        }
    }
}
