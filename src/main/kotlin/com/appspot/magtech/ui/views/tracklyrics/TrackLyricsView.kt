package com.appspot.magtech.ui.views.tracklyrics

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.tracklyrics.TrackLyricsAction
import com.appspot.magtech.application.tracks.SearchAction
import com.appspot.magtech.application.tracks.TracksAction
import com.appspot.magtech.application.tracks.getTrackById
import com.appspot.magtech.entities.*
import com.appspot.magtech.extensions.flow.FlowView
import com.appspot.magtech.extensions.flow.fxadapters.clicks
import com.appspot.magtech.extensions.flow.fxadapters.userActions
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.extensions.jfxButton
import com.appspot.magtech.extensions.jfxSpinner
import com.appspot.magtech.ui.components.drawer.materialDrawer
import com.appspot.magtech.ui.components.fetchingview.fetchingWrapper
import com.appspot.magtech.ui.components.placeholderPane
import javafx.scene.control.ScrollPane
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import tornadofx.*

@FlowPreview
fun Flow<AppState>.mapFulfilledAnalysis() =
    mapDistinctBy { it.tracksState.getTrackById(it.playerState.selectedTrackId)?.track }
        .filterIsInstance<LocalTrack>()
        .map { it.analysis }
        .filterIsInstance<FulfilledAnalysis>()

class TrackLyricsContent(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

    init {
        importStylesheet<TrackLyricsStyle>()
    }

    private val lyricsText = LyricsText(states, actions)
    private val placeholderView = LyricsPlaceholderView()

    private val analysisPlaceholderView = AnalysisPlaceholderView(states, actions)
    private val analysisInfo = AnalysisInfo(states, actions)
    private val analysisProcessing = AnalysisProcessingView(states)

    class LyricsText(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

        @ExperimentalCoroutinesApi
        @FlowPreview
        override val root = vbox {
            addClass(TrackLyricsStyle.trackLyricsMainInfo)
            label("Song lyrics:") {
                addClass(TrackLyricsStyle.lyricsTextTitle)
            }
            label {
                addClass(TrackLyricsStyle.lyricsText)
                textProperty() bindFlow states
                    .mapDistinctBy { it.trackLyricsState }
                    .map {
                        when (it.isExpanded) {
                            true -> it.trackDetailedInfo?.lyric?.firstOrNull()?.fullLyrics ?: ""
                            false -> it.trackDetailedInfo?.lyric?.firstOrNull()?.lyrics ?: ""
                        }
                    }
            }
            anchorpane {
                label {
                    textProperty() bindFlow states
                        .mapDistinctBy { it.trackLyricsState.isExpanded }
                        .map {
                            when (it) {
                                true -> "Roll Up"
                                false -> "Expand"
                            }
                        }
                    anchorpaneConstraints {
                        topAnchor = 10.0
                        rightAnchor = 0.0
                        bottomAnchor = 0.0
                    }
                    addClass(TrackLyricsStyle.lyricsExpandLabel)
                    clicks.map {
                        states.map { it.trackLyricsState.isExpanded }
                            .first()
                    }.map { isExpanded ->
                        when (isExpanded) {
                            true -> TrackLyricsAction.RollUp
                            false -> TrackLyricsAction.Expand
                        }
                    }.sendTo(actions)
                }
            }
        }
    }

    class LyricsPlaceholderView : View() {

        override val root = flowpane {
            addClass(TrackLyricsStyle.emptyLyricsPane)
            label("No available lyrics") {
                addClass(TrackLyricsStyle.emptyMessage)
            }
        }
    }

    class AnalysisInfo(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

        @ExperimentalCoroutinesApi
        @FlowPreview
        override val root = vbox {
            addClass(TrackLyricsStyle.analysisPane)
            label("Composition characteristics") {
                addClass(TrackLyricsStyle.lyricsTextTitle)
            }
            vbox {
                label("Tempo: ") {
                    addClass(TrackLyricsStyle.linkLabel)
                    textProperty() bindFlow states.mapFulfilledAnalysis()
                        .map { "Tempo: ${getTempoFromBPM(it.bpm).name.toLowerCase()}" }

                    clicks.map {
                        states.mapFulfilledAnalysis()
                            .map { getTempoFromBPM(it.bpm) }
                            .first()
                    }.map { SearchAction.SelectTempo(it) }
                        .sendTo(actions)
                }
                label {
                    textProperty() bindFlow states.mapFulfilledAnalysis()
                        .map { "BPM = ${it.bpm}" }
                }
                label {
                    textProperty() bindFlow states.mapFulfilledAnalysis()
                        .map { "Key: ${it.tonality}" }
                }
            }
            gridpane {
                addClass(TrackLyricsStyle.notesGrid)
                row {
                    hbox {
                        gridpaneColumnConstraints { percentWidth = 50.0 }
                        addClass(TrackLyricsStyle.noteInfoWrapper)
                        label {
                            textProperty() bindFlow states.mapFulfilledAnalysis()
                                .map { String.format("%.0f%%", it.notePercents.getValue(8) * 100) }
                        }
                        pane { addClass(TrackLyricsStyle.x8Note) }
                    }
                    hbox {
                        gridpaneColumnConstraints { percentWidth = 50.0 }
                        addClass(TrackLyricsStyle.noteInfoWrapper)
                        label {
                            textProperty() bindFlow states.mapFulfilledAnalysis()
                                .map { String.format("%.0f%%", it.notePercents.getValue(2) * 100) }
                        }
                        pane { addClass(TrackLyricsStyle.x2Note) }
                    }
                }
                row {
                    hbox {
                        gridpaneColumnConstraints { percentWidth = 50.0 }
                        addClass(TrackLyricsStyle.noteInfoWrapper)
                        label {
                            textProperty() bindFlow states.mapFulfilledAnalysis()
                                .map { String.format("%.0f%%", it.notePercents.getValue(4) * 100) }
                        }
                        pane { addClass(TrackLyricsStyle.x4Note) }
                    }
                    hbox {
                        gridpaneColumnConstraints { percentWidth = 50.0 }
                        addClass(TrackLyricsStyle.noteInfoWrapper)
                        label {
                            textProperty() bindFlow states.mapFulfilledAnalysis()
                                .map { String.format("%.0f%%", it.notePercents.getValue(1) * 100) }
                        }
                        pane { addClass(TrackLyricsStyle.x1Note) }
                    }
                }
            }
        }
    }

    class AnalysisProcessingView(states: Flow<AppState>) : FlowView() {

        @FlowPreview
        override val root = vbox {
            addClass(TrackLyricsStyle.emptyAnalysisPane)
            label("Processing analysis...")
            jfxSpinner() {
                progressProperty() bindFlow states
                    .mapDistinctBy { it.tracksState.getTrackById(it.playerState.selectedTrackId)?.track }
                    .filterIsInstance<LocalTrack>()
                    .map { it.analysis }
                    .filterIsInstance<ProcessingAnalysis>()
                    .map { it.percent }
            }
        }
    }

    class AnalysisPlaceholderView(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

        @FlowPreview
        @ExperimentalCoroutinesApi
        override val root = vbox {
            addClass(TrackLyricsStyle.emptyAnalysisPane)
            label("Download track to get the analysis") {
                textProperty() bindFlow states.mapNotNull { it.tracksState.getTrackById(it.playerState.selectedTrackId) }
                    .mapDistinctBy { it.track }
                    .map {
                        when (it) {
                            is LocalTrack -> "Remove track"
                            is YMusicTrack -> "Download track to get the analysis"
                        }
                    }
            }
            jfxButton {
                states.mapNotNull { it.tracksState.getTrackById(it.playerState.selectedTrackId) }
                    .mapDistinctBy { it.track }
                    .watch {
                        toggleClass(TrackLyricsStyle.downloadButton, it is YMusicTrack)
                        toggleClass(TrackLyricsStyle.removeButton, it is LocalTrack)
                    }

                userActions.map {
                    states.map { it.tracksState.getTrackById(it.playerState.selectedTrackId) }
                        .first()?.track
                }.filterNotNull().map {
                    when (it) {
                        is YMusicTrack -> TracksAction.DownloadTrack(it)
                        is LocalTrack -> TracksAction.RemoveTrack(it)
                    }
                }.sendTo(actions)
            }
        }
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = scrollpane {
        addClass(TrackLyricsStyle.trackLyricsRoot)
        lyricsText.root.maxWidthProperty().bind(this@scrollpane.widthProperty().subtract(38.0))
        lyricsText.root.prefWidthProperty().bind(this@scrollpane.widthProperty().subtract(38.0))
        placeholderView.root.maxWidthProperty().bind(this@scrollpane.widthProperty().subtract(38.0))
        placeholderView.root.prefWidthProperty().bind(this@scrollpane.widthProperty().subtract(38.0))

        hbarPolicy = ScrollPane.ScrollBarPolicy.NEVER

        vbox {
            addClass(TrackLyricsStyle.trackLyricsContent)
            vbox {
                addClass(TrackLyricsStyle.trackLyricsHead)
                maxWidthProperty().bind(this@scrollpane.widthProperty().subtract(38.0))
                prefWidthProperty().bind(this@scrollpane.widthProperty().subtract(38.0))
                label {
                    addClass(TrackLyricsStyle.lyricsTitle)
                    textProperty() bindFlow states
                        .mapDistinctBy { it.tracksState.getTrackById(it.playerState.selectedTrackId) }
                        .map { it?.track?.info }
                        .map {
                            if (it != null) {
                                "${it.title} - ${it.artists.joinToString(", ") { artist -> artist.name }}"
                            } else ""
                        }
                }
            }
            vbox {
                states.mapDistinctBy { it.tracksState.getTrackById(it.playerState.selectedTrackId)?.track }
                    .watch { track ->
                        this.children.clear()
                        when (track) {
                            is LocalTrack -> when (track.analysis) {
                                is NotAvailableAnalysis -> this.children.add(analysisPlaceholderView.root)
                                is FulfilledAnalysis -> this.children.add(analysisInfo.root)
                                is ProcessingAnalysis -> this.children.add(analysisProcessing.root)
                            }
                            else -> this.children.add(analysisPlaceholderView.root)
                        }
                    }
            }
            placeholderPane(lyricsText.root, placeholderView.root) {
                isActiveProperty bindFlow states
                    .mapDistinctBy { it.trackLyricsState.trackDetailedInfo?.lyric?.firstOrNull()?.lyrics }
                    .map { it != null }
            }
        }
    }
}

class TrackLyricsWrapper(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

    private val trackLyricsContent = TrackLyricsContent(states, actions)

    override val root = fetchingWrapper(trackLyricsContent) {
        isFetchingProperty bindFlow states
            .map { it.trackLyricsState.isFetching }
        widthProperty()
    }
}

class TrackLyricsView(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = materialDrawer(0.8) {
        isOpenedProperty bindFlow states
            .mapDistinctBy { it.trackLyricsState.isOpened }

        closedActions.map { TrackLyricsAction.Close }
            .sendTo(actions)

        sideContent = TrackLyricsWrapper(states, actions).root
    }
}
