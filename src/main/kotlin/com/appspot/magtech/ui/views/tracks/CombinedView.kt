package com.appspot.magtech.ui.views.tracks

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.tracks.SearchAction
import com.appspot.magtech.entities.Tempo
import com.appspot.magtech.extensions.flow.FlowView
import com.appspot.magtech.extensions.flow.fxadapters.clicks
import com.appspot.magtech.extensions.flow.fxadapters.texts
import com.appspot.magtech.extensions.flow.fxadapters.userActions
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.extensions.jfxButton
import com.appspot.magtech.extensions.jfxCheckbox
import com.appspot.magtech.extensions.jfxTextField
import com.appspot.magtech.ui.components.placeholderPane
import com.appspot.magtech.ui.views.search.SearchView
import com.jfoenix.controls.JFXPopup
import javafx.scene.control.TextField
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import tornadofx.*

class CombinedView(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

    init {
        importStylesheet<TracksStyle>()
    }

    lateinit var searchField: TextField

    @FlowPreview
    @ExperimentalCoroutinesApi
    private val filterPopup = JFXPopup().apply {
        popupContent = VBox().apply {
            addClass(TracksStyle.filterPopupRoot)
            label("Tempo:") {
                addClass(TracksStyle.filterParagraph)
            }
            vbox {
                Tempo.available.forEach { tempo ->
                    hbox {
                        addClass(TracksStyle.tempoItem)
                        anchorpane {
                            hgrow = Priority.ALWAYS
                            anchorpaneConstraints {
                                topAnchor = 0.0
                                leftAnchor = 0.0
                                rightAnchor = 0.0
                                bottomAnchor = 0.0
                            }
                            label(tempo.name.toLowerCase())
                        }
                        jfxCheckbox() {
                            clicks.map {
                                when (this.isSelected) {
                                    true -> SearchAction.SetTempo(tempo)
                                    false -> SearchAction.ResetTempo(tempo)
                                }
                            }.sendTo(actions)

                            states.mapDistinctBy { it.searchState.tempos }
                                .watch {
                                    isSelected = it.contains(tempo)
                                }
                        }
                    }
                }
            }
            hbox {
                anchorpane {
                    hgrow = Priority.ALWAYS
                    jfxButton("Reset") {
                        anchorpaneConstraints {
                            topAnchor = 0.0
                            leftAnchor = 0.0
                            rightAnchor = 0.0
                            bottomAnchor = 0.0
                        }
                        userActions.map { SearchAction.ResetTempos }
                            .sendTo(actions)
                    }
                }
                anchorpane {
                    hgrow = Priority.ALWAYS
                    jfxButton("Apply") {
                        anchorpaneConstraints {
                            topAnchor = 0.0
                            leftAnchor = 0.0
                            rightAnchor = 0.0
                            bottomAnchor = 0.0
                        }
                        userActions.map { SearchAction.ApplyTempos }
                            .sendTo(actions)
                    }
                }
            }
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    override val root = vbox {
        addClass(TracksStyle.combinedRoot)
        hbox {
            prefWidthProperty().bind(this@vbox.widthProperty())
            addClass(TracksStyle.filterBox)
            anchorpane {
                hgrow = Priority.ALWAYS
                searchField = jfxTextField {
                    anchorpaneConstraints {
                        topAnchor = 0.0
                        leftAnchor = 0.0
                        bottomAnchor = 0.0
                        rightAnchor = 0.0
                    }
                    texts.map { SearchAction.SetQuery(it) }
                        .sendTo(actions)
                }
            }
            jfxButton {
                addClass(TracksStyle.searchButton)
                action {
                    searchField.requestFocus()
                }
            }
            jfxButton {
                addClass(TracksStyle.filterButton)
                action {
                    if (filterPopup.isShowing) {
                        filterPopup.hide()
                    } else {
                        filterPopup.show(
                            this, JFXPopup.PopupVPosition.TOP, JFXPopup.PopupHPosition.RIGHT,
                            0.0, TracksStyle.filterBoxHeight.toDouble(),
                        )
                    }
                }
            }
        }
        placeholderPane(
            SearchView(states, actions).root,
            TracksView(states, actions).root,
        ) {
            vgrow = Priority.ALWAYS
            isActiveProperty bindFlow states.mapDistinctBy { it.searchState }
                .map {
                    (it.query.isNotEmpty() && it.query.isNotBlank()) ||
                            it.appliedTempos.size != Tempo.available.size
                }
        }
    }
}