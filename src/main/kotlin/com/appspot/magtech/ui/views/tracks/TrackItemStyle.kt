package com.appspot.magtech.ui.views.tracks

import com.appspot.magtech.ui.values.Fonts
import com.appspot.magtech.ui.values.labelFont
import javafx.geometry.Pos
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.paint.Color
import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.px

class TrackItemStyle : Stylesheet() {

    companion object {
        val trackItemRoot by cssclass()
        val itemTitle by cssclass()
        val itemTitleDisabled by cssclass()

        val rightBox by cssclass()
        val butDownload by cssclass()
        val butRemove by cssclass()
    }

    init {
        trackItemRoot {

            itemTitle {
                labelFont = Fonts.trackTitle
            }
            itemTitleDisabled {
                labelFont = Fonts.trackTitleDisabled
            }

            rightBox {
                val butSize = 20
                alignment = Pos.CENTER_RIGHT
                spacing = 20.px

                button {
                    prefWidth = butSize.px
                    prefHeight = butSize.px
                    backgroundColor += Color.TRANSPARENT
                    backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                    backgroundPosition += BackgroundPosition.CENTER
                    backgroundSize += BackgroundSize(butSize.toDouble(), butSize.toDouble(),
                            false, false, false, false)
                }
                butDownload {
                    backgroundImage += this::class.java.getResource("/images/standard/download.png").toURI()
                }
                butRemove {
                    backgroundImage += this::class.java.getResource("/images/standard/delete.png").toURI()
                }
            }
        }
    }
}
