package com.appspot.magtech.ui.views.tracks

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.tracks.TrackElement
import com.appspot.magtech.application.tracks.TracksAction
import com.appspot.magtech.entities.*
import com.appspot.magtech.extensions.flow.FlowView
import com.appspot.magtech.extensions.flow.fxadapters.clicks
import com.appspot.magtech.extensions.flow.fxadapters.userActions
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.extensions.jfxButton
import com.appspot.magtech.ui.components.fetchingview.fetchingWrapper
import javafx.beans.property.DoubleProperty
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Priority
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import tornadofx.*

class TrackControlsView(
    states: Flow<AppState>,
    actions: Channel<Action>,
    index: Int,
    parentWidthProperty: DoubleProperty,
    mapTrackElement: Flow<AppState>.(Int) -> Flow<TrackElement>,
) : FlowView() {

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = hbox {
        addClass(TrackItemStyle.rightBox)
        maxWidthProperty().bind(parentWidthProperty.multiply(0.25))
        anchorpaneConstraints {
            rightAnchor = 10.0
            topAnchor = 0.0
            bottomAnchor = 0.0
        }
        label {
            addClass(TrackItemStyle.itemTitle)
            textProperty() bindFlow states.mapTrackElement(index)
                .mapDistinctBy { it.track.info.duration.toMinutes() }
        }

        jfxButton {
            states.mapTrackElement(index)
                .mapDistinctBy { it.track }
                .watch { track ->
                    toggleClass(TrackItemStyle.butDownload, track is YMusicTrack)
                    toggleClass(TrackItemStyle.butRemove, track is LocalTrack)
                }

            userActions.map { states.mapTrackElement(index).first() }
                .map {
                    when (it.track) {
                        is LocalTrack -> TracksAction.RemoveTrack(it.track)
                        is YMusicTrack -> TracksAction.DownloadTrack(it.track)
                    }
                }.sendTo(actions)
        }
    }
}

class TrackItemContent(
    states: Flow<AppState>,
    actions: Channel<Action>,
    index: Int,
    mapTrackElement: Flow<AppState>.(Int) -> Flow<TrackElement>,
    mapItemClicks: Flow<MouseEvent>.(Flow<AppState>, Int) -> Flow<Action>,
) : FlowView() {

    private lateinit var controlsView: TrackControlsView

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = hbox {
        addClass(TrackItemStyle.trackItemRoot)
        val rootWidthProperty = this.maxWidthProperty()

        clicks.mapItemClicks(states, index)
            .sendTo(actions)

        anchorpane {
            hgrow = Priority.ALWAYS
            label {
                anchorpaneConstraints {
                    leftAnchor = 10.0
                    topAnchor = 0.0
                    bottomAnchor = 0.0
                }
                maxWidthProperty().bind(rootWidthProperty.multiply(0.75))

                states.mapTrackElement(index)
                    .mapDistinctBy { it.track.isAvailable() }
                    .watch { available ->
                        toggleClass(TrackItemStyle.itemTitle, available)
                        toggleClass(TrackItemStyle.itemTitleDisabled, !available)
                    }

                textProperty() bindFlow states.mapTrackElement(index)
                    .mapDistinctBy { it.track.displayInfo() }
                    .map { info -> "${(index + 1)}.  $info" }
            }
        }

        controlsView = TrackControlsView(states, actions, index, rootWidthProperty, mapTrackElement)

        states.mapTrackElement(index)
            .mapDistinctBy { it.track.isAvailable() }
            .watch { available ->
                if (available) {
                    children.add(controlsView.root)
                } else {
                    children.remove(controlsView.root)
                }
            }
    }

    override fun onUndock() {
        super.onUndock()
        controlsView.onUndock()
    }
}

class TrackItemView(
    states: Flow<AppState>,
    actions: Channel<Action>,
    index: Int,
    mapTrackElement: Flow<AppState>.(Int) -> Flow<TrackElement>,
    mapItemClicks: Flow<MouseEvent>.(Flow<AppState>, Int) -> Flow<Action>,
) : FlowView() {

    init {
        importStylesheet<TrackItemStyle>()
    }

    val itemContent = TrackItemContent(states, actions, index, mapTrackElement, mapItemClicks)

    @FlowPreview
    override val root = fetchingWrapper(itemContent) {
        isFetchingProperty bindFlow states.mapTrackElement(index)
            .mapDistinctBy { it.isFetching }
    }

    override fun onUndock() {
        super.onUndock()
        itemContent.onUndock()
    }
}
