package com.appspot.magtech.ui.views.tracks

import com.appspot.magtech.ui.values.Colors
import com.appspot.magtech.ui.values.Fonts
import com.appspot.magtech.ui.values.labelFont
import com.appspot.magtech.ui.views.login.LoginStyle
import com.appspot.magtech.ui.views.main.MainStyle
import javafx.geometry.Pos
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.paint.Color
import tornadofx.*

class TracksStyle : Stylesheet() {

    companion object {
        val combinedRoot by cssclass()
        val tracksRoot by cssclass()
        val tracksList by cssclass()

        const val filterBoxHeight = 30
        val filterBox by cssclass()
        val searchButton by cssclass()
        val filterButton by cssclass()

        val filterPopupRoot by cssclass()
        val filterParagraph by cssclass()
        val tempoItem by cssclass()

        val playlistInfo by cssclass()

        val emptyTracksPane by cssclass()
        val emptyMessage by cssclass()
        val listParagraph by cssclass()
    }

    init {
        filterPopupRoot {
            prefWidth = 180.px
            padding = box(0.px, 0.px, 0.px, 0.px)

            filterParagraph {
                labelFont = Fonts.filterButton
                padding = box(2.px, 8.px, 2.px, 8.px)
            }

            tempoItem {
                alignment = Pos.CENTER_LEFT
                prefHeight = 24.px
                padding = box(4.px, 2.px, 4.px, 10.px)

                label {
                    labelFont = Fonts.tempoItem
                }
                checkBox {
                    LoginStyle.jfxCheckedColor.set(Colors.primeYellow)
                    box {
                        padding = box(0.166667.em, 0.166667.em, 0.25.em, 0.25.em)
                        prefWidth = 15.px
                        prefHeight = 15.px
                        mark {
                            padding = box(0.416667.em, 0.416667.em, 0.5.em, 0.5.em)
                        }
                    }
                }
            }

            button {
                prefHeight = 32.px
                labelFont = Fonts.filterButton
            }
        }

        combinedRoot {
            prefHeight = MainStyle.prefWinHeight.px
            prefWidth = MainStyle.prefWinWidth.px

            filterBox {
                prefHeight = filterBoxHeight.px
                alignment = Pos.CENTER_LEFT
                padding = box(0.px, 5.px, 0.px, 5.px)
                spacing = 5.px
                val butSize = 24
                button {
                    prefWidth = butSize.px
                    prefHeight = butSize.px
                    backgroundColor += Color.TRANSPARENT
                    backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                    backgroundPosition += BackgroundPosition.CENTER
                    backgroundSize += BackgroundSize(
                        butSize.toDouble(), butSize.toDouble(),
                        false, false, false, false
                    )
                }
                textField {
                    fontSize = 15.px
                }
                searchButton {
                    backgroundImage += this::class.java.getResource("/images/common/search.png").toURI()
                }
                filterButton {
                    backgroundImage += this::class.java.getResource("/images/common/filter_alt.png").toURI()
                }
            }
        }

        tracksRoot {
            backgroundColor += Colors.white
            borderWidth += box(0.px)
            spacing = 0.px

            listParagraph {
                labelFont = Fonts.trackListParagraph
                padding = box(0.px, 0.px, 0.px, 8.px)
            }

            tracksList {
                listCell {
                    prefHeight = 60.px
                    padding = box(0.px)
                    borderWidth += box(0.px)
                    and(odd) {
                        backgroundColor += Colors.extraLightGray
                    }
                    and(selected) {
                        backgroundColor += Colors.lightYellow
                    }
                }
            }

            playlistInfo {
                alignment = Pos.CENTER
                borderWidth += box(0.px)
                backgroundColor += Colors.white
                prefHeight = 30.px
                label {
                    labelFont = Fonts.playlistsInfo
                }
            }
        }

        emptyTracksPane {
            alignment = Pos.TOP_CENTER
            padding = box(30.px, 0.px, 0.px, 0.px)
            backgroundColor += Colors.extraLightGray
            emptyMessage {
                labelFont = Fonts.noTracksContent
            }
        }
    }
}
