package com.appspot.magtech.ui.views.tracks

import com.appspot.magtech.application.Action
import com.appspot.magtech.application.AppState
import com.appspot.magtech.application.player.PlayerAction
import com.appspot.magtech.application.section.getPlaylistByTitle
import com.appspot.magtech.application.tracklyrics.TrackLyricsAction
import com.appspot.magtech.application.tracks.TrackElement
import com.appspot.magtech.entities.isAvailable
import com.appspot.magtech.entities.toHours
import com.appspot.magtech.extensions.flow.FlowView
import com.appspot.magtech.extensions.flow.mapDistinctBy
import com.appspot.magtech.ui.components.placeholderPane
import javafx.collections.FXCollections
import javafx.scene.input.MouseEvent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import tornadofx.*

@FlowPreview
fun Flow<AppState>.mapNonNullTrackElement(index: Int): Flow<TrackElement> =
    map { it.tracksState }
        .mapDistinctBy { it.tracks[it.selectedPlaylistTitle]?.getOrNull(index) }
        .filterNotNull()

fun Flow<MouseEvent>.mapItemClicks(states: Flow<AppState>, index: Int): Flow<Action> =
    map {
        states.map { it.tracksState }
            .map { it.tracks[it.selectedPlaylistTitle] to it.selectedPlaylistTitle }
            .first() to states
            .map { it.playerState }
            .map { it.selectedTrackId }
            .first()
    }.mapNotNull { (tracksState, selectedTrackId) ->
        val (tracks, playlistTitle) = tracksState
        if (tracks != null && playlistTitle != null && tracks[index].track.isAvailable()) {
            if (selectedTrackId == tracks[index].track.id) {
                TrackLyricsAction.Open
            } else {
                PlayerAction.SelectTrack(tracks[index].track.id, playlistTitle)
            }
        } else null
    }

class TracksPlaceholderView(text: String = "No tracks") : View() {

    override val root = flowpane {
        addClass(TracksStyle.emptyTracksPane)
        label(text) {
            addClass(TracksStyle.emptyMessage)
        }
    }
}

class TrackItemsListView<T>(
    states: Flow<AppState>,
    actions: Channel<Action>,
    getTrackList: AppState.() -> List<T>?,
    getTrackId: T.() -> Int,
    mapTrackElement: Flow<AppState>.(Int) -> Flow<TrackElement>,
    mapItemClicks: Flow<MouseEvent>.(Flow<AppState>, Int) -> Flow<Action>,
) : FlowView() {

    private val trackItems = FXCollections.observableArrayList<View>()

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = listview<View>(trackItems) {
        addClass(TracksStyle.tracksList)
        cellCache { it.root }

        states.mapDistinctBy { it.playerState.selectedTrackId }
            .combine(states.mapDistinctBy { it.getTrackList() }
                .map { it?.map { trackEl -> trackEl.getTrackId() } }
            ) { trackId, tracks -> tracks?.indexOf(trackId) }
            .watch { selectionModel.select(it ?: -1) }

        states.mapDistinctBy { it.getTrackList()?.size }
            .filterNotNull()
            .watch { newSize ->
                if (newSize < trackItems.size) {
                    trackItems.subList(newSize, trackItems.size)
                        .forEach { it.onUndock() }
                    trackItems.remove(newSize, trackItems.size)
                } else {
                    trackItems.addAll((trackItems.size until newSize)
                        .map { index ->
                            TrackItemView(
                                states, actions,
                                index,
                                mapTrackElement, mapItemClicks
                            ).apply {
                                itemContent.root.maxWidthProperty().bind(
                                    this@listview.widthProperty().subtract(15.0)
                                )
                            }
                        })
                }
            }
    }
}

class TracksView(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

    private class ContentView(states: Flow<AppState>, actions: Channel<Action>) : FlowView() {

        @ExperimentalCoroutinesApi
        @FlowPreview
        override val root = anchorpane {
            addClass(TracksStyle.tracksRoot)
            add(
                TrackItemsListView(
                    states, actions,
                    { this.tracksState.tracks[this.tracksState.selectedPlaylistTitle] },
                    { this.track.id },
                    Flow<AppState>::mapNonNullTrackElement,
                    Flow<MouseEvent>::mapItemClicks
                ).root.apply {
                    anchorpaneConstraints {
                        topAnchor = 0.0
                        leftAnchor = 0.0
                        rightAnchor = 0.0
                        bottomAnchor = 30.0
                    }
                }
            )
            flowpane {
                addClass(TracksStyle.playlistInfo)
                anchorpaneConstraints {
                    leftAnchor = 0.0
                    rightAnchor = 0.0
                    bottomAnchor = 0.0
                }
                label {
                    textProperty() bindFlow states
                        .mapDistinctBy { it.sectionsState.getPlaylistByTitle(it.tracksState.selectedPlaylistTitle) }
                        .filterNotNull()
                        .map { "${it.title} / ${it.info.trackCount} / ${it.info.duration.toHours()}" }
                }
            }
        }
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    override val root = placeholderPane(
        ContentView(states, actions).root,
        TracksPlaceholderView().root
    ) {
        isActiveProperty bindFlow states
            .map { it.tracksState }
            .mapDistinctBy { it.tracks[it.selectedPlaylistTitle]?.size }
            .map { it != null && it > 0 }
    }
}
