package com.appspot.magtech.uiports

interface NotificationManager : AutoCloseable {

    fun showInfo(message: String)
}
