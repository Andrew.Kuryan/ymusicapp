package com.appspot.magtech.api

import com.appspot.magtech.application.Action
import com.appspot.magtech.dependencies.LocalFileSystem
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

class ApiExperiments {

    private val actions = Channel<Action>()
    private val api = YMusicKtorApi(HttpClient(CIO), LocalFileSystem(), "downloads/", actions)

    @Test
    fun searchExperiment() = runBlocking {
        val user = api.localApi.getSavedUsers().find { it.first.login == "mttest" }!!.first
        val result = api.searchTracks(user, "fall")
        println(result.size)
        Unit
    }
}