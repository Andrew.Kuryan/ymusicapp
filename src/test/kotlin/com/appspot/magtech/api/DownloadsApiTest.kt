package com.appspot.magtech.api

import com.appspot.magtech.api.db.DBDownloadsApi
import com.appspot.magtech.dependencies.LocalFileSystem
import com.appspot.magtech.entities.*
import io.ktor.client.HttpClient
import io.ktor.client.engine.mock.*
import io.ktor.http.Url
import io.ktor.http.fullPath
import io.ktor.http.hostWithPort
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import java.nio.file.Files
import java.nio.file.Paths

private val Url.hostWithPortIfRequired: String get() = if (port == protocol.defaultPort) host else hostWithPort
private val Url.fullUrl: String get() = "${protocol.name}://$hostWithPortIfRequired$fullPath"

@TestMethodOrder(OrderAnnotation::class)
class DownloadsApiTest {

    private val testClient = HttpClient(MockEngine) {
        engine {
            addHandler { request ->
                when (request.url.fullUrl) {
                    "http://localhost/test-link" -> respond("test-content".toByteArray())
                    else -> error("Unhandled ${request.url.fullUrl}")
                }
            }
        }
    }

    private val downloadsApi = DBDownloadsApi(
        storagePath = "src/test/resources/",
        client = testClient,
        getTrackLink = { _, _ -> "http://localhost/test-link" },
        fileSystem = LocalFileSystem()
    )

    private val testTrack1 = YMusicTrack(
        id = 0,
        available = true,
        info = TrackInfo(
            title = "Test Track",
            duration = 15000,
            coverUri = "track-cover-0",
            artists = listOf(
                Artist(0, "Test Artist 0", "artist-cover-0"),
                Artist(1, "Test Artist 1", "artist-cover-1")
            ),
            albums = listOf(
                Album(0, "Test Album 0", 20, "album-cover-0"),
                Album(1, "Test Album 1", 116, "album-cover-1")
            )
        )
    )

    private val testTrack2 = YMusicTrack(
        id = 1,
        available = true,
        info = TrackInfo(
            title = "Second Track",
            duration = 35000,
            coverUri = "track_cover-1",
            artists = listOf(Artist(1, "Test artist 1", "artist-cover-1")),
            albums = listOf(Album(0, "Test Album 0", 20, "album-cover-0"))
        )
    )

    val testUser = User(
        uid = "test-uid",
        login = "TestUser",
        avatarUrl = null,
        sessionCredentials =
        SessionCredentials("", "", "", "", "")
    )

    @Test
    @Order(1)
    fun getPlaylistsTest() = runBlocking {
        val result = downloadsApi.getLocalPlaylists()
        assertEquals(1, result.size)

        val (playlist, tracks) = result.first()
        assertEquals("Downloads", playlist.title)
        assertEquals(0, playlist.info.duration)
        assertEquals(0, playlist.info.trackCount)
        assertEquals(0, tracks.size)
    }

    @Test
    @Order(2)
    fun downloadTrackTest() = runBlocking {
        downloadsApi.downloadTrack(testUser, testTrack1)
        assert(Files.exists(Paths.get("src/test/resources/Test Track.mp3")))

        val result = downloadsApi.getLocalPlaylists()
            .find { it.first.title == "Downloads" }!!
        val (playlist, tracks) = result
        assertEquals(1, playlist.info.trackCount)
        assertEquals(testTrack1.info.duration, playlist.info.duration)

        assertEquals(1, tracks.size)
        assertEquals(testTrack1.info.title, tracks.first().info.title)
    }

    @Test
    @Order(3)
    fun downloadSecondTrackTest() = runBlocking {
        downloadsApi.downloadTrack(testUser, testTrack2)

        val result = downloadsApi.getLocalPlaylists()
            .find { it.first.title == "Downloads" }!!
        val (playlist, tracks) = result
        assertEquals(2, playlist.info.trackCount)
        assertEquals(testTrack1.info.duration + testTrack2.info.duration, playlist.info.duration)

        assertEquals(2, tracks.size)
    }

    @Test
    @Order(4)
    fun getPlaylistAlbumsTest() = runBlocking {
        val result = downloadsApi.getLocalPlaylists()
            .find { it.first.title == "Downloads" }!!
        val (playlist, tracks) = result

        val albums = downloadsApi.getPlaylistAlbums(playlist)
        assertEquals(2, albums.size)
    }

    @Test
    @Order(5)
    fun getPlaylistArtistsTest() = runBlocking {
        val result = downloadsApi.getLocalPlaylists()
            .find { it.first.title == "Downloads" }!!
        val (playlist, tracks) = result

        val artists = downloadsApi.getPlaylistArtists(playlist)
        assertEquals(2, artists.size)
    }

    @Test
    @Order(6)
    fun removeTrackTest() = runBlocking {
        downloadsApi.removeTrack(testTrack1.toLocal("src/test/resources/Test Track.mp3", NotAvailableAnalysis))
        assert(!Files.exists(Paths.get("src/test/resources/Test Track.mp3")))

        val result = downloadsApi.getLocalPlaylists()
            .find { it.first.title == "Downloads" }!!
        val (playlist, tracks) = result
        assertEquals(1, playlist.info.trackCount)
        assertEquals(testTrack2.info.duration, playlist.info.duration)

        assertEquals(1, tracks.size)

        val albums = downloadsApi.getPlaylistAlbums(playlist)
        val artists = downloadsApi.getPlaylistArtists(playlist)
        assertEquals(1, albums.size)
        assertEquals(1, artists.size)
    }

    @Test
    @Order(7)
    fun removeSecondTrackTest() = runBlocking {
        downloadsApi.removeTrack(testTrack2.toLocal("src/test/resources/Second Track.mp3", NotAvailableAnalysis))

        val result = downloadsApi.getLocalPlaylists()
            .find { it.first.title == "Downloads" }!!
        val (playlist, tracks) = result
        assertEquals(0, playlist.info.trackCount)
        assertEquals(0, playlist.info.duration)

        assertEquals(0, tracks.size)

        val albums = downloadsApi.getPlaylistAlbums(playlist)
        val artists = downloadsApi.getPlaylistArtists(playlist)
        assertEquals(0, albums.size)
        assertEquals(0, artists.size)
    }

    companion object {

        @AfterAll
        @JvmStatic
        fun clearDB() {
            Files.delete(Paths.get("src/test/resources/downloads.mv.db"))
        }
    }
}
